#!/bin/bash

services=("image-service" "auth-service" "user-service" "map-service" "shop-service" "api-gateway")

for service in ${services[@]}
do
    cd $service
    cd integration-test
    ./test.sh
    exitCode=$?
    if [ $exitCode -ne 0 ] ; then
        exit 1;
    fi
    cd ../..
done
