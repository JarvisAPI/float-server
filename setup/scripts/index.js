'use strict';

const {makeClient} = require("./mongo.js");
const {options} = require("./config.js");
const {addEmptyProductArray, addEmptyShopArray} = require("./script.js");

function errorCallback(err) {
  console.log("Obtained error!");
  console.log(err);
}

makeClient(options, (db_client) => {
  addEmptyProductArray(db_client);
  addEmptyShopArray(db_client);
}, errorCallback);
