"use strict";

var map_db, shop_db,auth_db,user_db;
var updateCount;
var totalCount;

let expo = {};
/**
 * Change the shop product schema by adding location
 * field to each product.
 */
function script(db_client) {
  map_db = db_client.db("map_db");
  shop_db = db_client.db("shop_db");
  updateCount = 0;
  totalCount = 0;
  
  let cursor = shop_db.collection("product").find();
  cursor.forEach(migrateProduct, (err) => {
    if (err) {
      console.log("Script error!");
      console.log(err);
      return;
    }
    console.log("Number of objects to be migrated: " + totalCount);
  });
}

/**
 * Add empty array as products field if shop doesn't have the products field.
 */
expo.addEmptyProductArray = (db_client) => {
  shop_db = db_client.db("shop_db");
  let shopCol = shop_db.collection("shop");
  let cursor = shop_db.collection("shop").find();
  let promises = [];
  cursor.forEach((shop) => {
    if (!shop.products) {
      promises.push(shopCol.updateOne({shop_id: shop.shop_id}, {$set: {products: []}}));
    }
  }, (err) => {
    if (err) {
      console.log(err);
    } else {
      Promise.all(promises)
        .then(() => {
          console.log("Shops updated with empty product array if field doesn't exist");
        });
    }
  });
};

expo.addEmptyShopArray = (db_client) => {
  user_db = db_client.db("user_db");
  let userCol = user_db.collection("user");
  let cursor = user_db.collection("user").find();
  let promises = [];
  cursor.forEach((user) => {
    if (!user.shops) {
      promises.push(userCol.updateOne({username: user.username}, {$set: {shops: []}}));
    }
  }, (err) => {
    if (err) {
      console.log(err);
    } else {
      Promise.all(promises)
        .then(() => {
          console.log("User updated with empty shops array if field doesn't exist");
        });
    }
  });
}

function migrateProduct(product) {
  totalCount++;
  let shop_id = product.shop_id;
  map_db.collection("icons").findOne({icon_id: shop_id})
    .then((icon) => {
      updateProduct(product, icon);
    })
    .catch((err) => {
      console.log("Getting icon obtained error!");
      console.log(err);
    });
}

function updateProduct(product, icon) {
  let update = {
    $set: {location: icon.icon_location}
  };
  shop_db.collection("product").updateOne(
    {product_id: product.product_id},
    update,
    (err, result) => {
      if (err) {
	console.log("Update product obtained error!");
	console.log(err);
      } else if (result) {
	updateCount += result.modifiedCount;
      }
    });
}

function updateAuthUserDB(db_client){
  auth_db = db_client.db("user_db");
  user_db = db_client.db("auth_db");
  updateCount = 0;
  totalCount = 0;

  let cursor = auth_db
		.collection("user")
		.find({"username": {$in:["100610017033674530420", "williem"]}})
		.project({username: 1,profile_images:1, _id:0})
	cursor.toArray().then((err,result) => {console.log(err)})
}

function addName(user) {
  console.log(user)
  totalCount++
  let username = user.username
  let update = {
    $set: {name: username}
  };
  if(!user.name){
    auth_db.collection("user").updateOne(
	    {username: username},
	    update,
	    (err, result) =>{
	      if(err){
		console.log("error occured while updating auth_db")
		console.log(err);
	      } else if(result){
		updateCount += result.modifiedCount;
	      }
	    })

    user_db.collection("user").updateOne(
            {username: username},
            update,
            (err, result) =>{
              if(err){
                console.log("error occured while updating auth_db")
                console.log(err);
              } else if(result){
                updateCount += result.modifiedCount;
              }
            })
  }
}


module.exports = expo;
