#!/bin/bash

scriptdir="$(dirname "$0")"
cd $scriptdir

docker-compose -p script build
docker-compose -p script up
docker-compose -p script rm -f
