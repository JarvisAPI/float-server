'use strict';

const {MongoClient, Server} = require('mongodb');

const makeClient = (options, callback, errorCallback) => {
  new MongoClient(new Server(options.host, options.port), {
    user: options.user,
    password: options.pwd,
    authSource: options.db})
    .connect((err, client) => {
      if (err) {
	errorCallback(err);
	return;
      }
      callback(client);
    });
};

module.exports = {makeClient};
