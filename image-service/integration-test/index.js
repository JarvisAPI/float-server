"use strict";

const request = require("supertest");
const should = require("should");
const isPortReachable = require("is-port-reachable");
const fs = require("fs");

describe("image-service", function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // Need a long timeout for exponential backoff to work.
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(7, () => isPortReachable(port, {host: host}));
    };
    return tryConnect(url);
  });

  it("post image then get image back", (done) => {
    let verifyImage = (err, res) => {
      if (err) {
        throw err;
      }
      fs.readFile("test-image.jpg", (err, data) => {
        if (err) {
          throw err;
        }
        should(Buffer.compare(data, res.body)).equal(0);
        done();
      });
    };
    let getImageFile = (err, res) => {
      if (err) {
        throw err;
      }
      let image_file = res.body.filename;
      api.get(`/image/${image_file}`)
        .expect(200, verifyImage);
    };
    api.post("/image")
      .field("name", "test-image")
      .attach("image", "test-image.jpg")
      .expect(200, getImageFile);
  });

  it("post private image then get image back", (done) => {
    let bitmap = fs.readFileSync("test-image.jpg");
    let pu = ["user1","user2"];


    let verifyImage = (err, res) => {
      if (err) {
        throw err;
      }
      fs.readFile("test-image.jpg", (err, data) => {
        if (err) {
          throw err;
        }
        should(Buffer.compare(data, res.body)).equal(0);
        done();
      });
    };
    let getImageFile = (err, res) => {
      if (err) {
        throw err;
      }
      let image_file = res.body.filename;
      api.get(`/image/${image_file}` + "?username=" + pu[0])
        .expect(200, verifyImage);
    };

    api.post(`/private_image?permission_users=${JSON.stringify(pu)}`)
      .attach("image", "test-image.jpg")
      .expect(200, getImageFile);
  });

  it("add one image then delete it", (done) => {
    let fn;
    api.post("/image")
      .field("name", "test-image")
      .attach("image", "test-image.jpg")
      .then(req => {
        fn = [];
        fn.push(req.body.filename);
        let jarr = JSON.stringify(fn);
        return api.delete(`/image/${jarr}`)
          .expect(200);
      })
      .then(() => api.get(`/image/${fn[0]}`).expect(404))
      .then(() => done())
      .catch(done);
  });

});
