const {dbSettings, serverSettings, uploadSettings} = require('./config');
const {initDI} = require('./di');
const database = require('./db');

const init = initDI.bind(null, {database, dbSettings, serverSettings, uploadSettings});

module.exports = Object.assign({}, {init});
