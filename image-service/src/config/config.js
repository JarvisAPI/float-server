"use strict";

const serverSettings = {
    port: process.env.PORT || 3000,
    ssl: require("./ssl")
};

const uploadSettings = {
  image_server_host: process.env.IMAGE_SERVER_HOST,
  dirbase: "data",
  dirdel: "deleted",
  private_dirbase: "private_data",
  root: "/app"
};

const dbSettings = {
  db: process.env.DB || "image_db",
  user: process.env.DB_USER || "image_service",
  pwd: process.env.DB_PASS || "TCPmyClientMapAppSocketv1.0",
  host: process.env.DATABASE_HOST || "127.0.0.1",
  port: "27017"
};

module.exports = Object.assign({serverSettings, dbSettings, uploadSettings});
