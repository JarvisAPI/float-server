"use strict";

module.exports = (app, container) => {
  const logger = container.cradle.logger;
  const repo = container.cradle.repo;
  let fileUpload = container.cradle.upload;
  let upload = fileUpload.upload(container);
  let uploadSettings = container.cradle.uploadSettings;

  app.post("/image", upload.single("image"), (req, res, next) => {
    let fn;
    fileUpload.movefile(req.file, uploadSettings.dirbase)
      .then(filename => {
        fn = filename;
        return repo.addImage(filename);
      })
      .then(() => res.status(200).json({filename: fn}))
      .catch(next);
  });

  app.post("/private_image", upload.single("image"), (req, res, next) => {
    let pu = JSON.parse(req.query.permission_users);
    logger.debug("pu" + JSON.stringify(pu));
    if(!pu || !(pu instanceof Array) || pu.length === 0){
      res.status(400).send();
    }
    fileUpload.movefile(req.file, uploadSettings.dirbase)
      .then((filename) => {
        repo.addPrivateImage(filename, pu).then(() => {
	      res.status(200).json({filename});
        }).catch(next);
      }).catch(next);
  });

  app.get("/image/:image_file", (req, res, next) => {
    let image_file = req.params.image_file;
    let username = req.query.username;
    repo.checkPermission(image_file, username)
      .then(() => {
        let dirPath = fileUpload.getpath(req.params.image_file, uploadSettings.dirbase);
        let imagePath = `${dirPath}/${req.params.image_file}`;
        res.sendFile(imagePath, {root: uploadSettings.root}, (err) => {
          if (err) {
  	        next(err);
          }
        });
      }).catch(() => {
        res.status(404).send();
      });
  });

  app.delete("/image/:image_files", (req, res, next) => {
    let image_files = req.params.image_files;
    try {
      image_files = JSON.parse(image_files);
    } catch(jerr) {
      next(jerr);
      return;
    }
    let promises = [];
    for (let i = 0; i < image_files.length; i++) {
      let image_file = image_files[i];
      promises.push(
        fileUpload.mvfile(image_file, uploadSettings.dirbase, uploadSettings.dirdel)
          .then(() => repo.removeImage(image_file))
      );
    }
    Promise.all(promises)
      .then(() => res.sendStatus(200))
      .catch(next);
  });
};
