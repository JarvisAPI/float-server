"use strict";

const createIndex = (db) => {
  db.createIndex("image", {image_url: 1}, {unique: true} ,(err, name) => {/*Ignore*/});
};

const repository = (container) => {
  const db_client = container.resolve("db_client");
  const db = db_client.db(container.cradle.dbSettings.db);
  const logger = container.resolve("logger");
  createIndex(db);

  const imgCol = db.collection("image");

  let repo = {};

  repo.addPrivateImage = (imageUrl, permissionUsers) => {
    return new Promise((resolve, reject) => {
      const payload = {
        image_url: imageUrl,
        restricted: true,
        permission_users : permissionUsers
      };
      imgCol.insertOne(payload, (err, result) => {
	    if (err) {
	      reject(new Error("An error occurred when adding a new image, err: " + err));
	      return;
	    }
	    resolve();
      });
    });
  };

  repo.addImage = (imageUrl) => {
    return new Promise((resolve, reject) => {
      const payload = {
        image_url: imageUrl,
        restricted: false
      };
      imgCol.insertOne(payload, (err, result) => {
	    if (err) {
	      reject(new Error("An error occurred when adding a new image, err: " + err));
	      return;
	    }
	    resolve();
      });
    });
  };

  repo.checkPermission = (imageUrl, username) => {
    return new Promise((resolve, reject) => {
      imgCol.findOne({image_url: imageUrl}, (err, result) => {
	    if (err) {
	      reject(new Error("Error occured while finding image, err: " + err));
	      return;
	    }
        if (result){
          if(!result.restricted ||
             username &&
             result.permission_users && result.permission_users.indexOf(username) > -1){
            resolve();
          }
          else{
            let err = new Error("Not authorized");
            err.status = 401;
            reject(err);
          }
        } else {
          let err = new Error("Unable to find image in db");
          err.status = 404;
          reject(err);
        }
      });
    });
  };

  repo.removeImage = (image_url) => {
    return imgCol.findOne({image_url})
      .then(imgObj => {
        if (imgObj && !imgObj.restricted) {
          return imgCol.deleteOne({image_url});
        }
        return Promise.reject();
      });
  };

  repo.disconnect = () => {
    db_client.close();
  };

  return repo;
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve("db_client")) {
      reject(new Error("Connection db client not supplied!"));
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});
