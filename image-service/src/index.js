"use strict";

const {EventEmitter} = require("events");
const di = require("./config");
const mediator = new EventEmitter();
const repository = require("./repository/repository");
const server = require("./server/server");
const {asValue} = require("awilix");

const bootServer = (container) => {
  let logger = container.cradle.logger;
  logger.info("Starting server");

  repository.connect(container)
    .then((repo) => {
      logger.info("Connected... Starting Server");
      container.register("repo", asValue(repo));
      return server.start(container);
    })
    .then((app) => {
      logger.info(`Server started successfully, running on port: ${container.cradle.serverSettings.port}.`);
      app.on("close", () => {
        container.cradle.repo.disconnect();
      });
    });
};

mediator.on("di.ready", (container) => {
  bootServer(container);  
});

di.init(mediator);

mediator.emit("init");
