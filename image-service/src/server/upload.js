// Upload image folder scheme.
"use strict";

const md5 = require("md5");
const fs = require("fs");
const path = require("path");
const multer = require("multer");
const mkdirp = require("mkdirp");

let exp = {};

// f: the md5 hash sum used as the file name.
// dirname: the base directory name.
const setupFolders = (f, dirname) => {
  return new Promise((resolve, reject) => {
    let path = getpath(f, dirname);
    mkdirp(path, (err) => {
      if (err) {
	    reject(err);
	    return;
      }
      resolve(path);
    });
  });
};

// Moves a file to a different folder.
exp.mvfile = (filename, dirname, destdir) => {
  let path = `${getpath(filename, dirname)}/${filename}`;
  let dest = `${destdir}/${filename}`;
  return new Promise((resolve, reject) => {
    fs.rename(path, dest, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

// rename a file to it"s md5 hash, and store it in a directory structure
// based on the md5 hash.
//
// Postcondition: If file is null, then the promise returned will
//   resolve an empty object. Otherwise the promise returned will
//   resolve with the name of the file that was stored onto disk.
//
// file: can be null, otherwise must contain
//   file.filename: the name of the file.
//   file.destination: the folder which the file is at.
// dirname: the base directory name. The resulting path will be
//   "${dirname}/(some intermediate directories)/(md5 hash filename)".
exp.movefile = (file, dirname) => {
  return new Promise((resolve, reject) => {
    if (!file) {
      resolve();
      return;
    }
    let path = `${file.destination}/${file.filename}`;
    fs.readFile(path, (err, buf) => {
      let filename = md5(buf) + Date.now();
      setupFolders(filename, dirname)
	.then((dest) => {
	  let newPath = `${dest}/${filename}`;
	  fs.rename(path, newPath, (err) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	    resolve(filename);
	  });
	})
	.catch((err) => {
	  reject(err);
	});
    });
  });
};

// get the directory path to a file based on the md5 hash file name, and a
// base directory path.
// f: the filename.
// dirname: the base directory path.
const getpath = (f, dirname) => {
  return `${dirname}/${f[0]}/${f[1]}/${f[2]}`;
};
exp.getpath = getpath;

const storage = (container) => {
  return multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, container.cradle.uploadSettings.dirbase);
    },
    filename: function(req, file, cb) {
      cb(null, file.fieldname + "-" + Date.now());
    }
  });
};

/**
 * Get the object associated with file uploads.
 */
exp.upload = (container) => {
  return multer({storage: storage(container)});
};

module.exports = exp;
