'use strict';

const spdy = require('spdy');
const express = require('express');
const helmet = require('helmet');
const api = require('../api/image');
const {asValue} = require('awilix');
const bodyparser = require('body-parser');

const start = (container) => {
  let logger = container.cradle.logger;
  // options contains:
  //  port: port to listen on.
  return new Promise((resolve, reject) => {
    if (!container.cradle.serverSettings.port) {
      reject(new Error('The server must be started with an available port'));
      return;
    }

    const app = express();
    if (!process.env.PROD) {
      let morgan = require('morgan');
      app.use(morgan('dev'));
    }
    app.use(bodyparser.json());
    app.use(helmet());
    app.use((err, req, res, next) => {
      logger.error(err);
      let code = 500;
      if (err.status) {
        code = err.status;
      }
      res.status(code).send('Something went wrong!');
    });

    logger.info('Setting up api path...');
    api(app, container);

    if (process.env.TEST === '1') {
      const server = app.listen(container.cradle.serverSettings.port, () => resolve(server));
    } else {
      const server = spdy.createServer(container.cradle.serverSettings.ssl, app)
    	    .listen(container.cradle.serverSettings.port, () => resolve(server));
    }
  });
};


module.exports = Object.assign({}, {start});
