'use strict';

const dbSettings = {
  db: process.env.DB || 'map_db',
  user: process.env.DB_USER || 'map_service',
  pwd: process.env.DB_PASS || 'TCPmyClientMapAppSocketv1.0',
  host: process.env.DATABASE_HOST || '127.0.0.1',
  port: 27017
};

const serverSettings = {
  port: process.env.PORT || 3000,
  ssl: require('./ssl')
};

module.exports = Object.assign({}, {dbSettings, serverSettings});
