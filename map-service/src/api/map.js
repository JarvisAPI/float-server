'use strict';

module.exports = (app, container) => {
  const repo = container.cradle.repo;
  const logger = container.cradle.logger;
  const validate = container.cradle.validate;  

  app.get('/map/icons/:latitude/:longitude', (req, res, next) => {
    let lat = parseFloat(req.params.latitude);
    let lng = parseFloat(req.params.longitude);
    repo.getMapIcons(lat, lng)
      .then((icons) => {
	    res.status(200).json({icons});
      }, (err) => {
        logger.error(err);
	    res.sendStatus(400);
      });
  });

  app.get('/map/icons/:latitude/:longitude/:min_distance/:max_distance/:offset/:limit', (req, res, next) => {
    let lat = parseFloat(req.params.latitude);
    let lng = parseFloat(req.params.longitude);
    let min_distance = parseFloat(req.params.min_distance);
    let max_distance = parseFloat(req.params.max_distance);
    let offset = parseFloat(req.params.offset);
    let limit = parseFloat(req.params.limit);
    repo.getMapIconsWithinBounds(lat, lng, min_distance, max_distance, offset, limit)
      .then((icons) => {
        res.status(200).json({icons});
      })
      .catch((err) => {
        logger.error(err);
        res.sendStatus(400);
      });
  });

  app.get('/map/icons/:icon_ids', (req, res, next) => {
    let icon_ids = JSON.parse(req.params.icon_ids);
    let {error} = validate({icon_ids}, 'icon_ids');
    if (!error && icon_ids.length < 30) {
      repo.getMapIconsById(icon_ids)
        .then((icons) => {
          res.status(200).json({icons});
        })
        .catch((err) => {
          logger.error(err);
          res.sendStatus(500);
        });
    } else {
      logger.error(error);
      res.sendStatus(400);
    }
  });

  app.post('/map/icons', (req, res, next) => {
    logger.debug("HERE!");    
    let icon = req.body.icon;
    let {error, value} = validate(icon, 'icon');
    if (!error) {
      repo.addMapIcon(icon)
        .then(() => {
	      res.sendStatus(200);
        })
        .catch(next);
    } else {
      res.sendStatus(400);
    }
  });

  app.delete('/map/icons', (req, res, next) => {
    let icon = req.body.icon;
    repo.removeMapIcon(icon)
      .then(() => {
        res.sendStatus(200);
      })
      .catch(next);
  });
};
