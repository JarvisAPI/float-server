'use strict';

const request = require('supertest');
const should = require('should');
const server = require('../server/server');
const config = require('../config/config');
const {createContainer, asValue} = require('awilix');
const logger = require('winston');

describe('Map API', () => {
  process.env.TEST = '1';
  let container = createContainer();

  let app = null;

  let testRepo = {
    getMapIcons(lat, lng) {
      return Promise.resolve([]);
    },
    addMapIcon(icon) {
      return Promise.resolve();
    }
  };  

  let validate = (icon, type) => {
    return Promise.resolve();
  };

  let testEventSource = {
    publish(topic, message, callback) {
    },
    subscribe(topics, callback, errorCallback) {
    }
  };
  
  container.register({
    serverSettings: asValue({port: 3000}),
    validate: asValue(validate),
    repo: asValue(testRepo),
    eventSource: asValue(testEventSource),
    logger: asValue(logger)
  });

  beforeEach(() => {
    return server.start(container)
      .then((serv) => {
	app = serv;
      });
  });

  afterEach(() => {
    app.close();
    app = null;
  });

  it('can return map locations', (done) => {
    let expected = {};
    expected['icons'] = [];
    request(app)
      .get('/map/icons/0/0')
      .expect((res) => {
	res.body.should.containEql(expected);
      })
      .expect(200, done);
  });

  it('can get correct response after adding map icon', (done) => {
    request(app)
      .post('/map/icons')
      .send({icon: {}})
      .expect(201, done);
  });
});
