'use strict';

const express = require('express');
const helmet = require('helmet');
const bodyparser = require('body-parser');
const api = require('../api/map');
const spdy = require('spdy');

const start = (container) => {
  let logger = container.cradle.logger;
  return new Promise((resolve, reject) => {
    const {port, ssl} = container.resolve('serverSettings');
    if (!port) {
      reject(new Error('The server must be started with an available port'));
      return;
    }

    const app = express();
    if (!process.env.PROD) {
      let morgan = require('morgan');
      app.use(morgan('dev'));
    }
    app.use(bodyparser.json());
    app.use(helmet());

    api(app, container);

    app.use((err, req, res, next) => {
      logger.error(err);
      
      let statusCode = 500;
      let message = 'Something went wrong!';
      if (err.status) {
        statusCode = err.status;
      }
      if (err.message) {
        message = err.message;
      }
      res.status(statusCode).json({error: message, type: 'ERR_SHOP_TOO_CLOSE'});
      next();
    });    
    
    if (process.env.TEST === '1') {
      const server = app.listen(port, () => resolve(server));
    } else {
      const server = spdy.createServer(ssl, app)
	    .listen(port, () => resolve(server));
    }
  });
};

module.exports = Object.assign({}, {start});
