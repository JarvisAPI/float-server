'use strict';

const server = require('./server');
const should = require('should');

describe('Server', () => {  
  it('should require a port to start', () => {
    let container = {
      resolve(key) {
	if (key === 'serverSettings') {
	  return {port: null, ssl: null};
	}
	return {};
      }
    };    
    return server.start(container).should.be.rejectedWith(/port/);
  });
});
