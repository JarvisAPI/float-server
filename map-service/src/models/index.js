const joi = require('joi');
const icon = require('./icon.model')(joi);
const icon_ids = require('./icon_ids.model')(joi);

const schemas = {icon, icon_ids};

const schemaValidator = (object, type) => {
  if (!object) {
    return {error: new Error('No object to validate')};
  }
  return joi.validate(object, schemas[type]);
};

module.exports = {validate: schemaValidator};
