'use strict';

module.exports = (joi) => ({
  icon_id: joi.string().required(),
  icon_latitude: joi.number().required(),
  icon_longitude: joi.number().required(),
  icon_type: joi.number().integer().required()
});
