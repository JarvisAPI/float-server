'use strict';

module.exports = (joi) => ({
  icon_ids: joi.array().items(joi.string()).required()
});
