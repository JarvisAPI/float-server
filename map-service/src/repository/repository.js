"use strict";

const createIconIndexes = (db) => {
  db.createIndex("icons", {icon_id: 1}, {unique: true}, (err, name) => {/*Ignore*/});
  db.createIndex("icons", {icon_location: "2dsphere"});
};

const repository = (container) => {
  const db_client = container.resolve("db_client");
  const db = db_client.db(container.cradle.dbSettings.db);
  const logger = container.cradle.logger;
  createIconIndexes(db);

  const iconsCol = db.collection("icons");
  const repo = {};
  
  /**
   * Convert the location representation of the mongo icon.
   */
  const convertLocation = (icon) => {
    let lng = icon.icon_location.coordinates[0];
    let lat = icon.icon_location.coordinates[1];
    delete icon.icon_location;
    icon.icon_latitude = lat;
    icon.icon_longitude = lng;
  };

  const convertIconLocations = (icons) => {
    for (let i = 0; i < icons.length; i++) {
      convertLocation(icons[i]);
    }
    return icons;
  };

  repo.addMapIcon = function (icon) {
    let latitude = icon.icon_latitude;
    let longitude = icon.icon_longitude;
    let maxDistance = 3; // The minimum gap between icons in meters.
    return getMapIconsWithinDistance(latitude, longitude, maxDistance)
      .count()
      .then(count => {
	    if (count === 0) {
	      return insertMapIconToDB(icon);
        }
        let err = new Error("Icon location is too near another icon");
        err.status = 400;
        logger.debug("Return rejection!");
        return Promise.reject(err);
      });
  };
  
  const insertMapIconToDB = (icon) => {
    const payload = {
      icon_id: icon.icon_id,
      icon_location: {type: "Point", coordinates: [parseFloat(icon.icon_longitude), parseFloat(icon.icon_latitude)]},
      icon_type: icon.icon_type
    };    
    return iconsCol.insertOne(payload);
  };

  /**
   * Returns the mongodb cursor for icons nearby.
   */
  const getMapIconsWithinDistance = (latitude, longitude, distance) => {
    let options = {
      projection: {_id: 0}
    };
    return iconsCol.find({icon_location:
                          {$near: {
                            $geometry: {
                              type: "Point",
                              coordinates: [longitude, latitude]
                            },
                            $maxDistance: distance}
                          }
                         }, options
                        );
  };

  /**
   * Obtains the map icons that are at most max_distance and at least min_distance
   * away from the point indicated by latitude and longitude.
   */
  repo.getMapIconsWithinBounds = function (latitude, longitude, min_distance, max_distance, offset, limit) {
    let options = {
      projection: {
        _id: 0
      }
    };
    const cursor = iconsCol.find({icon_location:
                                  {$near: {
                                    $geometry: {
                                      type: "Point",
                                      coordinates: [longitude, latitude]
                                    },
                                    $minDistance: min_distance,
                                    $maxDistance: max_distance}
                                  }
                                 }, options
                                );
    let maxReturn = 30;
    limit = limit <= maxReturn ? limit : maxReturn;
    return cursor.skip(offset).limit(limit).toArray()
      .then(arr => convertIconLocations(arr));
  };

  // Gets the map icons within a certain radius, about a certain point.
  repo.getMapIcons = function (latitude, longitude){
    let radiusInMiles = 1;
    let radiusOfEarth = 3963.2;
    let radiusInRadians = radiusInMiles / radiusOfEarth;
    let options = {
      projection: {_id: 0}
    };
    const cursor = iconsCol.find({ icon_location:
				                   { $geoWithin:
				                     { $centerSphere: [ [longitude, latitude], radiusInRadians ] } } }, options);
    let maxReturn = 30;
    return cursor.limit(maxReturn).toArray()
      .then(arr => convertIconLocations(arr));
  };

  /**
   * Remove a map icon.
   * icon: a json object that contains the icon_id field.
   */
  repo.removeMapIcon = function (icon) {
    return iconsCol.deleteOne({icon_id: icon.icon_id});
  };

  repo.getMapIconsById = function (icon_ids) {
    let query = {
      icon_id: {
        $in: icon_ids
      }
    };
    let options = {
      projection: {
        _id: 0
      }
    };
    return iconsCol.find(query, options)
      .toArray()
      .then(arr => convertIconLocations(arr));
  };
  
  repo.disconnect = () => {
    db_client.close();
  };

  return repo;
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve("db_client")) {
      reject(new Error("Connection db client not supplied!"));
      return;
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});
