#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cleanup () {
    docker rm map-service-unit-test -f
}

trap 'cleanup ; printf "${RED}Tests Failed for Unexpected Reasons${NC}\n" ; exit 1' HUP INT QUIT PIPE TERM
cd ..
docker build --tag map-service-unit-test -f DockerfileUnitTest .
docker run --name map-service-unit-test map-service-unit-test

TEST_EXIT_CODE=`docker wait map-service-unit-test`
if [[ -z ${TEST_EXIT_CODE+x} || "$TEST_EXIT_CODE" -ne 0 ]] ; then
    printf "${RED}Tests Failed${NC} - Exit Code: $TEST_EXIT_CODE\n"
else
    printf "${GREEN}Tests Passed${NC}\n"
    cleanup
fi
# Remove images with no tags.
docker rmi $(docker images | grep "^<none>" | awk '{print $3}') &> /dev/null
docker volume rm $(docker volume ls -qf dangling=true) &> /dev/null
exit $TEST_EXIT_CODE

