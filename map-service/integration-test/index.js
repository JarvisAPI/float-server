'use strict';

const request = require('supertest');
const isPortReachable = require('is-port-reachable');
const should = require('should');

describe('map-service', function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // Need a long timeout for exponential backoff to work.
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(5, () => isPortReachable(port, {host}));
    };
    return tryConnect(url);
  });

  it('can add an icon and get that icon back correctly', (done) => {
    let stubIcon = {};
    let lat = 3, lng = 3;
    stubIcon['icon_id'] = 'test-uuid';
    stubIcon['icon_latitude'] = lat;
    stubIcon['icon_longitude'] = lng;
    stubIcon['icon_type'] = 0;
    let icon = Object.assign({
      icon: stubIcon
    });
    let expectedIcon = {
      icon_id: 'test-uuid',
      icon_latitude: lat,
      icon_longitude: lng,
      icon_type: 0
    };
    api.post('/map/icons')
      .send(icon)
      .expect(200, (err, res) => {
        if (err) {
          throw err;
        }
	api.get(`/map/icons/${lat}/${lng}`)
	  .expect((res) => {
	    res.body.should.have.property('icons').containEql(expectedIcon);
	  })
	  .expect(200, done);	
      });
  });

  it('can add an icon and then delete that icon', (done) => {
    let testIcon = {};
    let lat = 1, lng = 2;
    testIcon['icon_id'] = 'test-uuid2';
    testIcon['icon_latitude'] = lat;
    testIcon['icon_longitude'] = lng;
    testIcon['icon_type'] = 0;
    let icon = {icon: testIcon};
    let expectedIcon = {
      icon_id: 'test-uuid2',
      icon_latitude: lat,
      icon_longitude: lng,
      icon_type: 0
    };

    let testIconGone = (err, res) => {
      if (err) {
        throw err;
      }
      // Make sure icon is gone from server.
      api.get(`/map/icons/${lat}/${lng}`)
        .expect((res) => {
          res.body.should.have.property('icons').not.containEql(expectedIcon);
        })
        .expect(200, done);
    };
    let testRemoveIcon = (err, res) => {
      if (err) {
        throw err;
      }      
      api.delete('/map/icons')
        .send(icon)
        .expect(200, testIconGone);
    };
    let testGetIcon = (err, res) => {
      if (err) {
        throw err;
      }
      // Make sure icon that is added is actually present.
      api.get(`/map/icons/${lat}/${lng}`)
        .expect((res) => {
          res.body.should.have.property('icons').containEql(expectedIcon);
        })
        .expect(200, testRemoveIcon);
    };
    api.post('/map/icons')
      .send(icon)
      .expect(200, testGetIcon);
  });

  it('adding icons too close together returns error', (done) => {
    let testIcon1 = {
      icon_id: 'testIcon1-uuid',
      icon_latitude: 0,
      icon_longitude: 0,
      icon_type: 0
    };
    let testIcon2 = {
      icon_id: 'testIcon2-uuid',
      icon_latitude: 0,
      icon_longitude: 0.0001,
      icon_type: 0
    };
    let icon1 = {icon: testIcon1};
    let icon2 = {icon: testIcon2};
    api.post('/map/icons')
      .send(icon1)
      .expect(200, (err, res) => {
        if (err) {
          throw err;
        }
        api.post('/map/icons')
          .send(icon2)
          .expect(400, done);
      });
  });

  it('add an array of icons then get them back by id', (done) => {
    let calledDone = false;
    let expectedIconArr = [];
    let icon_ids = [];
    let promises = [];
    for (let i = 0; i < 10; i++) {
      let testIcon = {
        icon_id: `array-icon-${i}`,
        icon_latitude: 10 + i * 0.005,
        icon_longitude: 10 + i * 0.005,
        icon_type: 0
      };
      promises.push(api.post('/map/icons')
                    .send({icon: testIcon})
                    .catch((err) => {
                      if (!calledDone) {
                        calledDone = true;
                        done(err);
                      }
                    }));
      icon_ids.push(testIcon.icon_id);
      expectedIconArr.push(testIcon);
    }
    let verifyIcons = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.icons.should.deepEqual(expectedIconArr);
      done();
    };
    let getIconsById = (res) => {
      api.get(`/map/icons/${JSON.stringify(icon_ids)}`)
        .expect(200, verifyIcons);
    };
    Promise.all(promises)
      .then(getIconsById)
      .catch((err) => {
        done(new Error('Unable to add Icon array'));
      });
  });

  it('add two icons close together and get both back using their bounds', (done) => {
    let icon1 = {
      icon_id: 'icon1',
      icon_latitude: 1,
      icon_longitude: 1,
      icon_type: 0
    };
    let icon2 = {
      icon_id: 'icon2',
      icon_latitude: 1.001,
      icon_longitude: 1,
      icon_type: 0
    };
    let verifyIcons = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      res.body.should.have.property('icons').deepEqual([icon1, icon2]);
      done();
    };
    let getIcons = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      api.get('/map/icons/1/1/0/200/0/10')
        .expect(200, verifyIcons);
    };
    let sendIcon2 = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      api.post('/map/icons')
        .send({icon: icon2})
        .expect(200, getIcons);
    };
    api.post('/map/icons')
      .send({icon: icon1})
      .expect(200, sendIcon2);
  });
});
