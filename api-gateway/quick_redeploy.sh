#!/bin/bash

# Quickly redeploy all containers that require mongodb, used when their mongodb instance pool is destroyed

./redeploy.sh image-service
./redeploy.sh shop-service
./redeploy.sh user-service
./redeploy.sh map-service
./redeploy.sh auth-service
./redeploy.sh api-gateway
