#!/bin/bash

docker-compose -p deploy build
docker-compose -p deploy up -d --no-recreate mongodb
sleep 20
echo "Setting up mongo replica set"
docker exec deploy_mongodb_1 /initiate_replica_set.sh
sleep 20
docker-compose -p deploy up -d --no-recreate api-gateway
docker-compose -p deploy up -d --no-recreate elastic-search-service
sleep 30
docker-compose -p deploy up -d --no-recreate ejabberd-service
docker-compose -p deploy up -d --no-recreate elastic-mongo-connector
echo "enabling ejabberd mod http offline module"
sleep 30
../ejabberd-service/enable_mod_http_offline.sh
echo "setting up elastic search indexes"
../elastic-search-service/setup/english/setup-index.sh -i shop_db

# Remove images with no tags.
docker rmi $(docker images | grep "^<none>" | awk '{print $3}') &> /dev/null
docker volume rm $(docker volume ls -qf dangling=true) &> /dev/null
