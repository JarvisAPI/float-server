'use strict';

const repository = (container) => {
  /*
   * An event document:
   *   actions: the list of actions that transactions requires to be completed.
   *   completed_actions: the list of actions that is completed.
   *   revert: true means that we should revert all completed_actions, false
   *     means that we should complete all remaining actions.
   *
   * Collections:
   *   event: the current collection of pending transactions.
   */
  const db_client = container.cradle.db_client;
  const db = db_client.db(container.cradle.dbSettings.db);

  const addEvent = (forward_states, compensating_states, current_state) => {
    let eventObj = {
      forward_states,
      compensating_states,
      is_compensating: false,
      current_state
    };
    return new Promise((resolve, reject) => {
      db.collection('event').insertOne(eventObj, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.insertedId);
        }
      });
    });
  };

  /**
   * Update event based on mongo id key.
   */
  const updateEvent = (_id, current_state) => {
    let update = {
      $set: {current_state}
    };
    return new Promise((resolve, reject) => {
      db.collection('event').updateOne({_id}, update, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  const removeEvent = (_id) => {
    return new Promise((resolve, reject) => {
      db.collection('event').deleteOne({_id})
        .then(resolve)
        .catch(reject);
    });
  };

  /**
   * Set an event to be compensating.
   */
  const setEventCompensating = (_id) => {
    let update = {
      $set: {is_compensating: true}
    };
    return new Promise((resolve, reject) => {
      db.collection('event').updateOne({_id}, update, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  const disconnect = () => {
    db_client.close();
  };

  return {
    addEvent,
    updateEvent,
    removeEvent,
    setEventCompensating,
    disconnect
  };
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve('db_client')) {
      reject(new Error('Connection db client not supplied!'));
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});
