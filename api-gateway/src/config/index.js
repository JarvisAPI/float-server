const {dockerSettings, serverSettings, requestOptions, dbSettings, jwtSettings} = require('./config');
const database = require('./db');
const {initDI} = require('./di');

const init = initDI.bind(null, {serverSettings, dockerSettings, requestOptions, dbSettings, jwtSettings, database});

module.exports = Object.assign({}, {init});
