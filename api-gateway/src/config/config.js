'use strict';

const fs = require('fs');

let proto = 'https';
if (process.env.TEST === '1') {
  proto = 'http';
}

const serverSettings = {
  proto: proto,
  host: process.env.HOST || 'ec2api.win',
  port: process.env.PORT || 8080,
  prefix: '/gateway',
  ssl: require('./ssl')
};

const requestOptions = {
  CA: fs.readFileSync(__dirname + '/ssl/ca.pem')
};

const jwtSettings = {
  publicKey: fs.readFileSync(__dirname + '/keys/jwt_key.pub')
};

let dockerSettings = null;
if (process.env.USE_DOCKER_MACHINE === '1') {
  const machine = process.env.DOCKER_HOST;
  const tls = process.env.DOCKER_TLS_VERIFY;
  const certDir = process.env.DOCKER_CERT_PATH;

  if (!machine) {
    throw new Error('You must set the DOCKER_HOST environment variable');
  }
  if (tls === 1) {
    throw new Error('When using DOCKER_TLS_VERIFY=1 you must specify the property DOCKER_CERT_PATH for certificates');
  }
  if (!certDir) {
    throw new Error('You must set the DOCKER_CERT_PATH environment variable');
  }

  dockerSettings = {
    protocol: 'https',
    host: machine.substr(machine.indexOf(':', 0) + 3, machine.indexOf(':', 6) - 6),
    port: parseInt(machine.substr(-4), 10),
    checkServerIdentity: false,
    ca: fs.readFileSync(certDir + '/ca.pem'),
    cert: fs.readFileSync(certDir + '/cert.pem'),
    key: fs.readFileSync(certDir + '/key.pem'),
    version: 'v1.25'
  };
}
else {
  dockerSettings = {
    host: '127.0.0.1',
    socketPath: '/var/run/docker.sock'
  };
}

const dbSettings = {
  db: process.env.DB || 'api_db',
  user: process.env.DB_USER || 'api_service',
  pwd: process.env.DB_PASS || 'TCPmyClientMapAppSocketv1.0',
  host: process.env.DATABASE_HOST || '127.0.0.1',
  port: process.env.DATABASE_PORT || 27017
};

module.exports = Object.assign({}, {serverSettings, dockerSettings, requestOptions, dbSettings, jwtSettings});
