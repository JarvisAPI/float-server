'use strict';
const mysql = require('mysql');

const MAX_TIMEOUT = 20000;
var timeOut = 100;

const makeClient = (options, callback, errorCallback) => {
    mysql.createConnection({
        host: options.host,
        user: options.user ,
        password: options.pwd,
        database: options.db,
        port: options.port
   }).connect((err, client) => {
      if (err) {
        errorCallback(err);
        return;
      }
      callback(client);
    });
};

const expBackoff = (options, mediator) => {
  makeClient(options, (client) => {
    mediator.emit('db_client.ready', client);
  }, (err) => {
    if (timeOut < MAX_TIMEOUT) {
      setTimeout(expBackoff, timeOut, options, mediator);
      timeOut *= 2;
    } else {
      mediator.emit('db_client.error', err);
    }
  });
};

const connect = (options, mediator) => {
  mediator.once('boot.ready', () => {
    expBackoff(options, mediator); 
  });
};

module.exports = Object.assign({}, {connect});
