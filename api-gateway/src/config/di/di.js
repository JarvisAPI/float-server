'use strict';

const {createContainer, asValue} = require('awilix');
var request = require('request-promise');
var rcb = require('request'); // request library that uses callbacks instead of promises.
const jwt = require('jsonwebtoken');

const winston = require('winston');
const winstonError = require('winston-error');
const config = winston.config;
const logObj = {
  formatter: function(options) {
	return `[${new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')}]`+
      '[' +
	  config.colorize(options.level, options.level.toUpperCase()) +
	  ']: ' +
	  (options.message && !options.meta.error ? options.message : '') +
          (options.meta.error ? options.meta.error.stack : '');
      }
};
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console) (logObj),
    new (winston.transports.File) (Object.assign({filename: 'logfile.log'}, logObj))
  ]
});

winstonError(logger, {
  decoratedLevels: ['error']
});

function initDI({serverSettings, dockerSettings, requestOptions, dbSettings, jwtSettings, database}, mediator) {
  mediator.once('init', () => {
    if (process.env.DEBUG === '1') {
      logger.level = 'debug';
    }
    if (process.env.TEST !== '1') {
      request = request.defaults({
        ca: requestOptions.CA,
        timeout: 20000
      });
      rcb = rcb.defaults({
        ca: requestOptions.CA,
        timeout: 20000
      });
    }
    mediator.on('db_client.ready', (client) => {
      jwtSettings.jwt = jwt;
      const container = createContainer();
      container.register({
        dockerSettings: asValue(dockerSettings),
        serverSettings: asValue(serverSettings),
        dbSettings: asValue(dbSettings),
        logger: asValue(logger),
        db_client: asValue(client),
        request: asValue(request),
        rcb: asValue(rcb),
        jwtSettings: asValue(jwtSettings)
      });
      
      mediator.emit('di.ready', container);
    });

    mediator.on('db_client.error', (err) => {
      mediator.emit('di.error', err);
    });

    database.connect(dbSettings, mediator);
    // Signals the database to startup.
    mediator.emit('boot.ready');
  });
};

module.exports.initDI = initDI;
