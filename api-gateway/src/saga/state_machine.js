'use strict';

/**
 *
 * name: the name of the state.
 * data: the data used to execute the state action.
 * function_name: the name of the function used to execute the state.
 */
function State(name, data, function_name) {
  this.name = name;
  this.data = data;
  this.function_name = function_name;
}

/**
 * 
 * forward_states: the states required to complete the saga.
 * compensating_state: the compensating states required to revert the saga,
 *   has a direct index correspondant with forward_states except the last index
 *   which can be empty.
 */
function SagaStateMachine(forward_states, compensating_states, function_map,
			              timeOutInterval=500) {
  this.forward_states = forward_states;
  this.compensating_states = compensating_states;
  this.function_map = function_map;
  this.timeOutCount = 10;
  this.timeOutCounter = 0;
  this.timeOutInterval = timeOutInterval; // In milliseconds
}

/**
 * Callback should accept the arguments:
 *   err: error during state machine execution. This value is null
 *     if state machine executed successfully.
 */
SagaStateMachine.prototype.setup = function(container, callback) {
  this.repo = container.cradle.repo;
  this.logger = container.cradle.logger;
  this.callback = callback;  
};

function stateSuccess() {
  if (this.is_compensating) {
    this.timeOutCounter = 0;
    this.currentStateIndex--;
    if (this.currentStateIndex < 0) {
      this.repo.removeEvent(this.eventId)
	    .then(() => {
	      this.callback(this.compensatingError);
	    })
	    .catch((err) => {
	      this.callback(err);
	    });
    } else {
      let nextState = this.compensating_states[this.currentStateIndex];
      this.repo.updateEvent(this.eventId, nextState)
	    .then(() => {
	      runStateMachine.call(this);
	    })
	    .catch((err) => {
	      this.callback(err);
	    });
    }
  } else {
    this.currentStateIndex++;
    if (this.currentStateIndex >= this.forward_states.length) {
      this.repo.removeEvent(this.eventId)
	    .then(() => {
	      this.callback(null);
	    })
	    .catch((err) => {
	      this.callback(err);
	    });
    } else {
      let nextState = this.forward_states[this.currentStateIndex];
      this.repo.updateEvent(this.eventId, nextState)
	    .then(() => {
          runStateMachine.call(this);
	    })
	    .catch((err) => {
          this.callback(err);
	    });
    }
  }
}

function stateFailure(err) {
  this.logger.error(err);
  if (this.is_compensating) {
    this.timeOutCounter++;
    if (this.timeOutCounter > this.timeOutCount) {
      let theEndError = new Error('Timeout when trying to execute compensating action, it is the end of the world?');
      this.logger.error(theEndError);
      this.callback(theEndError);
    } else {
      let that = this;
      setTimeout(function() {
	    runStateMachine.call(that);
      }, this.timeOutInterval);
    }
  } else {
    this.compensatingError = err;
    this.repo.setEventCompensating(this.eventId)
      .then(() => {
	    if (this.currentStateIndex == 0) {
          // No changes applied yet so we are done.
          this.repo.removeEvent(this.eventId)
            .then(() => {
	          this.callback(this.compensatingError);
            })
            .catch((err) => {
              this.logger.error("Unable to delete event with id: " + this.eventId);
              this.callback(this.compensatingError);
            });
	    } else {
          this.is_compensating = true;
          // Assuming that when current state fails that we don't have to
          // apply compensating action on the current state.
          this.currentStateIndex--;
          runStateMachine.call(this);
	    }
      })
      .catch((err) => {
	    this.callback(err);
      });
  }
}

function runStateMachine() {
  let currentState;
  if (this.is_compensating) {
    currentState = this.compensating_states[this.currentStateIndex];
  } else {
    currentState = this.forward_states[this.currentStateIndex];
  }
  let currentAction = this.function_map[currentState.function_name];
  currentAction(currentState.data)
    .then(() => {
      stateSuccess.call(this);
    })
    .catch((err) => {
      stateFailure.call(this, err);
    });
};

/**
 * Starts the state machine to run a distributed saga from beginning to end.
 *
 */
SagaStateMachine.prototype.startStateMachine = function() {
  this.is_compensating = false;
  this.currentStateIndex = 0;  
  this.repo.addEvent(this.forward_states, this.compensating_states,
		             this.forward_states[this.currentStateIndex])
    .then((eventId) => {
      this.logger.debug('eventId: ' + eventId);
      this.eventId = eventId;
      runStateMachine.call(this);
    })
    .catch((err) => {
      this.logger.debug('start state machine error');
      this.callback(err);
    });  
};

module.exports = {
  SagaStateMachine,
  State
};
