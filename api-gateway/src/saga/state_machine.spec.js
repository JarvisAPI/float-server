'use strict';

const {SagaStateMachine, State} = require('./');
const should = require('should');
const sinon = require('sinon');

const getStubRepo = () => {
  let idCounter = 0;
  let events = [];
  
  const updateEvent = (eventId, current_state) => {
    events[eventId].current_state = current_state;
    return Promise.resolve();
  };
  
  const removeEvent = (eventId) => {
    return Promise.resolve();
  };
  
  const setEventCompensating = (eventId) => {
    events[eventId].is_compensating = true;
    return Promise.resolve();
  };
  
  const addEvent = (forward_states, compensating_states, current_state) => {
    let event = {
      forward_states,
      compensating_states,
      is_compensating: false,
      current_state
    };
    events[idCounter] = event;
    let eventId = idCounter;
    idCounter++;
    return Promise.resolve(eventId);
  };

  return {updateEvent,
          removeEvent,
          setEventCompensating,
          addEvent};
};

const getStubLogger = () => {
  const error = (msg) => {
    console.log(msg);
  };

  const debug = (msg) => {
    console.log(msg);
  };

  return {error,
          debug};
};

describe('Saga State Machine', () => {
  const container = {
    cradle: {
      repo: getStubRepo(),
      logger: getStubLogger()
    }
  };
  
  it('test simple successful state transition', (done) => {
    let forward_states = [
      new State('state_0', null, 'state0'),
      new State('state_1', null, 'state1')
    ];
    let compensating_states = [
      new State('compensating_state_0', null, 'compensating0')
    ];
    let testValue = 0;
    let expectedTestValue = 1*11;
    let function_map = {
      state0: function(data) {
	    testValue++;
	    return Promise.resolve();
      },
      state1: function(data) {
	    testValue *= 11;
	    return Promise.resolve();
      },
      compensating0: function(data) {
      }
    };
    
    let testSaga = new SagaStateMachine(forward_states, compensating_states,
					function_map);
    testSaga.setup(container, (err) => {
      if (err) {
        done(err);
	return;
      }
      testValue.should.equal(expectedTestValue);
      done();
    });
    testSaga.startStateMachine();
  });

  it('test compensating states after beginning state fails', (done) => {
    let forward_states = [
      new State('state_0', null, 'state0'),
      new State('state_1', null, 'state1')
    ];
    let compensating_states = [
      new State('compensating_state_0', null, 'compensating0')
    ];
    let function_map = {
      state0: function(data) {
	    let error = new Error('test action failure, 2nd test');
	    error.type = 'customError';
	    return Promise.reject(error);
      }
    };    
    
    let testSaga = new SagaStateMachine(forward_states, compensating_states,
					function_map);
    testSaga.setup(container, (err) => {
      err.type.should.equal('customError');
      done();
    });
    testSaga.startStateMachine();
  });

  it('test compensating states after some forward state transitions', (done) => {
    let forward_states = [
      new State('state_0', 10, 'state0'),
      new State('state_1', 10, 'state1'),
      new State('state_2', null, 'state2')
    ];
    let compensating_states = [
      new State('compensating_state_0', null, 'compensating0'),
      new State('compensating_state_1', null, 'compensating1')
    ];
    let testValue = 0;
    let expectedTestValue = (10*10 + 25) / 5;

    let compensatingError = new Error('test action failure, 3rd test');
    compensatingError.type = 'compensatingError';
    const function_map = {
      state0: function(data) {
        testValue += data;
        return Promise.resolve();
      },
      state1: function(data) {
        testValue *= data;
        return Promise.resolve();
      },
      state2: function(data) {
        return Promise.reject(compensatingError);
      },
      compensating0: function(data) {
        testValue /= 5;
        return Promise.resolve();
      },
      compensating1: function(data) {
        testValue += 25;
        return Promise.resolve();
      }
    };
    
    let testSaga = new SagaStateMachine(forward_states, compensating_states,
					function_map);
    testSaga.setup(container, (err) => {
      err.type.should.equal('compensatingError');
      testValue.should.equal(expectedTestValue);
      done();
    });
    testSaga.startStateMachine();
  });

  it('failure when compensating should retry to see if can succeed', (done) => {
    let forward_states = [
      new State('state_0', 10, 'state0'),
      new State('state_1', 10, 'state1')
    ];
    let compensating_states = [
      new State('compensating_state_0', null, 'compensating0')
    ];    
    let retry = 0;
    let compensated = false;
    const function_map = {
      state0: function() {
        return Promise.resolve();
      },
      state1: function() {
        return Promise.reject(new Error('test action failure, 4th test'));
      },
      compensating0: function() {
	    if (retry === 0) {
	      retry++;
          return Promise.reject(new Error('test revert action failure, 4th test'));
	    } else {
	      compensated = true;
	      return Promise.resolve();
	    }
      }
    };
    let testSaga = new SagaStateMachine(forward_states, compensating_states,
					                    function_map, 0);
    testSaga.setup(container, (err) => {
      compensated.should.be.true();
      done();
    });
    testSaga.startStateMachine();    
  });

  it('test first state failed should remove event from database', (done) => {
    let forward_states = [
      new State('state_0', null, 'state0'),
      new State('state_1', null, 'state1')
    ];
    let compensating_states = [
      new State('compensating_state_0', null, 'compensating0')
    ];

    let function_map = {
      state0: function() {
	    let error = new Error('test action failed id #1001231');
	    error.type = 'customError';
	    return Promise.reject(error);
      },
      state1: function () {
        return Promise.reject(new Error("should not reach here"));        
      },
      compensating0: function () {
        return Promise.reject(new Error("should not reach here"));
      }
    };

    let repo = getStubRepo();
    repo.removeEvent = sinon.spy(repo.removeEvent);

    const container = {
      cradle: {
        repo,
        logger: getStubLogger()
      }
    };
    
    let testSaga = new SagaStateMachine(forward_states, compensating_states,
					function_map, 0);
    testSaga.setup(container, (err) => {
      try {
        should(repo.removeEvent.calledOnce).be.true();
      } catch(err) {
        done(err);
        return;
      }
      done();
    });
    testSaga.startStateMachine();    
  });
  
});
