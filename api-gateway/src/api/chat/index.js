'use strict';

const gateway_chat = require('./gateway_chat');

module.exports = (app, container) => {
  gateway_chat(app, container);
};
