'use strict';

const {proxy, stripPrefix, getRoute} = require('../gateway');
const hal = require('hal');

const success = (response) => {
  if (!response) {
    return true;
  }
  let statusCode = response.statusCode;
  return statusCode >= 200 && statusCode < 300;
};

module.exports = (app, container) => {
  let routes = container.cradle.routes;
  let request = container.cradle.request;
  let logger = container.cradle.logger;
  let pa = container.cradle.permissionAuthority;

  const chatProxyWithPermission = (req, res, next) => {
    let authOperation = req.query.shop_id ? 'shop-permission' : 'pass-permission';
    logger.debug('authOperation: ' + authOperation);
    
    pa.checkUserPermission(req.user, {shop_id: req.query.shop_id}, authOperation)
      .then((authorized) => {
        if(authorized) {
          let proxyBaseUrl = null;
          if (req.proxyBaseUrl) {
            proxyBaseUrl = req.proxyBaseUrl;
          } else {
            proxyBaseUrl = stripPrefix(req.url, container);
          }
          let url = getRoute(routes, 'chat-service') + proxyBaseUrl;
          if (req.onProxyComplete) {
            proxy(container, url, req, res, req.onProxyComplete);
          } else {
            proxy(container, url, req, res, (response, body) => {
              let resource = new hal.Resource(body, req.url);
              res.status(200).json(resource);
            });
          }
        }
        else{
          res.status(401).send();
        }
      })
      .catch(next);
  };

  app.get('/gateway/chat/user/dialog', pa.getUser, (req, res, next) => {
    let jid = req.query.shop_id ?  req.query.shop_id : req.user.username;
    logger.debug('body: ' + jid);
    req.proxyBaseUrl = `/chat/user/${jid}/dialog`;
    next();
  }, chatProxyWithPermission);

  app.get('/gateway/chat/token', pa.getUser, (req, res, next) => {
    let jid = req.query.shop_id ?  req.query.shop_id : req.user.username;
    logger.debug('body: ' + jid);
    req.proxyBaseUrl = `/chat/user/${jid}/password`;
    req.onProxyComplete = (response, body) => {
      let responseBody = {chat_token: body.auth_token};
      if(body.authToken){
        responseBody = {chat_token: body.authToken};
      }
      let resource = new hal.Resource(responseBody, req.url);
      res.status(200).json(resource);
    };
    next();    
  }, chatProxyWithPermission);

  app.put('/gateway/chat/user/dialog/:barePeer/:count', pa.getUser, (req, res, next) => {
    let jid = req.query.shop_id ?  req.query.shop_id : req.user.username;
    logger.debug('body: ' + jid);
    req.proxyBaseUrl = `/chat/user/${jid}/dialog/${req.params.barePeer}/${req.params.count}`;
    next();    
  }, chatProxyWithPermission);

  app.post('/gateway/chat/user/notificationToken/:notificationToken', pa.getUser, (req, res, next) => {
    let jid = req.query.shop_id ?  req.query.shop_id : req.user.username;
    logger.debug('body: ' + jid);
    req.proxyBaseUrl = `/chat/user/${jid}/notificationToken/${req.params.notificationToken}`;
    next();    
  }, chatProxyWithPermission);

  app.post('/gateway/chat/user/dialog/:barePeer/image', pa.getUser, (req, res, next) => {
    let jid = req.query.shop_id ?  req.query.shop_id : req.user.username;
    logger.debug('body: ' + jid);
    req.proxyBaseUrl = `/chat/user/${jid}/dialog/${req.params.barePeer}/image`;
    next();    
  }, chatProxyWithPermission);

  app.get('/gateway/chat/:users/profile', (req, res, next) => {
    let users = req.params.users
    console.log(users)
    let shopServiceUrl = getRoute(routes, "shop-service") + `/shop/${users}/main-image`
    let userServiceUrl = getRoute(routes, "user-service") + `/user/${users}/profile_images`
    console.log(shopServiceUrl)
    console.log(userServiceUrl)
    let shopMainImageReq = new Promise((resolve, reject) => {
      request(shopServiceUrl, function(err, response, body) {
        if(err || !success(response)) {
          if (response) {
            reject(response.statusCode);
          } else {
	    reject(500);	  
            logger.error("shopMainImageReq: response is undefined");
          }
        }
        else{
          resolve(body);
        }
      });
    });

    let userProfileImageReq = new Promise((resolve, reject) => {
      request(userServiceUrl, function(err, response, body) {
        if(err || !success(response)) {
          if (response) {
            reject(response.statusCode);
          } else {
	    reject(500);
            logger.error("userProfileImageReq: response is undefined");
          }
        }
        else{
          resolve(body);
        }
      });
    });

    let promises = [shopMainImageReq, userProfileImageReq];
    Promise.all(promises)
    .then(function(values) {
      let shopResult = JSON.parse(values[0]);
      let userResult = JSON.parse(values[1]);
      let result = {}
      result.user = userResult
      result.shop = shopResult
      let resource = new hal.Resource(result, req.url);
      res.status(200).json(resource);
    })
    .catch(function(error) {
      res.status(400).send();
    });
  })
};
