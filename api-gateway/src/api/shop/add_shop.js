'use strict';

const {SagaStateMachine, State} = require('../../saga');
const bodyParser = require('body-parser');
const {getRoute} = require('../gateway');

var request, routes, logger;

const function_map = {
  addShopToUser: function(addShopToUserBody) {
    logger.debug('Adding shop to user');
    return request.post({
      url: getRoute(routes, 'user-service') + '/user/shops',
      body: addShopToUserBody,
      json: true
    });
  },

  makeShopChat: function(makeShopChatBody) {
    logger.debug('Making chat user');
    return request.post({
      url: getRoute(routes, 'chat-service') + '/chat/user',
      json: true,
      body: makeShopChatBody
    });
  },  

  makeShop: function(makeShopBody) {
    logger.debug('Making shop');
    return request.post({
      url: getRoute(routes, 'shop-service') + '/shop',
      body: makeShopBody,
      json: true
    });
  },
  
  makeShopIcon: function(makeShopIconBody) {
    logger.debug('Making shop icon');
    return request.post({
      url: getRoute(routes, 'map-service') + '/map/icons',
      json: true,
      body: makeShopIconBody
    });
  },
  
  removeShopFromUser: function(addShopToUserBody) {
    logger.debug('Removing shop from user');
    return request.delete({
      url: getRoute(routes, 'user-service') + '/user/shops',
      json: true,
      body: addShopToUserBody
    });
  },


  removeShopChat: function(makeShopChatBody) {
    logger.debug('Removing shop chat');
    return request.delete({
      url: getRoute(routes, 'chat-service') + '/chat/user',
      json: true,
      body: makeShopChatBody
    });
  },  
  
  removeShop: function(shop_id) {
    logger.debug('Removing shop');
    return request.delete({
      url: getRoute(routes, 'shop-service') + `/shop/${shop_id}`
    });
  }
};

module.exports = (app, container) => {
  const repo = container.cradle.repo;
  request = container.cradle.request;
  logger = container.cradle.logger;
  routes = container.cradle.routes;
  const pa = container.cradle.permissionAuthority;
  
  app.post('/gateway/shop', bodyParser.json(), pa.getUser, (req, res, next) => {
    /* Creates a shop require actions:
     *   add_shop_to_user: add shop to user
     *   make_shop: make shop on shop service
     *   make_shop_icon: make shop icon on map service
     */
    let shopBody = req.body.shop;
    let addShopToUserBody = {
      user: {
        username: req.user.username
      },
      shop: {
        shop_name: shopBody.shop_name,
        shop_id: shopBody.shop_id
      }
    };
    let makeShopBody = {
      shop: {
        shop_name: shopBody.shop_name,
        shop_description: shopBody.shop_description,
        shop_id: shopBody.shop_id,
        latitude: shopBody.shop_icon_latitude,
        longitude: shopBody.shop_icon_longitude
      }
    };
    let makeShopIconBody = {
      icon: {
        icon_id: shopBody.shop_id,
        icon_type: shopBody.shop_icon_type,
        icon_latitude: shopBody.shop_icon_latitude,
        icon_longitude: shopBody.shop_icon_longitude
      }
    };
    
    let makeShopChatBody = {
      username: shopBody.shop_id
    };

    let forward_states = [
      new State('add_shop_to_user', addShopToUserBody, 'addShopToUser'),
      new State('make_shop_chat', makeShopChatBody, 'makeShopChat'),
      new State('make_shop', makeShopBody, 'makeShop'),
      new State('make_shop_icon', makeShopIconBody, 'makeShopIcon')
    ];
    let compensating_states = [
      new State('remove_shop_from_user', addShopToUserBody, 'removeShopFromUser'),
      new State('remove_shop_chat', makeShopChatBody, 'removeShopChat'),
      new State('remove_shop', shopBody.shop_id, 'removeShop')
    ];
      
    let saga = new SagaStateMachine(forward_states, compensating_states, function_map);
    
    saga.setup(container, (err) => {
      if (err) {
        let message = err.error ? err.error : '';
        let statusCode = 0;
        
        if (err.statusCode) {
          statusCode = err.statusCode;
        } else if (err.status) {
          statusCode = err.status;
        }
        
        if (statusCode !== 0) {
          let errorBody = err.error ? err.error : {};
	      res.status(statusCode).json(errorBody);
        } else {
          res.status(500).send();
        }
      } else {
        res.status(200).send();
      }
    });
    saga.startStateMachine();
  });
};
