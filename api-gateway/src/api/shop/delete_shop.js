"use strict";

const {getRoute} = require("../gateway");

module.exports = (app, container) => {
  const repo = container.cradle.repo;
  const request = container.cradle.request;
  const routes = container.cradle.routes;
  const pa = container.cradle.permissionAuthority;
  const checkUserPermission = pa.checkUserPermission;
  const logger = container.cradle.logger;
  
  app.delete("/gateway/shop/:shop_id", pa.getUser, (req, res, next) => {
    checkUserPermission(req.user, {shop_id: req.params.shop_id}, "shop-permission")
      .then(authorized => {
	if (authorized) {
	  deleteShop(req, res, next);
	} else {
	  res.sendStatus(401);
	}
      })
      .catch(next);
  });

  function deleteShop(req, res, next) {
    let promises = [];
    let checkErr = response => {
      if (response.statusCode >= 500) {
	throw new Error("Service error on delete!");
      }
      return;
    };
    promises.push(
      request
	.delete({
	  url: getRoute(routes, "shop-service") + `/shop/${req.params.shop_id}`,
	  resolveWithFullResponse: true
	})
	.then(checkErr));
    
    promises.push(
      request
	.delete({
	  url: getRoute(routes, "map-service") + "/map/icons",
	  json: true,
	  body: {icon: {icon_id: req.params.shop_id}},
	  resolveWithFullResponse: true
	})
	.then(checkErr));
    
    promises.push(
      request
	.delete({
	  url: getRoute(routes, "chat-service") + "/chat/user",
	  json: true,
	  body: {username: req.params.shop_id},
	  resolveWithFullResponse: true
	})
	.then(checkErr));
    
    Promise.all(promises)
      .then(() => request
	    .delete({
	      url: getRoute(routes, "user-service") + "/user/shops",
	      json: true,
	      body: {user: {username: req.user.username},
                     shop: {shop_id: req.params.shop_id}}
	    }))
      .then(() => res.sendStatus(200))
      .catch(next);
  }
};
