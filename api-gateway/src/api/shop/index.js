"use strict";

const add_shop = require("./add_shop.js");
const delete_shop = require("./delete_shop.js");
const gateway_shop = require("./gateway_shop.js");

const api = (app, container) => {
  add_shop(app, container);
  delete_shop(app, container);
  gateway_shop(app, container);
};

module.exports = api;
