"use strict";

const hal = require("hal");
const {stripPrefix, proxy, getRoute} = require("../gateway");

/**
 * Determines whether a request is successful by the response
 * status code. If the response is null or undefined or it has
 * a status code in the 200 range then true is returned. Otherwise
 * false is returned.
 */
const success = (response) => {
  if (!response) {
    return true;
  }
  let statusCode = response.statusCode;
  return statusCode >= 200 && statusCode < 300;
};

const setCacheHeader = (res, maxAge) => {
  res.set("Cache-Control", "public, max-age=" + maxAge);
};

module.exports = (app, container) => {
  let request = container.cradle.request;
  let routes = container.cradle.routes;
  let logger = container.cradle.logger;
  let pa = container.cradle.permissionAuthority;
  let checkUserPermission = pa.checkUserPermission;

  app.get("/gateway/shop/:shop_id", (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let shopServiceUrl = getRoute(routes, "shop-service") + proxyBaseUrl;
    let userServiceUrl = getRoute(routes, "user-service") + "/user" + proxyBaseUrl;
    let shopReq = new Promise((resolve, reject) => {
      request(shopServiceUrl, function(err, response, body) {
        if(err || !success(response)) {
          if (response) {
            reject(response.statusCode);
          } else {
            logger.error("shopReq: response is undefined");
            reject(500);
          }
        }
        else{
          resolve(body);
        }
      });
    });
    let shopOwnerUsernameReq = new Promise((resolve, reject) => {
      request(userServiceUrl, function(err, response, body) {
        if(err || !success(response)) {
          if (response) {
            reject(response.statusCode);
          } else {
            logger.error("shopOwnerUsernameReq: response is undefined");
            reject(500);
          }
        }
        else{
          resolve(body);
        }
      });
    });
    let promises = [shopReq, shopOwnerUsernameReq];
    Promise.all(promises)
    .then(function(values) {
      let shopResult = JSON.parse(values[0]);
      let userResult = JSON.parse(values[1]);
      shopResult.usernames = userResult.usernames;
      let resource = new hal.Resource(shopResult, req.url);
      setCacheHeader(res, 300);
      res.status(200).json(resource);
    })
    .catch(function(error) {
      res.status(error).send();
    });
  });

  app.get("/gateway/shop/:shop_id/product/:offset/:limit", (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let url = getRoute(routes, "shop-service") + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      setCacheHeader(res, 300);
      res.status(200).json(resource);
    });
  });

  app.get("/gateway/product/:product_name/:offset/:limit", (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let url = getRoute(routes, "shop-service") + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      setCacheHeader(res, 300);
      res.status(200).json(resource);
    });
  });

  app.get("/gateway/shop/:shop_id/product/:product_id", (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let url = getRoute(routes, "shop-service") + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      setCacheHeader(res, 300);
      res.status(200).json(resource);
    });
  });

  const shopProxyWithPermission = (req, res, next) => {
    // Authorization:
    //   make sure user modifying shop is authorized by:
    //     - Asking user-service to see if user has permission for shop    
    checkUserPermission(req.user, {shop_id: req.params.shop_id}, "shop-permission")
      .then((authorized) => {
        if (authorized) {
          let proxyBaseUrl = stripPrefix(req.url, container);
          let url = getRoute(routes, "shop-service") + proxyBaseUrl;
          proxy(container, url, req, res, (response, body) => {
            let resource = new hal.Resource(body, req.url);
            res.status(200).json(resource);
          });
        } else {
          res.status(401).send();
        }
      })
      .catch(next);    
  };

  app.put("/gateway/shop/:shop_id", pa.getUser, shopProxyWithPermission);

  app.delete("/gateway/shop/:shop_id/product/:product_id", pa.getUser, shopProxyWithPermission);

  app.post("/gateway/shop/:shop_id/image", pa.getUser, shopProxyWithPermission);

  app.post("/gateway/shop/:shop_id/main-image", pa.getUser, shopProxyWithPermission);

  app.put("/gateway/shop/:shop_id/main-image", pa.getUser, shopProxyWithPermission);

  app.delete("/gateway/shop/:shop_id/image/:image_files", pa.getUser, shopProxyWithPermission);

  app.post("/gateway/shop/:shop_id/product", pa.getUser, shopProxyWithPermission);

  app.post("/gateway/shop/:shop_id/product/:product_id/main-image", pa.getUser, shopProxyWithPermission);

  app.post("/gateway/shop/:shop_id/product/:product_id/image", pa.getUser, shopProxyWithPermission);

  app.put("/gateway/shop/:shop_id/product/:product_id", pa.getUser, shopProxyWithPermission);

  app.put("/gateway/shop/:shop_id/product/:product_id/main-image", pa.getUser, shopProxyWithPermission);

  app.delete("/gateway/shop/:shop_id/product/:product_id/main-image", pa.getUser, shopProxyWithPermission);

  app.put("/gateway/shop/:shop_id/product/:product_id/images", pa.getUser, shopProxyWithPermission);
};
