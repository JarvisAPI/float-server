'use strict';

const gateway_user = require('./gateway_user');
const add_user = require('./add_user');
const get_user_shops = require('./get_user_shops');

const api = (app, container) => {
  add_user(app, container);
  gateway_user(app, container);
  get_user_shops(app, container);
};

module.exports = api;
