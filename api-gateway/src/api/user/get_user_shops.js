'use strict';

const {getRoute} = require('../gateway');
const hal = require('hal');

module.exports = (app, container) => {
  let routes = container.cradle.routes;
  let logger = container.cradle.logger;  
  let request = container.cradle.request;
  
  app.get('/gateway/user/:username/shops', (req, res, next) => {
    let sendBackShops = (shops, icons) => {
      let response = [];
      for (let i = 0; i < shops.length; i++) {
        let shop = shops[i];
        let icon = icons.find((element) => {
          return element.icon_id === shop.shop_id;
        });
        if (icon) {
          shop.shop_icon_latitude = icon.icon_latitude;
          shop.shop_icon_longitude = icon.icon_longitude;
          shop.shop_icon_type = icon.icon_type;
        }
        response.push(shop);
      }
      let resource = new hal.Resource({shops: response}, req.url);
      resource.link('delete_product', {href: '/gateway/shop/{shop_id}/product/{product_id}', templated: true});
      resource.link('shop_products', {href: '/gateway/shop/{shop_id}/product/{offset}/{limit}', templated: true});      
      resource.link('make_product', {href: '/gateway/shop/{shop_id}/product', templated: true});
      resource.link('upload_product_main_image', {href: '/gateway/shop/{shop_id}/product/{product_id}/main-image', templated: true});
      resource.link('upload_product_image', {href: '/gateway/shop/{shop_id}/product/{product_id}/image', templated: true});
      resource.link('edit_shop', {href: '/gateway/shop/{shop_id}', templated: true});
      resource.link('edit_product', {href: '/gateway/shop/{shop_id}/product/{product_id}', templated: true});
      resource.link('swap_product_main_image', {href: '/gateway/shop/{shop_id}/product/{product_id}/main-image', templated: true});
      resource.link('delete_product_main_image', {href: '/gateway/shop/{shop_id}/product/{product_id}/main-image', templated: true});
      resource.link('delete_product_images', {href: '/gateway/shop/{shop_id}/product/{product_id}/images', templated: true});
      res.status(200).json(resource);
    };
    let getMoreShopInfo = (shopsResponse) => {
      let shops = JSON.parse(shopsResponse).shops;
      let shopIds = [];
      for (let i = 0; i < shops.length; i++) {
        shopIds.push(shops[i].shop_id);
      }
      let iconsByIdPath = `/map/icons/${JSON.stringify(shopIds)}`;
      let iconsByIdUrl = getRoute(routes, 'map-service') + iconsByIdPath;
      request.get({
        url: iconsByIdUrl
      })
        .then((iconsResponse) => {
          sendBackShops(shops, JSON.parse(iconsResponse).icons);
        })
        .catch((err) => {
          logger.error(err);
          res.sendStatus(404);
        });
    };
    let userShopsPath = `/user/${req.params.username}/shops`;
    let userShopUrl = getRoute(routes, 'user-service') + userShopsPath;    
    request.get({
      url: userShopUrl
    })
      .then(getMoreShopInfo)
      .catch((err) => {
        logger.error(err);
        res.sendStatus(404);
      });
  });  
};
