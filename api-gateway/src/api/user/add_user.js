'use strict';

const {SagaStateMachine, State} = require('../../saga');
const bodyParser = require('body-parser');
const {getRoute} = require('../gateway');

const minUsernameLength = 6;
const maxUsernameLength = 30;
const minPasswordLength = 8;
const maxPasswordLength = 50;

var request, routes, logger;

const function_map = {
  makeAuthUser: function(makeAuthUserBody)  {
    logger.debug('Making auth user');
    return request.post({
      url: getRoute(routes, 'auth-service') + '/auth/user',
      json: true,
      body: makeAuthUserBody
    });
  },

  makeUser: function(makeUserBody) {
    logger.debug('Making user');
    return request.post({
      url: getRoute(routes, 'user-service') + '/user',
      json: true,
      body: makeUserBody
    });
  },

  makeChatUser: function(makeChatUserBody) {
    logger.debug('Making chat user');
    return request.post({
      url: getRoute(routes, 'chat-service') + '/chat/user',
      json: true,
      body: makeChatUserBody
    });     
  },
  
  removeAuthUser: function(makeAuthUserBody) {
    logger.debug('Removing auth user');
    return request.delete({
      url: getRoute(routes, 'auth-service') + '/auth/user',
      json: true,
      body: makeAuthUserBody
    });
  },

  removeUser: function(makeUserBody) {
    logger.debug('Removing user');
    return request.delete({
      url: getRoute(routes, 'user-service') + '/user',
        json: true,
      body: makeUserBody
    });
  }
};  

module.exports = (app, container) => {
  const repo = container.cradle.repo;
  request = container.cradle.request;
  logger = container.cradle.logger;
  routes = container.cradle.routes;

  const createUser = (handlerFunc, userBody)=> {
    
   let makeAuthUserBody = {
        user: {
          username: userBody.username,
          password: userBody.password,
          email: userBody.email,
	  oauth: userBody.oauth
        }
      };
    let makeUserBody = {
        user: {
          username: userBody.username,
          name: userBody.name
      }
    };
    let makeChatUserBody = {
      username: userBody.username
    };

    let forward_states = [
      new State('make_auth_user', makeAuthUserBody, 'makeAuthUser'),
      new State('make_user', makeUserBody, 'makeUser'),
      new State('make_chat_user', makeChatUserBody, 'makeChatUser')
    ];
    let compensating_states = [
      new State('remove_auth_user', makeAuthUserBody, 'removeAuthUser'),
      new State('remove_user', makeUserBody, 'removeUser')
    ];

    let saga = new SagaStateMachine(forward_states, compensating_states, function_map);

    saga.setup(container,handlerFunc);
    saga.startStateMachine();
  }

  app.post('/gateway/user', bodyParser.json(), (req, res, next) => {
    let userBody = req.body.user;
    userBody.oauth = false;
    let usernameLength = userBody.username.length;
    let passwordLength = userBody.password.length;
    if (usernameLength < minUsernameLength || usernameLength > maxUsernameLength ||
        passwordLength < minPasswordLength || passwordLength > maxPasswordLength) {
      res.status(400).json({error: "Invalid input"});
      return;
    }
    userBody.username = userBody.username.toLowerCase()
    logger.debug("creating username:"+ userBody.username + " email:"+ userBody.email)
    let handler = (err,result) => {
      if (err) {
        if (err.statusCode) {
                console.log(err.error)
          res.status(err.statusCode).json(err.error);
        } else {
          res.status(500).send();
        }
      } else {
        res.status(200).send();
      }
    }
    createUser(handler, userBody)
  });

  return{
    createUser
  }
};
