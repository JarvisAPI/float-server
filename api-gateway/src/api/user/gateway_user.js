'se strict';

const {proxy, stripPrefix, getRoute} = require('../gateway');
const hal = require('hal');
const add_user  = require('./add_user');
const bodyParser = require('body-parser');
const FACEBOOK_OAUTH_TYPE = "facebook"
const GOOGLE_OAUTH_TYPE = "google"


module.exports = (app, container) => {
  let routes = container.cradle.routes;
  let logger = container.cradle.logger;
  let pa = container.cradle.permissionAuthority;
  let request = container.cradle.request;
  let createUser = add_user(app, container).createUser;

  const getAuthLinks = (resource) => {
    resource.link('make_shop', '/gateway/shop');
    resource.link('user_shops', {href: '/gateway/user/{username}/shops', templated: true});
    resource.link('get_chat_token', '/gateway/chat/token');
    resource.link('add_user_profile_image', {href: '/gateway/user/profile/profile_image'});
    resource.link('add_user_background_image', {href: '/gateway/user/profile/background'});
    resource.link('get_chat_dialogs', {href: '/gateway/chat/user/dialog'});
    resource.link('set_conversation_message_count', {href: '/gateway/chat/user/dialog/{barePeer}/{count}', templated:true});
    resource.link('update_chat_notification_token', {href: '/gateway/chat/user/notificationToken/{notification_token}', templated:true});
    resource.link('send_image_chat', {href: '/gateway/chat/user/dialog/{barePeer}/image', templated:true});
  }

  app.post('/gateway/auth/token', (req, res, next) => {
    let url = getRoute(routes, 'auth-service') + "/auth/token"; // hardcoded to prevent injection
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      getAuthLinks(resource)
      res.status(200).json(resource);
    });
  });

  app.post('/gateway/auth/oauth', bodyParser.json(), (req, res, next) => {
    var username;	  
    let returnRequest = () =>{
      let url = getRoute(routes, 'auth-service') + "/auth/token?oauth=true";
      request.post({
         url,
	 json:{username}
       }).then((response) => {
	 let resource = new hal.Resource(response, req.url);
         getAuthLinks(resource)
	 res.status(200).json(resource);
       })
       .catch((err) => {
         res.status(500).send()
       })
    }

    let handler = (err,result) => {
      if (err){
        if (err.statusCode == 409) {
	  returnRequest(username)
        } else {
          res.status(500).json(err.error);
        }
      } else {
        returnRequest(username)
      }
    }

    let facebookAuth = (access_token)=> {
      fbVerUrl = "https://graph.facebook.com/me?access_token=" + access_token
      request.get({
        url:fbVerUrl,
	ca:null
      }).then((response) => {  
	resp = JSON.parse(response)
	if(!resp || !resp.name || !resp.id){
	  res.status(401).send()
	  return
	} else {
	  username = resp.id      
          user = { name:resp.name, username, oauth:true}
          createUser(handler, user)
	}
      })
      .catch((err) => {
        res.status(401).send() 
      })
    }

    let googleAuth = (access_token) => {
      googVerUrl = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + access_token
      request.get({
        url:googVerUrl,
	ca:null
      }).then((response) => {
        resp = JSON.parse(response)
	if(!resp || !resp.name || resp.id){
          res.status(401).send()
	} else {
	  username = resp.sub
          user = { name:resp.name, username, oauth:true}
          createUser(handler, user)
	}
      })
      .catch((err) => {
        res.status(401).send() 
      })
    }

    let oauthType = req.body.type;
    logger.debug("oauth type:"+oauthType)
    if(oauthType === GOOGLE_OAUTH_TYPE){
      googleAuth(req.body.access_token)
    } else if(oauthType === FACEBOOK_OAUTH_TYPE){
      facebookAuth(req.body.access_token)
    } else{
      res.status(400).send()
    }
  })


  app.post('/gateway/user/profile/profile_image', pa.getUser, (req, res, next) => {
    logger.debug('adding profile picture');
    let proxyBaseUrl = `/user/${req.user.username}/profile/profile_image`;
    let url = getRoute(routes, 'user-service') + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      res.status(200).json(resource);
    });
  });
  
  app.post('/gateway/user/profile/background', pa.getUser, (req, res, next) => {
    logger.debug('adding background picture');
    let proxyBaseUrl = `/user/${req.user.username}/profile/background`;
    let url = getRoute(routes, 'user-service') + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      res.status(200).json(resource);
    });
  });

  app.get('/gateway/user/:username/profile',(req, res, next) => {
    logger.debug('getting profile');
    let proxyBaseUrl = `/user/${req.params.username}/profile`;
    let url = getRoute(routes, 'user-service') + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      res.status(200).json(resource);
    });
  });
};
