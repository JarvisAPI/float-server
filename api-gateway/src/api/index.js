'use strict';

const gateway = require('./gateway').api;

const shop_api = require('./shop');
const map_api = require('./map');
const user_api = require('./user');
const chat_api = require('./chat');
const permissionAuthority = require('./permission_authority');
const {asValue} = require('awilix');

const api = (app, container) => {
  container.register('permissionAuthority', asValue(permissionAuthority(container)));
  gateway(app, container);
  map_api(app, container);
  shop_api(app, container);
  user_api(app, container);
  chat_api(app, container);
};

module.exports = api;
