'use strict';

const gateway_map = require('./gateway_map');

const api = (app, container) => {
  gateway_map(app, container);
};

module.exports = api;
