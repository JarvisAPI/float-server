'use strict';

const hal = require('hal');
const {proxy, stripPrefix, getRoute} = require('../gateway');

module.exports = (app, container) => {
  const routes = container.cradle.routes;
  const logger = container.cradle.logger;
  
  app.get('/gateway/map/icons/:latitude/:longitude', (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let url = getRoute(routes, 'map-service') + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      res.status(200).json(resource);
    });
  });

  app.get('/gateway/map/icons/:latitude/:longitude/:min_distance/:max_distance/:offset/:limit', (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let url = getRoute(routes, 'map-service') + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      res.status(200).json(resource);
    });
  });
};
