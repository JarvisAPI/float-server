'use strict';

const {getRoute} = require('./gateway');

module.exports = (container) => {
  const getUser = (req, res, next) => {
    let logger = container.cradle.logger;
    let token = checkAuthHeader(req);
    verifyToken(token)
      .then((decoded) => {
        req.user = {username: decoded.sub};
        next();
      })
      .catch((err) => {
        logger.error(err);
        res.status(401).send();
      });
  };

  const getUserAuthOptional = (req, res, next) => {
    let logger = container.cradle.logger;
    let token = checkAuthHeader(req);
    verifyToken(token)
      .then((decoded) => {
        req.user = decoded.sub;
        next();
      })
      .catch((err) => {
        next();
      });
  };

  const verifyToken = (token) => {
    if (!token) {
      return Promise.reject(new Error("No token in header"));
    }
    let jwt = container.cradle.jwtSettings.jwt;
    let publicKey = container.cradle.jwtSettings.publicKey;
    return new Promise((resolve, reject) => {
      jwt.verify(token, publicKey, (err, decoded) => {
        if (err) {
          reject(err);
        } else {
          resolve(decoded);
        }
      });
    });
  };

  /**
   * Check that the authorization header of a request is valid.
   * Returns the jwt access token found in the header if there is a valid token field,
   *   or null if the authorization field is invalid.
   */
  const checkAuthHeader = (req) => {
    let authHeader = req.get('Authorization');
    if (authHeader) {
      let authValues = authHeader.split(' ');
      if (authValues[0] == 'Bearer') {
        return authValues[1];
      }
    }
    return null;
  };

  /**
   * req: the request to retrive authorization information from.
   * grant: the data that explains what user seek access to.
   */
  const checkUserPermission = (user, grant, permission) => {
    var logger = container.cradle.logger;
    return new Promise((resolve, reject) => {
      switch(permission) {
        case "shop-permission":
          logger.debug("checking shop permission");
          checkShopPermission(user, grant, resolve, reject);
          break;        
        case "pass-permission":
          logger.debug("passing permission check");
          resolve(true);
          break;
        default:
          resolve(false);
      }
    });
  };

  const checkShopPermission = (user, grant, resolve, reject) => {
    let request = container.cradle.rcb;
    let url = getRoute(container.cradle.routes, 'user-service') + `/user/${user.username}/shops`;
    request.get({url}, (err, response, body) => {
      if (err) {
        reject(err);
      } else {
        try {
          body = JSON.parse(body);
          let shops = body.shops;
          let containsShop = false;
          for (let i = 0; i < shops.length; i++) {
            if (shops[i].shop_id === grant.shop_id) {
              containsShop = true;
              break;
            }
          }
          resolve(containsShop);          
        } catch(err) {
          reject(err);
          return;
        }    
      }
    });
  };

  return {
    getUser,
    getUserAuthOptional,
    checkUserPermission
  };
};
