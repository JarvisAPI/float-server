"use strict";

const hal = require("hal");
const urlParser = require('url');

const stripPrefix = (baseUrl, container) => {
  return baseUrl.substring(container.cradle.serverSettings.prefix.length);
};

const getRoute = (routes, serviceName) => {
  if (routes[serviceName]) {
    return routes[serviceName].target;
  }
  return "";
};

/**
 * Proxy json requests.
 */
const proxy = (container, url, req, res, middleware) => {
  let request = container.cradle.rcb;
  let logger = container.cradle.logger;
  let stream = request({url, json: true}, (err, response, body) => {
    if (err) {
      logger.error(err);
      res.status(500).send();
    } else {
      if (response.statusCode >= 300) {
        res.status(response.statusCode).send(body);
        return;
      }
      middleware(response, body);
    }
  });
  req.pipe(stream);
};

const api = (app, container) => {
  let routes = container.cradle.routes;
  let request = container.cradle.request;
  let logger = container.cradle.logger;
  let pa = container.cradle.permissionAuthority;

  app.get("/", (req, res, next) => {
    let resource = new hal.Resource({}, req.url);
    resource.link("default_config", {href: "/gateway/config"});

    resource.link("map_icons", {href: "/gateway/map/icons/{latitude}/{longitude}", templated: true});
    resource.link("login", "/gateway/auth/token");
    resource.link("sign_up", "/gateway/user");
    resource.link("oauth", "/gateway/auth/oauth");
    resource.link("shop_info", {href: "/gateway/shop/{shop_id}", templated: true});
    resource.link("image", {href: "/gateway/image/{image_file}", templated: true});
    resource.link("user_shops", {href: "/gateway/user/{username}/shops", templated: true});
    resource.link("user_profile", {href: "/gateway/user/{username}/profile", templated: true});
    resource.link("nearby_shops", {href: "/gateway/map/icons/{latitude}/{longitude}/{min_distance}/{max_distance}/{offset}/{limit}", templated: true});
    resource.link("shop_products", {href: "/gateway/shop/{shop_id}/product/{offset}/{limit}", templated: true});
    resource.link("search_products", {href: "/gateway/product/search"});
    resource.link("product_details", {href: "/gateway/shop/{shop_id}/product/{product_id}", templated: true});
    resource.link("delete_shop", {href: "/gateway/shop/{shop_id}", templated: true});
    resource.link("upload_shop_image", {href: "/gateway/shop/{shop_id}/image", templated: true});
    resource.link("upload_shop_main_image", {href: "/gateway/shop/{shop_id}/main-image", templated: true});
    resource.link("swap_shop_main_image", {href: "/gateway/shop/{shop_id}/main-image", templated: true});
    resource.link("delete_shop_images", {href: "/gateway/shop/{shop_id}/image/{image_files}", templated: true});
    resource.link("get_chat_profiles", {href: "/gateway/chat/{users}/profile", templated: true});
    res.status(200).json(resource);
  });

  app.get("/gateway/config", (req, res, next) => {
    let body = {};
    body["max_shop_image_uploads"] = 10;
    body["max_product_image_uploads"] = 10;
    res.status(200).json(body);
  });

  app.get("/gateway/image/:image_file", pa.getUserAuthOptional, (req, res, next) => {
    let proxyBaseUrl = stripPrefix(urlParser.parse(req.url).pathname, container);
    let errorHandler = (err) => {
      logger.error(err);
      res.status(500).send();
    };
    // request library using callback is used, for more efficient streaming of images.
    let rcb = container.cradle.rcb;
    let url;

    const getImage = (url) => {
      rcb({
        url: url
      })
        .on("error", errorHandler)
        .pipe(res)
        .on("error", errorHandler);
    };    
    
    if(req.user){
      let shopId = req.query.shop_id;
      if(shopId){
        pa.checkUserPermission({username:req.user},{shop_id:shopId},"shop-permission")
          .then((authorized)=>{
	        if(authorized){
	          url = routes["image-service"].target + proxyBaseUrl + "?username=" + shopId;
              getImage(url);
	        }
            else {
              res.status(404);
            }
	      });
      } else{
        url = routes["image-service"].target + proxyBaseUrl + "?username=" + req.user;
        getImage(url);
      }
    }
    else{
      url = routes["image-service"].target + proxyBaseUrl;
      getImage(url);
    }
  });

  app.get("/gateway/product/search", (req, res, next) => {
    let proxyBaseUrl = stripPrefix(req.url, container);
    let url = getRoute(routes, "shop-service") + proxyBaseUrl;
    proxy(container, url, req, res, (response, body) => {
      let resource = new hal.Resource(body, req.url);
      res.status(200).json(resource);
    });
  });
};

module.exports = Object.assign({}, {proxy, stripPrefix, api, getRoute});
