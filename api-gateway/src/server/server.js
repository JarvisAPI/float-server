'use strict';

const express = require('express');
const spdy = require('spdy');
const api = require('../api/');
const helmet = require('helmet');

// container is a awilix container used for dependency injection.
const start = (container) => {
  return new Promise((resolve, reject) => {
    const {port, ssl} = container.cradle.serverSettings;
    const routes = container.cradle.routes;

    if (!routes) {
      reject(new Error('The server must be started with routes discovered'));
      return;
    }
    if (!port) {
      reject(new Error('The server must be started with an available port'));
      return;
    }

    const app = express();
    if (!process.env.PROD) {
      let morgan = require('morgan');
      app.use(morgan('dev'));
    }
    app.use(helmet());

    api(app, container);

    app.use((err, req, res, next) => {
      reject(new Error('Something went wrong!, err:' + err));
      res.status(500).send('Something went wrong!');
    });    

    if (process.env.TEST === '1') {
      const server = app.listen(port, () => resolve(server));
    } else {
      const server = spdy.createServer(ssl, app)
            .listen(port, () => resolve(server));
    }

  });
};

module.exports = Object.assign({}, {start});
