'use strict';

const {EventEmitter} = require('events');
const server = require('./server/server');
const docker = require('./docker/docker');
const di = require('./config');
const {asValue} = require('awilix');
const mediator = new EventEmitter();
const repository = require('./repository/repository');

const discoverDockerContainers = (container) => {
  let logger = container.cradle.logger;
  docker.discoverRoutes(container)
    .then((routes) => {
      logger.info('Connected. Starting server');
      container.register('routes', asValue(routes));
      return server.start(container);
    })
    .then((app) => {
      logger.info(`Connected to Docker: ${container.cradle.dockerSettings.host}`);
      logger.info(`Server started successfully, API Gateway running on port: ${container.cradle.serverSettings.port}.`);
      app.on('close', () => {
	logger.info('Server finished');
      });
    });
};

const bootServer = (container) => {
  let logger = container.cradle.logger;
  logger.info('db connected!');
  repository.connect(container)
    .then((repo) => {
      container.register('repo', asValue(repo));
      discoverDockerContainers(container);
    });
};

mediator.on('di.ready', (container) => {
  bootServer(container);
});

mediator.on('di.error', (err) => {
  throw err;
});

di.init(mediator);

mediator.emit('init');
