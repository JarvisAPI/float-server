'use strict';
const Docker = require('dockerode');

const discoverRoutes = (container) => {
  return new Promise((resolve, reject) => {
    const dockerSettings = container.resolve('dockerSettings');
    const docker = new Docker(dockerSettings);
    const logger = container.cradle.logger;

    const dockerImageNames = ['shop-service', 'map-service', 'auth-service', 'user-service',
                              'image-service', 'chat-service'];
    
    const getUpStreamUrl = (serviceDetails) => {
      const {PublicPort} = serviceDetails.Ports[0];
      return `${container.cradle.serverSettings.proto}://${dockerSettings.host}:${PublicPort}`;
    };

    const addRoute = (routes, details) => {
      routes[details.Labels.mTag] = {
	id: details.Id,
	name: details.Names[0].split('').splice(1).join(''),
	route: details.Labels.apiRoute,
	target: getUpStreamUrl(details)
      };
    };

    const avoidContainers = (service) => {
      let tag = service.Labels.mTag;
      if (!tag) {
        return true;
      }
      if (process.env.TEST === "1" && service.Labels.tester !== "1") {
        return true; // If we are testing then ignore containers without tester label.
      }
      let inImageNames = dockerImageNames.indexOf(tag) >= 0;
      return !inImageNames;
    };

    docker.listContainers((err, services) => {
      if (err) {
	reject(new Error('an error occured listing containers, err: ' + err));
        return;
      }

      const routes = new Proxy({}, {
	get(target, key) {
	  logger.info(`Get properties from -> "${key}" container`);
	  return Reflect.get(target, key);
	},
	set(target, key, value) {
	  logger.info(`Setting properties ${key} %j`, value);
	  return Reflect.set(target, key, value);
	}
      });

      services.forEach((service) => {
	if (!avoidContainers(service)) {
	  addRoute(routes, service);
	}
      });

      resolve(routes);
    });
  });
};

module.exports = Object.assign({}, {discoverRoutes});
