#!/bin/bash

scriptdir="$(dirname "$0")"
cd $scriptdir

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cleanup () {
    docker rm api-gateway-unit-test -f
}

trap 'cleanup ; printf "${RED}Tests Failed for Unexpected Reasons${NC}\n" ; exit 1' HUP INT QUIT PIPE TERM
cd ..
docker build --tag api-gateway-unit-test -f DockerfileUnitTest .
docker run --name api-gateway-unit-test api-gateway-unit-test

TEST_EXIT_CODE=`docker wait api-gateway-unit-test`
if [[ -z ${TEST_EXIT_CODE+x} || "$TEST_EXIT_CODE" -ne 0 ]] ; then
    printf "${RED}Tests Failed${NC} - Exit Code: $TEST_EXIT_CODE\n"
else
    printf "${GREEN}Tests Passed${NC}\n"
    cleanup
fi
# Remove images with no tags.
docker rmi $(docker images | grep "^<none>" | awk '{print $3}') &> /dev/null
docker volume rm $(docker volume ls -qf dangling=true) &> /dev/null
exit $TEST_EXIT_CODE

