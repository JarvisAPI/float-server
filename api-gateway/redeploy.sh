#!/bin/bash

service=

if [ "$1" == "" ] ; then
    echo "usage: ./redeploy.sh service"
    exit 0
fi

service=$1

#if [ $service == "elastic-mongo-connector" ] ; then
#    echo "redeploy elastic-search-service instead"
#    exit 0
#fi

remake () {
    docker-compose -p deploy kill $1 &> /dev/null
    docker-compose -p deploy rm -f $1 &> /dev/null
    docker-compose -p deploy build $1
    docker-compose -p deploy up -d --no-recreate $1
}

remake $service

if [ $service == "ejabberd-service" ] ; then
    echo "enabling ejabberd mod http offline module"
    sleep 20
    ../ejabberd-service/enable_mod_http_offline.sh
fi

#if [ $service == "elastic-search-service" ] ; then
#    sleep 10
#    remake "elastic-mongo-connector"
#fi


# Remove images with no tags.
docker rmi $(docker images | grep "^<none>" | awk '{print $3}') &> /dev/null
# This command might remove volumes (dangerous command)
# docker volume rm $(docker volume ls -qf dangling=true) &> /dev/null
