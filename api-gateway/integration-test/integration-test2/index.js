"use strict";

const request = require("supertest");
const should = require("should");
const isPortReachable = require("is-port-reachable");
const fs = require("fs");

describe("api-gateway", function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // Need a long timeout for exponential backoff to work.
  const globalThis = this;
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          if (reachable) {
            globalThis.timeout(20000);
          }
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(7, () => isPortReachable(port, {host}));
    };
    return tryConnect(url);
  });

  // This access_token should be obtained during the first test
  // in order to be used for testing in later tests.
  let authkey = "Authorization";
  let authval = null;
  it("setup tests with user, shop and product", (done) => {
    let user = {
      username: "testuser",
      password: "testPassword"
    };
    let shop = {
      shop_name: "testshop",
      shop_id: "testshop-id",
      shop_description: "testshop-description",
      shop_icon_type: 1,
      shop_icon_latitude: 0,
      shop_icon_longitude: 0
    };
    let product = {
      product_name: "testproduct",
      product_id: "testproduct-id",
      product_description: "testproduct-description",
      product_price: 10
    };
    let addProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/gateway/shop/${shop.shop_id}/product`)
        .set(authkey, authval)
        .send({product})
        .expect(200, done);
    };
    let makeShop = (err, res) => {
      if (err) {
        throw err;
      }
      let access_token = res.body.access_token;
      authval = "Bearer " + access_token;      
      api.post("/gateway/shop")
        .set(authkey, authval)
        .send({shop})
        .expect(200, addProduct);
    };
    let getToken = (err, res) => {
      if (err) {
        throw err;
      }
      let basicAuthValue = Buffer.from(`${user.username}:${user.password}`).toString("base64");
      api.post("/gateway/auth/token")
        .set("Authorization", "Basic " + basicAuthValue)
        .expect(200, makeShop);
    };
    api.post("/gateway/user")
      .send({user})
      .expect(200, getToken);
  });

  it("add main image and sub image to product checking that images are only added if authorized", (done) => {
    // Assuming shop and product are made in previous tests.
    let shop_id = "testshop-id";
    let product_id = "testproduct-id";
    let addSubImageAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/gateway/shop/${shop_id}/product/${product_id}/image`)
        .set(authkey, authval)
        .field("name", "test-sub-image")
        .attach("image", "test-image.jpg")
        .expect(200, done);
    };    
    let addSubImageNotAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/gateway/shop/${shop_id}/product/${product_id}/image`)
        .field("name", "test-sub-image")
        .attach("image", "test-image.jpg")
        .expect(401, addSubImageAuthorized);
    };
    let addMainImageAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
        .set(authkey, authval)
        .field("name", "test-main-image")
        .attach("image", "test-image.jpg")
        .expect(200, addSubImageNotAuthorized);
    };
    api.post(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
      .field("name", "test-main-image")
      .attach("image", "test-image.jpg")
      .expect(401, addMainImageAuthorized);
  });

  it("modify shop product returns 200 success if authorized otherwise 401", (done) => {
    // Assuming shop and product are made in previous tests.
    let shop_id = "testshop-id";
    let product_id = "testproduct-id";
    let product_edit = {
      product_name: "testproduct-edit"
    };
    let modifyShopAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      api.put(`/gateway/shop/${shop_id}/product/${product_id}`)
        .set(authkey, authval)
        .send({product_edit})
        .expect(200, done);
    };
    api.put(`/gateway/shop/${shop_id}/product/${product_id}`)
      .send(product_edit)
      .expect(401, modifyShopAuthorized);    
  });

  it("swap product main image with sub image returns 200 success if authorized otherwise 401", (done) => {
    // Assuming shop and product are made in previous tests.
    let shop_id = "testshop-id";
    let product_id = "testproduct-id";
    let main_image = null;
    let sub_image = null;
    let swapMainAndSubImage = (err, res) => {
      if (err) {
        throw err;
      }
      api.put(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
        .set(authkey, authval)
        .send({old_main_image_file: main_image, new_main_image_file: sub_image})
        .expect(200, done);
    };
    let swapMainAndSubImageNotAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      main_image = product.product_main_image_file;
      sub_image = product.product_image_files[0];
      api.put(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
        .send({old_main_image_file: main_image, new_main_image_file: sub_image})
        .expect(401, swapMainAndSubImage);      
    };
    api.get(`/gateway/shop/${shop_id}/product/${product_id}`)
      .expect(200, swapMainAndSubImageNotAuthorized);
  });

  it("delete product main image returns 200 success if authorized otherwise 401", (done) => {
    // Assuming shop and product are made in previous tests.
    let shop_id = "testshop-id";
    let product_id = "testproduct-id";
    let deleteMainImageAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
        .set(authkey, authval)
        .expect(200, done);
    };
    api.delete(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
      .expect(401, deleteMainImageAuthorized);
  });

  it("delete product sub image return 200 success if authorized otherwise 401", (done) => {
    // Assuming shop and product are made in previous tests.
    let shop_id = "testshop-id";
    let product_id = "testproduct-id";
    let image_files = null;
    let deleteSubImages = (err, res) => {
      if (err) {
        throw err;
      }
      api.put(`/gateway/shop/${shop_id}/product/${product_id}/images`)
        .set(authkey, authval)
        .send({image_files})
        .expect(200, done);
    };
    let deleteSubImagesNotAuthorized = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      image_files = product.product_image_files;
      api.put(`/gateway/shop/${shop_id}/product/${product_id}/images`)
        .send({image_files})
        .expect(401, deleteSubImages);
    };
    api.get(`/gateway/shop/${shop_id}/product/${product_id}`)
      .expect(200, deleteSubImagesNotAuthorized);
  });

  it("delete shop return 200 success and then try to get back shop products and shop info should fail", (done) => {
    let shop_id = "testshop-id";
    api.delete(`/gateway/shop/${shop_id}`).set(authkey, authval).expect(200)
      .then(() => api.get(`/gateway/shop/${shop_id}`).expect(404))
      .then(() => api.get(`/gateway/shop/${shop_id}/product/0/10`).expect(404))
      .then(() => done())
      .catch(done);
  });

  /**
   * The shop that is used previous is deleted at this point.
   * 
   */
});
