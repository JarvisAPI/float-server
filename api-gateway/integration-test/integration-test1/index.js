'use strict';

const request = require('supertest');
const should = require('should');
const isPortReachable = require('is-port-reachable');
const fs = require('fs');

describe('api-gateway', function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // Need a long timeout for exponential backoff to work.
  const globalThis = this;
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          if (reachable) {
            globalThis.timeout(20000);
          }
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(7, () => isPortReachable(port, {host}));
    };
    return tryConnect(url);
  });

  // This access_token should be obtained during the first test
  // in order to be used for testing in later tests.
  let access_token = null;

  it('adding a well formed user then getting user token', (done) => {
    let user = {
      username: 'testuser',
      password: 'testPassword',
      name:'test'
    };
    let verifyToken = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property('access_token');
      res.body.should.have.property('refresh_token');
      access_token = res.body.access_token;
      done();
    };
    let getToken = (err, res) => {
      if (err) {
        throw err;
      }
      let basicAuthValue = Buffer.from(`${user.username}:${user.password}`).toString('base64');
      api.post('/gateway/auth/token')
        .set('Authorization', 'Basic ' + basicAuthValue)
        .expect(200, verifyToken);
    };
    api.post('/gateway/user')
      .send({user})
      .expect(200, getToken);
  });

  it('can get map icons', (done) => {
    api.get('/gateway/map/icons/0/0')
      .expect(200, (err, res) => {
        if (err) {
          throw err;
        }
        res.body.should.have.property('icons');
        done();
      });
  });

  it('can create a shop and then get it back', (done) => {
    let shop_name = 'test';
    let shop_id = 'test-shop-uuid0';
    let shopBody = {
      shop_name,
      shop_description: 'testing shop',
      shop_id,
      shop_icon_type: 0,
      shop_icon_latitude: 0,
      shop_icon_longitude: 0
    };

    let verifyShop = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.shop.shop_name.should.equal(shop_name);
      done();
    };

    let getShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/shop/${shop_id}`)
        .expect(200, verifyShop);
    };

    api.post('/gateway/shop')
      .set('Authorization', 'Bearer ' + access_token)
      .send({shop: shopBody})
      .expect(200, getShop);
  });

  it('create shop, then verify that shop chat user is made', (done) => {
    let shop = {
      shop_name: 'test-chat-shop',
      shop_description: 'test-chat-shop chat user',
      shop_id: 'test-chat-shop',
      shop_icon_type: 0,
      shop_icon_latitude: 1.7,
      shop_icon_longitude: 7.1
    };
    let verifyShopChatUserMade = (err , res) => {
      if (err) {
        throw err;
      }
      if (res.body.chat_token) {
	done();
      } else {
	done(new Error('Chat token is empty!'));
      }
    };
    let getShopChatToken = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/chat/token?shop_id=${shop.shop_id}`)
        .set('Authorization', 'Bearer ' + access_token)
        .expect(200, verifyShopChatUserMade);
    };
    api.post('/gateway/shop')
      .set('Authorization', 'Bearer ' + access_token)
      .send({shop})
      .expect(200, getShopChatToken);
  });  

  it('create a shop with incomplete info should fail and shop should not exist', (done) => {
    let shop_id = 'test-bad-shop-uuid0';
    let shopBody = {
      shop_name: 'bad-shop-req',
      shop_description: 'bad-shop-desc',
      shop_id
    };
    // No latitude, longitude or icon_type supplied so shop cannot be placed onto map.
    let getShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/shop/${shop_id}`)
        .expect(404, done);
    };
    api.post('/gateway/shop')
      .set('Authorization', 'Bearer ' + access_token)
      .send({shop: shopBody})
      .expect(400, getShop);
  });

  it('create shop, add product to shop and get product back', (done) => {
    let shop_id = 'product-shop';
    let shop = {
      shop_name: 'product-shop-name',
      shop_description: 'product-shop-desc',
      shop_id,
      shop_icon_type: 0,
      shop_icon_latitude: 1,
      shop_icon_longitude: 1
    };
    let product_id = 'test-product-id';
    let product = {
      product_id,
      product_name: 'test-product-name-0',
      product_price: '20.12',
      product_category: 'food',
      product_description: 'test-product-desc'
    };
    let verifyProduct = (err, res) => {
      if (err) {
        throw err;
      }
      let obtained = res.body.product;
      let expected = product;
      expected.shop_id = shop_id;
      obtained.should.containEql(expected);
      done();
    };
    let getProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/shop/${shop_id}/product/${product_id}`)
        .expect(200, verifyProduct);
    };
    let addProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/gateway/shop/${shop_id}/product`)
        .set('Authorization', 'Bearer ' + access_token)
        .send({product})
        .expect(200, getProduct);
    };
    api.post('/gateway/shop')
      .set('Authorization', 'Bearer ' + access_token)
      .send({shop})
      .expect(200, addProduct);
  });

  it('add list of products to shop then get list of products back', (done) => {
    // Assuming the following shop is created in previous tests.
    let shop_id = 'product-shop';
    let verifyProducts = (err, res) => {
      if (err) {
        throw err;
      }
      let product_list = res.body.product_list;
      product_list.length.should.equal(10);
      done();
    };
    let getProducts = (res) => {
      api.get(`/gateway/shop/${shop_id}/product/10/10`)
        .expect(200, verifyProducts);
    };
    let promises = [];
    for (let i = 0; i < 20; i++) {
      let product = {};
      product.product_id = `${i}`;
      product.product_name = 'test';
      product.product_description = 'testDesc';
      product.product_price = '10';
      promises.push(api.post(`/gateway/shop/${shop_id}/product`)
                    .set('Authorization', 'Bearer ' + access_token)
                    .send({product}));
    }
    Promise.all(promises)
      .then(getProducts)
      .catch((err) => {throw err;});
  });

  it('add main image to shop product and then get main image back', (done) => {
    // Assuming this shop and product are created through previous tests.
    let shop_id = 'product-shop';
    let product_id = '0';
    let verifyImage = (err, res) => {
      if (err) {
        throw err;
      }
      fs.readFile('test-image.jpg', (err, data) => {
        if (err) {
          throw err;
        }
        should(Buffer.compare(data, res.body)).equal(0);
        done();
      });
    };
    let getImage = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      let main_image_file = product.product_main_image_file;
      main_image_file.should.be.ok();
      api.get(`/gateway/image/${main_image_file}`)
        .expect(200, verifyImage);
    };
    let getImageFile = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/shop/${shop_id}/product/${product_id}`)
        .expect(200, getImage);
    };
    api.post(`/gateway/shop/${shop_id}/product/${product_id}/main-image`)
      .set('Authorization', 'Bearer ' + access_token)
      .field('name', 'test-image0')
      .attach('image', 'test-image.jpg')
      .expect(200, getImageFile);
  });

  it('add profile and background pictures and get user profile', (done) => {
    // Assuming this shop and product are created through previous tests.
    let profileURL;
    let backgroundURL;

    let getProfile = (err, res) => {
      if (err) {
        throw err;
      }
      backgroundURL = res.body.image_url;
      api.get(`/gateway/user/testuser/profile`).expect(200,  function(err , res){
        if (err) {
          throw err;
        }
        var backgroundImages = res.body.background_images;
        var profileImages = res.body.profile_images;
        console.log(backgroundImages);
        console.log(profileImages);
        if(backgroundImages[backgroundImages.length -1].url === backgroundURL
           && profileImages[profileImages.length -1].url === profileURL){
          done();
        }
        else{
          done(new Error("something went wrong"));
        }
      });
    };

    let addBackgroundImage = (err, res) => {
      if (err) {
        throw err;
      }
      profileURL = res.body.image_url;
      api.post(`/gateway/user/profile/background`)
        .set('Authorization', 'Bearer ' + access_token)
        .field('name', 'test-image0')
        .attach('image', 'test-image.jpg')
        .expect(200, getProfile);
    };

    api.post(`/gateway/user/profile/profile_image`)
      .set('Authorization', 'Bearer ' + access_token)
      .field('name', 'test-image0')
      .attach('image', 'test-image.jpg')
      .expect(200, addBackgroundImage);
  });

  it('send image to a chat user', (done) => {
    // Assuming this shop and product are created through previous tests.
    let imageURL;
    let getPrivateImage = (err, res) => {
      if (err) {
        throw err;
      }
      imageURL = res.body.image_url;
      api.get(`/gateway/image/`+imageURL)
      .set('Authorization', 'Bearer ' + access_token)
      .expect(200,  function(err , res){
        if (err) {
          throw err;
        }
        fs.readFile('test-image.jpg', (err, data) => {
          if (err) {
            throw err;
          }
          should(Buffer.compare(data, res.body)).equal(0);
          done();
        });
      });
    };
    api.post(`/gateway/chat/user/dialog/jcho123/image`)
      .set('Authorization', 'Bearer ' + access_token)
      .attach('image', 'test-image.jpg')
      .expect(200, getPrivateImage);
  });

  it('should not be able to read image send via messaging without token', (done) => {
    // Assuming this shop and product are created through previous tests.
    let imageURL;
    let getPrivateImage = (err, res) => {
      if (err) {
        throw err;
      }
      imageURL = res.body.image_url;
      api.get(`/gateway/image/`+imageURL)
        .expect(404, done);
    };
    api.post(`/gateway/chat/user/dialog/jcho123/image`)
      .set('Authorization', 'Bearer ' + access_token)
      .attach('image', 'test-image.jpg')
      .expect(200, getPrivateImage);
  });

  it('add extra image to product, get that image file location and then get image back', (done) => {
    // Assuming this shop and product are created through previous tests.
    let shop_id = 'product-shop';
    let product_id = '1';
    const verifyImage = (err, res) => {
      if (err) {
        throw err;
      }
      fs.readFile('test-image.jpg', (err, data) => {
        if (err) {
          throw err;
        }
        should(Buffer.compare(data, res.body)).equal(0);
        done();
      });
    };
    const checkImageFiles = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      let image_files = product.product_image_files;
      image_files.should.be.ok();
      image_files.length.should.equal(1);
      api.get(`/gateway/image/${image_files[0]}`)
        .expect(200, verifyImage);
    };
    const getImageFiles = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/shop/${shop_id}/product/${product_id}`)
        .expect(200, checkImageFiles);
    };
    api.post(`/gateway/shop/${shop_id}/product/${product_id}/image`)
      .set('Authorization', 'Bearer ' + access_token)
      .field('name', 'test-image0')
      .attach('image', 'test-image.jpg')
      .expect(200, getImageFiles);
  });

  it('add shop then get it back based on its map location', (done) => {
    // Assuming newly created shop is isolated so assertion will work properly.
    let shop_id = 'map-shop';
    let lat = 7, lng = 7;
    let shop = {
      shop_name: 'map-shop-name',
      shop_description: 'map-shop-desc',
      shop_id,
      shop_icon_type: 0,
      shop_icon_latitude: lat,
      shop_icon_longitude: lng
    };
    let verifyShop = (err, res) => {
      if (err) {
        throw err;
      }
      let icons = res.body.icons;
      icons[0].icon_id.should.equal(shop_id);
      done();
    };
    let getShopFromMap = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/gateway/map/icons/${lat}/${lng}`)
        .expect(200, verifyShop);
    };
    api.post('/gateway/shop')
      .set('Authorization', 'Bearer ' + access_token)
      .send({shop})
      .expect(200, getShopFromMap);
  });

  it('get shops from user', (done) => {
    // Assuming user was created in previous tests.
    let username = 'testuser';
    let verifyShops = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property('shops');
      res.body.shops[0].should.have.property('shop_icon_latitude');
      res.body.shops[0].should.have.property('shop_icon_longitude');
      res.body.shops[0].should.have.property('shop_icon_type');
      res.body.shops[0].should.have.property('shop_id');
      res.body.shops[0].should.have.property('shop_name');
      done();
    };
    api.get(`/gateway/user/${username}/shops`)
      .expect(200, verifyShops);
  });

  it('get chat token from user', (done) => {
    // Assuming access_token was acquired.
    let verifyBody = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      res.body.should.have.property('chat_token');
      done();
    };

    api.get('/gateway/chat/token')
      .set('Authorization', 'Bearer ' + access_token)
      .expect(200, verifyBody);
  });

  it('set a dialog to 0 barepeer:james1 should be set to 0', (done) => {
    let checkIfSuccess = (err,res) => {
      if(err){
	       throw err;
      }
      api.get('/gateway/chat/user/dialog')
         .set('Authorization', 'Bearer ' + access_token)
         .expect(200, function(err,res) {
           if(err){
             done(Error("something went wrong while retriving dialog list"));
           }
      	   let result = res.body.dialog_list;
           console.log(result);
           let count = 0;
           for(let i = 0 ; i < result.length; i ++ ){
	           if(result[i].unread_count === 0 && result[i].bare_peer === "james1"){
               count ++;
             }
           }
           count.should.equals(1);
           done();
      });
    };
    api.put('/gateway/chat/user/dialog/james1/0')
       .set('Authorization', 'Bearer ' + access_token)
       .expect(200, checkIfSuccess);
  });

  it('set notification token', (done) => {
    api.post('/gateway/chat/user/notificationToken/12345')
       .set('Authorization', 'Bearer ' + access_token)
       .expect(200, done);
  });

  it('obtain 200 for getting shop icons within bounds', (done) => {
    api.get('/gateway/map/icons/0/0/0/10/0/10')
      .expect(200, done);
  });

  it('obtain 200 for product search', (done) => {
    let product_name = 'product';
    let verifyBody = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property('product_list');
      done();
    };
    api.get(`/gateway/product/search?query=${product_name}&location=[0,0]`)
      .expect(200, verifyBody);
  });

  it('obtain 200 for modifying shop', (done) => {
    // Assuming this shop is created through previous tests.
    let shop_id = 'product-shop';
    let shop_edit = {shop_description: 'test'};
    api.put(`/gateway/shop/${shop_id}`)
      .set('Authorization', 'Bearer ' + access_token)
      .send({shop_edit})
      .expect(200, done);
  });

  it('obtain 401 for modifying shop without permission', (done) => {
    // Assuming this shop is created through previous tests.
    let shop_id = 'product-shop';
    api.put(`/gateway/shop/${shop_id}`)
      .send({})
      .expect(401, done);
  });

  it('get profile pictures of chat users', (done) => {
    let checkResult = (err, res) =>{
      console.log(res.body)
      res.body.should.have.property('user');
      res.body.should.have.property('shop');
      res.body["user"].should.have.property("testuser");
      res.body["shop"].should.have.property("product-shop");
      done()
    }
    let users = JSON.stringify(['product-shop','testuser']);
    api.get(`/gateway/chat/${users}/profile`)
       .expect(200,checkResult)
  })
});
