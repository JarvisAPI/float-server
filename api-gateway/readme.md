### API Gateway


## Distributed Saga
The API gateway currently uses the saga pattern to maintain data consistency across services.

The distributed saga is implemented by the SagaStateMachine class in src/saga/state_machine.js.

 The **event schema** that is stored in the database during the transaction process:
```json
{
	"_id": "Unique id identifying the event",
	"forward_states": "Ordered list of states that is required to reach completion",
	"compensating_states": "Ordered list of compensating states that matches the indices of the forward states",
	"is_compensating": "true/false to indicate whether or not we are doing compensating actions",
	"current_state": "The current state of the event"
}
```

The **state schema**:
```json
{
	"name": "The name of the state for debugging",
	"data": "The data used to execute the state action",
	"function_name": "The name of the function used to execute the state action"
}
```

**Note**: If any of the actions fail then the failing action itself should not need to be compensated.

Simple saga state diagram example (consisting of 3 actions till completion):
- Forward Actions: [#1, #2, #3]
- Compensating Actions: [<-1, <-2]

![alt text](./images/saga-state-diagram-example.png "state-machine")
