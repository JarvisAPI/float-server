#!/bin/bash

scriptdir="$(dirname "$0")"
cd $scriptdir

docker-compose -p ci kill
docker-compose -p ci rm -f
