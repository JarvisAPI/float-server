"use strict";

const request = require("supertest");
const should = require("should");
const isPortReachable = require("is-port-reachable");
const fs = require("fs");

describe("shop-service", function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  const image_server_host = process.env.IMAGE_SERVER_HOST;
  const image_server_port = process.env.IMAGE_SERVER_PORT;
  const image_url = `http://${image_server_host}:${image_server_port}`;
  const image_api = request(image_url);

  // Need a long timeout for exponential backoff to work.
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(7, () => isPortReachable(port, {host: host}));
    };
    return tryConnect(url);
  });  
  
  it("returns a 200 for creating a shop, and requesting for the shop created", (done) => {
    let stubShop = {};
    stubShop["shop_id"] = "68f4da8f-9a5d-46c6-a335-97f75b49603d";
    stubShop["shop_name"] = "dummyShop";
    stubShop["shop_description"] = "dummyShopDesc";
    stubShop["latitude"] = 0;
    stubShop["longitude"] = 0;

    let verifyShop = (err, res) => {
      if (err) {
        throw err;
      }
      let shop = res.body.shop;
      shop.shop_name.should.equal("dummyShop");
      done();
    };
    
    let requestShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.get("/shop/68f4da8f-9a5d-46c6-a335-97f75b49603d")
	.expect(200, verifyShop);
    };
    
    api.post("/shop")
      .send({shop: stubShop})
      .expect(200, requestShop);

  });

  it("can create a shop, add a product to it and get the product that was added in that shop", (done) => {
    let shop_id = "4b73b4f3-872e-49c0-8f3e-aa4e798520c5";
    let product_id = "59fee0e7-1b97-4049-9f39-02157861bb27";
    let shop = {};
    shop.shop_id = shop_id;
    shop.shop_name = "testShop";
    shop.shop_description = "testShopDesc";
    shop.latitude = 0;
    shop.longitude = 0;
    
    let product = {};
    let checkProduct = (err, res) => {
      if (err) {
        throw err;
      }
      let obtained = res.body.product;
      let expected = product;
      expected.shop_id = shop_id;
      obtained.should.containEql(expected);
      done();
    };
    
    let getProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
	.expect(200, checkProduct);
    };
    
    let addProduct = (err, res) => {
      if (err) {
        throw err;
      }
      product.product_id = product_id;
      product.product_name = "testProduct";
      product.product_category = "food";
      product.product_description = "testProductDesc";
      product.product_price = "10.99";
      api.post("/shop/4b73b4f3-872e-49c0-8f3e-aa4e798520c5/product")
	.send({product})
	.expect(200, getProduct);
    };

    api.post("/shop")
      .send({shop})
      .expect(200, addProduct);
  });

  it("add a shop then delete it", (done) => {
    let shop_id = "test-shop-uuid0";
    const checkShopGone = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}`)
        .expect(404, done);
    };
    const deleteShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete(`/shop/${shop_id}`)
        .expect(200, checkShopGone);
    };
    let testShop = {
      shop_id,
      shop_name: "testShop2",
      shop_description: "testDesc2",
      latitude: 1,
      longitude: 1
    };
    api.post("/shop")
      .send({shop: testShop})
      .expect(200, deleteShop);
  });

  it("add list of products to shop then get the list of products back", (done) => {
    let shop_id = "product_shop";
    const verifyProducts = (err, res) => {
      if (err) {
        throw err;
      }
      let product_list = res.body.product_list;
      product_list.length.should.equal(10);
      done();
    };
    const getProducts = (res) => {
      api.get(`/shop/${shop_id}/product/0/10`)
        .expect(200, verifyProducts);
    };
    const addProducts = (err, res) => {
      if (err) {
        throw err;
      }
      let promises = [];
      for (let i = 0; i < 20; i++) {
        let product = {};
        product.product_id = `${i}`;
        product.product_name = "test";
        product.product_description = "testDesc";
        product.product_price = "10";
        promises.push(api.post(`/shop/${shop_id}/product`)
                      .send({product}));
      }
      Promise.all(promises)
        .then(getProducts)
        .catch((err) => {throw err;});
    };
    let testShop = {
      shop_id,
      shop_name: "test_product_shop",
      shop_description: "test_product_shop_desc",
      latitude: 2,
      longitude: 2
    };
    api.post("/shop")
      .send({shop: testShop})
      .expect(200, addProducts);
  });

  it("add main image to product, get main image file location and then get image back", (done) => {
    // Assuming this shop and product is created through previous tests.
    let shop_id = "product_shop";
    let product_id = "0";
    const verifyImage = (err, res) => {
      if (err) {
        throw err;
      }
      fs.readFile("test-image.jpg", (err, data) => {
        if (err) {
          throw err;
        }
        should(Buffer.compare(data, res.body)).equal(0);
        done();
      });
    };
    const getImage = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      let main_image_file = product.product_main_image_file;
      main_image_file.should.be.ok();
      image_api.get(`/image/${main_image_file}`)
        .expect(200, verifyImage);
    };
    const getImageFile = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, getImage);
    };
    api.post(`/shop/${shop_id}/product/${product_id}/main-image`)
      .field("name", "test-image")
      .attach("image", "test-image.jpg")
      .expect(200, getImageFile);
  });

  it("add extra image to product, get that image file location and then get image back", (done) => {
    // Assuming this shop and product are created through previous tests.
    let shop_id = "product_shop";
    let product_id = "1";
    const verifyImage = (err, res) => {
      if (err) {
        throw err;
      }
      fs.readFile("test-image.jpg", (err, data) => {
        if (err) {
          throw err;
        }
        should(Buffer.compare(data, res.body)).equal(0);
        done();
      });
    };
    const checkImageFiles = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      let image_files = product.product_image_files;
      image_files.should.be.ok();
      image_files.length.should.equal(1);
      image_api.get(`/image/${image_files[0]}`)
        .expect(200, verifyImage);
    };
    const getImageFiles = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, checkImageFiles);
    };
    api.post(`/shop/${shop_id}/product/${product_id}/image`)
      .field("name", "test-image0")
      .attach("image", "test-image.jpg")
      .expect(200, getImageFiles);
  });

  it("creating shop with bad request", (done) => {
    api.post("/shop")
      .send({})
      .expect(400, done);
  });

  it("creating shop product with bad request", (done) => {
    // Assuming this shop is created in previous tests.
    let shop_id = "product_shop";
    api.post(`/shop/${shop_id}/product`)
      .send({})
      .expect(400, done);
  });

  it("adding product to not created shop should return error", (done) => {
    let shop_id = "random-not-created-shop";
    let product_id = `random-product-id`;
    let product = {};
    product.product_id = product_id;
    product.product_name = "random-product";
    product.product_description = "testDesc";
    product.product_price = "10";
    let verifyProductNotExist = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(404, done);
    };
    api.post(`/shop/${shop_id}/product`)
      .send({product})
      .expect(404, verifyProductNotExist);
  });

  it("add a product to a shop, delete that product then try to get product back", (done) => {
    // Assuming this shop was created in previous tests.
    let shop_id = "product_shop";
    let product = {
      product_id: "delete-product-test-id",
      product_name: "test",
      product_description: "test",
      product_price: "10"
    };
    let verifyProductGone = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product.product_id}`)
        .expect(404, done);
    };
    let deleteProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete(`/shop/${shop_id}/product/${product.product_id}`)
        .expect(200, verifyProductGone);
    };
    api.post(`/shop/${shop_id}/product`)
      .send({product})
      .expect(200, deleteProduct);
  });
});
