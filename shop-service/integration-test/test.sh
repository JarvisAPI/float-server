#!/bin/bash

prefix="integration-test"
dirs=("${prefix}1" "${prefix}2")

for dir in ${dirs[@]}
do
    cd $dir
    ./test.sh
    exitCode=$?
    if [ $exitCode -ne 0 ] ; then
        exit 1;
    fi
    cd ..
done
