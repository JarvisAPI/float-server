"use strict";

const request = require("supertest");
const should = require("should");
const isPortReachable = require("is-port-reachable");
const fs = require("fs");

describe("shop-service", function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  const image_server_host = process.env.IMAGE_SERVER_HOST;
  const image_server_port = process.env.IMAGE_SERVER_PORT;
  const image_url = `http://${image_server_host}:${image_server_port}`;
  const image_api = request(image_url);

  // Need a long timeout for exponential backoff to work.
  this.timeout(200000);
  const globalThis = this;

  let wait = 20000; // Time time to wait for endpoints that require elastic search to update.
  
  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          if (reachable) {
            globalThis.timeout(10000);
            return Promise.resolve(reachable);
          }
          if (retries > 1) {
            return pause(delay).then(() => backoff(retries - 1, fn, delay * 2));
          }
          return Promise.reject();
        });
      };
      return backoff(7, () => isPortReachable(port, {host: host}));
    };
    return tryConnect(url);
  });  
  
  it("create a shop add one product and get product back as suggestion when searching for shop proucts", (done) => {
    let shop = {
      shop_id: "test-shop-id",
      shop_name: "test-shop",
      shop_description: "test-shop-desc",
      latitude: 0,
      longitude: 0
    };
    let product = {
      product_id: "test-product-id",
      product_name: "test-product-name",
      product_description: "test-product-desc",
      product_category: "food",
      product_price: "10"
    };
    let expectedProduct = Object.assign({
      shop_id: shop.shop_id
    }, product);
    let expected = [expectedProduct];
    let verifySuggestions = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property("product_list").containDeep(expected);
      done();
    };

    let getSuggestions = (err, res) => {
      if (err) {
        throw err;
      }
      setTimeout(() => {
        api.get(`/product/search?query=${product.product_name}&shop_id=${shop.shop_id}`)
          .expect(200, verifySuggestions);
      }, wait);
    };

    let addProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/shop/${shop.shop_id}/product`)
        .send({product})
	.expect(200, getSuggestions);
    };
    
    api.post("/shop")
      .send({shop})
      .expect(200, addProduct);
  });

  it("add multiple products to a shop and get back products as suggestion by given partial search query when searching for local products", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";
    let prefix = "my-product-";
    let maxLimit = 5;
    let expected = [];
    let promises = [];
    for (let i = 0; i < maxLimit; i++) {
      let product = {
        product_description: `desc ${i}`,
        product_price: "10",
        product_category: "food"
      };
      product.product_id = `${i}`;
      product.product_name = `${prefix}${i}`;
      let expectedProduct = Object.assign({shop_id}, product);
      expected.push(expectedProduct);
      product.latitude = 0;
      product.longitude = 0;
      promises.push(api.post(`/shop/${shop_id}/product`)
                    .send({product}));
    }
    let verifySuggestions = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property("product_list").containDeep(expected);
      done();
    };
    let getSuggestions = () => {
      setTimeout(() => {
        api.get(`/product/search?query=${prefix}&location=[0,0]`)
          .expect(200, verifySuggestions);
      }, wait);
    };
    Promise.all(promises)
      .then(getSuggestions)
      .catch((err) => {
        done(err);
      });
  });

  it("add multiple product with the same name", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";
    let promises = [];
    for (let i = 0; i < 2; i++) {
      let product = {
        product_name: "name",
        product_description: "desc",
        product_price: "10"
      };      
      product.product_id = `product_id-${i}`;
      promises.push(api.post(`/shop/${shop_id}/product`)
                    .send({product})
                    .expect(200));
    }
    Promise.all(promises)
      .then((arr) => {
        done();
      })
      .catch(done);
  });

  it("add products with similar name and get them back by their name by searching for shop products", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";
    let name = "unqiue";
    let limit = 10;
    let promises = [];
    let expected = [];
    for (let i = 0; i < limit; i++) {
      let product = {
        product_name: `${name} ${i}`,
        product_description: `desc ${i}`,
        product_price: "1",
        product_category: "food"
      };
      product.product_id = `unique-product_id-${i}`;
      promises.push(api.post(`/shop/${shop_id}/product`)
                    .send({product})
                    .expect(200));
      let expectedProduct = Object.assign({shop_id}, product);
      expected.push(expectedProduct);
    }
    let verifyProducts = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property("product_list").containDeep(expected);
      done();
    };
    let getBackProductsByName = () => {
      setTimeout(()=> {
        api.get(`/product/search?query=${name}&shop_id=${shop_id}&limit=${limit}`)
          .expect(200, verifyProducts);
      }, wait);
    };
    Promise.all(promises)
      .then((arr) => {
        getBackProductsByName();
      })
      .catch(done);
  });

  it("modify shop info, should get back modified shop", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";
    let new_shop_desc = "new-shop-desc";

    let verifyShopChanged = (err, res) => {
      if (err) {
        throw err;
      }
      let shop = res.body.shop;
      shop.shop_description.should.equal(new_shop_desc);
      done();
    };
    let getShopBack = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}`)
        .expect(200, verifyShopChanged);
    };
    let checkDescDifferent = (err, res) => {
      if (err) {
        throw err;
      }
      let shop = res.body.shop;
      shop.shop_description.should.not.equal(new_shop_desc);
      api.put(`/shop/${shop_id}`)
        .send({shop_edit: {shop_description: new_shop_desc}})
        .expect(200, getShopBack);
    };
    api.get(`/shop/${shop_id}`)
      .expect(200, checkDescDifferent);
  });

  it("modify shop info with empty body should return 400", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";
    api.put(`/shop/${shop_id}`)
      .send({})
      .expect(400, done);
  });

  it("add product to shop, modify the product then get it back", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";
    let product_id = "test-modify";
    let product = {
      product_name: "name",
      product_description: "desc",
      product_price: "10",
      product_id
    };
    let product_edit = {
      product_name: "test",
      product_description: "description",
      product_price: "12",
      product_category: "food"
    };
    let verifyProduct = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      product.product_name.should.equal(product_edit.product_name);
      product.product_description.should.equal(product_edit.product_description);
      product.product_price.should.equal(product_edit.product_price);
      product.product_category.should.equal(product_edit.product_category);
      done();
    };
    let getProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, verifyProduct);
    };
    let modifyProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.put(`/shop/${shop_id}/product/${product_id}`)
        .send({product_edit})
        .expect(200, getProduct);
    };
    api.post(`/shop/${shop_id}/product`)
      .send({product})
      .expect(200, modifyProduct);
  });

  it("add main image and sub image to product then swap the two images", (done) => {
    // Assuming shop and product were created in previous tests.
    let shop_id = "test-shop-id";
    let product_id = "test-modify";
    let main_image_file;
    let sub_image_file;
    let verifyImageFilesChanged = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      product.product_main_image_file.should.equal(sub_image_file);
      product.product_image_files.length.should.equal(1);
      product.product_image_files[0].should.equal(main_image_file);
      done();
    };
    let getProductAgain = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, verifyImageFilesChanged);      
    };
    let verifyImageFilesDifferent = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      main_image_file = product.product_main_image_file;
      sub_image_file = product.product_image_files[0];
      main_image_file.should.not.equal(sub_image_file);
      // Swap images.
      api.put(`/shop/${shop_id}/product/${product_id}/main-image`)
        .send({old_main_image_file: main_image_file,
               new_main_image_file: sub_image_file})
        .expect(200, getProductAgain);
    };
    let getMainAndSubImageFile = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, verifyImageFilesDifferent);
    };
    let uploadSubImage = (err, res) => {
      if (err) {
        throw err;
      }
      api.post(`/shop/${shop_id}/product/${product_id}/image`)
        .field("name", "test-image")
        .attach("image", "test-image.jpg")
        .expect(200, getMainAndSubImageFile);
    };
    api.post(`/shop/${shop_id}/product/${product_id}/main-image`)
      .field("name", "test-main-image")
      .attach("image", "test-image.jpg")
      .expect(200, uploadSubImage);    
  });

  it("delete product main image then get product main image back should be null", (done) => {
    // Assuming shop and product were created in previous tests.
    // Assuming product has main image added in previous test.
    let shop_id = "test-shop-id";
    let product_id = "test-modify";
    let verifyProductMainImageGone = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      should(product.product_main_image_file === null).be.true();
      done();
    };
    let getProduct = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, verifyProductMainImageGone);
    };
    api.delete(`/shop/${shop_id}/product/${product_id}/main-image`)
      .expect(200, getProduct);
  });

  it("delete product images then get product images back should be empty", (done) => {
    // Assuming shop and product were created in previous tests.
    // Assuming product has sub images added in previous test.
    let shop_id = "test-shop-id";
    let product_id = "test-modify";
    let verifyProductImageFilesGone = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      product.product_image_files.length.should.equal(0);
      done();
    };
    let getProductAgain = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}/product/${product_id}`)
        .expect(200, verifyProductImageFilesGone);
    };
    let deleteProductImages = (err, res) => {
      if (err) {
        throw err;
      }
      let product = res.body.product;
      let image_files = product.product_image_files;
      image_files.length.should.not.equal(0);
      api.put(`/shop/${shop_id}/product/${product_id}/images`)
        .send({image_files})
        .expect(200, getProductAgain);
    };
    api.get(`/shop/${shop_id}/product/${product_id}`)
      .expect(200, deleteProductImages);
  });

  it("add image to shop", (done) => {
    // Assuming shop were created in previous tests.
    let shop_id = "test-shop-id";

    const checkShopImage = (err, res) => {
      if (err) {
        throw err;
      }
      let image_files = res.body.shop.images;
      image_files.length.should.be.equal(1);
      done();
    };

    const getShopImages = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}`)
        .expect(200, checkShopImage);
    };
    
    api.post(`/shop/${shop_id}/image`)
      .field("name", "shop-image")
      .attach("image", "test-image.jpg")
      .expect(200, getShopImages);
  });

  it("set shop main image", (done) => {
    // Assuming shop were created in previous tests.
    let shop_id = "test-shop-id";

    const checkShopImages = (err, res) => {
      if (err) {
        throw err;
      }
      let image_files = res.body.shop.images;
      let statement = typeof image_files !== "undefined";
      statement.should.be.true();
      done();
    };
    
    const getShopImages = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}`)
        .expect(200, checkShopImages);
    };
    
    api.post(`/shop/${shop_id}/main-image`)
      .field("name", "shop-image")
      .attach("image", "test-image.jpg")
      .expect(200, getShopImages);
  });

  it("get many shop main images", (done) => {
    let checkResult = (err, res) => {
      res.body.should.have.property('test-shop-id')
      res.body['test-shop-id'].should.have.property("image")
      res.body['test-shop-id'].should.have.property("name")
      done()
    }
    let shop_ids = JSON.stringify(["test-shop-id", "test-shop-id"]);
    api.get(`/shop/${shop_ids}/main-image`)
       .expect(200, checkResult)
  })

  it("delete shop image", (done) => {
    // Assuming shop was created in previous tests.
    let shop_id = "test-shop-id";

    const verifyImagesDeleted = (err, res) => {
      if (err) {
        throw err;
      }
      let image_files = res.body.shop.images;
      image_files.length.should.equal(0);
      done();
    };
    
    const getImages = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/shop/${shop_id}`)
        .expect(200, verifyImagesDeleted);
    };

    const deleteShopImages = (err, res) => {
      if (err) {
        throw err;
      }
      let image_files = JSON.stringify(res.body.shop.images);
      api.delete(`/shop/${shop_id}/image/${image_files}`)
        .expect(200, getImages);
    };
    
    api.get(`/shop/${shop_id}`)
      .expect(200, deleteShopImages);
  });

  it("add more than shop image limit", (done) => {
    // Assume images for shop are all deleted in previous test
    let shop_id = "test-shop-id";
    // Max shop image limit is set to 10 for testing
    let maxShopImageLimit = 10;

    let promises = [];
    for (let i=0; i<maxShopImageLimit; i++) {
      promises.push(
        api.post(`/shop/${shop_id}/image`)
          .field("name", "shop-image")
          .attach("image", "test-image.jpg")
          .expect(200)
      );
    }

    promises.reduce((promiseChain, currentTask) => {
      return promiseChain
        .then(() =>
              currentTask
              .then(() => Promise.resolve())
             );
    }, Promise.resolve())
      .then(() => 
            api.post(`/shop/${shop_id}/main-image`)
            .field("name", "shop-image")
            .attach("image", "test-image.jpg")
            .expect(400))
      .then(() =>
            api.post(`/shop/${shop_id}/image`)
            .field("name", "shop-image")
            .attach("image", "test-image.jpg")
            .expect(400))
      .then(() => done())
      .catch(done);
  });

  it("add more than product image limit", (done) => {
    // Assume images for shop are all deleted in previous test
    let shop_id = "test-shop-id";
    // Max product image limit is set to 10 for testing
    let maxProductImageLimit = 10;

    let product_id = "test-img-limit";
    let product = {
      product_name: "name",
      product_description: "desc",
      product_price: "10",
      product_id
    };
    
    api.post(`/shop/${shop_id}/product`)
      .send({product})
      .expect(200)
      .then(() => {
        return api.post(`/shop/${shop_id}/product/${product_id}/main-image`)
          .field("name", "shop-image")
          .attach("image", "test-image.jpg")
          .expect(200);
      })
      .then(() => {
        let promises = [];
        for (let i=0; i<maxProductImageLimit-1; i++) {
          promises.push(
            api.post(`/shop/${shop_id}/product/${product_id}/image`)
              .field("name", "shop-image")
              .attach("image", "test-image.jpg")
              .expect(200)            
          );
        }

        return promises.reduce((promiseChain, currentTask) => {
          return promiseChain
            .then(() =>
                  currentTask
                  .then(() => Promise.resolve())
                 );
        }, Promise.resolve());
      })
      .then(() =>
            api.post(`/shop/${shop_id}/product/${product_id}/main-image`)            
            .field("name", "shop-image")
            .attach("image", "test-image.jpg")
            .expect(400))
      .then(() =>
            api.post(`/shop/${shop_id}/product/${product_id}/image`)
            .field("name", "shop-image")
            .attach("image", "test-image.jpg")
            .expect(400))
      .then(() => done())
      .catch(done);
  });
});
