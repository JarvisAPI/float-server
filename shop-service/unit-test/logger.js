"use strict";

function log(type, msg, obj) {
  let prefix = `[${type}]: `;
  if (obj) {
    console.log(prefix + msg + obj);
  } else {
    console.log(prefix + msg);
  }
}

module.exports = {
  info: (msg, obj) => { log("info", msg, obj); },
  debug: (msg, obj) => { log("debug", msg, obj); },
  error: (msg, obj) => { log("error", msg, obj); }
};
