"use strict";

class RepoError extends Error {
  constructor(message, status) {
    super(message);
    this.name = this.constructor.name;
    this.status = status || 500;
  }
}

class ApiSchemaError extends Error {
  constructor(message, status) {
    super(message);
    this.name = this.constructor.name;
    this.status = status || 500;
  }
}

module.exports = {RepoError, ApiSchemaError};
