"use strict";

const MongoSchema = {};

MongoSchema.shop = {
  id: "shop_id",
  name: "shop_name",
  description: "shop_description",
  location: "location",
  products: "products",
  image_files: "images"
};

MongoSchema.product = {
  id: "product_id",
  name: "product_name",
  price: "product_price",
  category: "product_category",
  description: "product_description",
  location: "location",
  type: "type",
  main_image_file: "product_main_image_file",
  image_files: "product_image_files",
  shop_id: "shop_id"
};

module.exports = {MongoSchema};
