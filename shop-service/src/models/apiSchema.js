"use strict";

const {ApiSchemaError} = require("../errors.js");
const joi = require("joi");
const schemas = {};

let joi_id = joi.string().max(36, "utf8");

let joi_shop_description = joi.string().max(1000, "utf8");

schemas.shop = {
  shop_id: joi_id.required(),
  shop_name: joi.string().max(50, "utf8").required(),
  shop_description: joi_shop_description,
  latitude: joi.number().min(-90).max(90).required(),
  longitude: joi.number().min(-180).max(180).required()
};

let joi_product_name = joi.string().max(50, "utf8");
let joi_product_price = joi.number().positive().max(Number.MAX_SAFE_INTEGER);
let joi_product_category = joi.string().max(20, "utf8");
let joi_product_description = joi.string().max(500, "utf8");

schemas.product = {
  product_id: joi_id.required(),
  product_name: joi_product_name.required(),
  product_price: joi_product_price.required(),
  product_category: joi_product_category,
  product_description: joi_product_description,
  latitude: joi.number().min(-90).max(90),
  longitude: joi.number().min(-180).max(180)
};

schemas.shop_edit = {
  shop_description: joi_shop_description
};

schemas.product_edit = {
  product_name: joi_product_name,
  product_price: joi_product_price,
  product_category: joi_product_category,
  product_description: joi_product_description
};

const ApiSchema = {};

ApiSchema.validate = function (object, type) {
  if (!object) {
    return Promise.reject(new ApiSchemaError("object is null or undefined", 400));
  }
  const {error, value} = joi.validate(object, schemas[type]);
  if (error) {
    return Promise.reject(new ApiSchemaError("object schema not valid", 400));
  }
  return Promise.resolve();
};

ApiSchema.shop = {
  id: "shop_id",
  name: "shop_name",
  description: "shop_description",
  latitude: "latitude",
  longitude: "longitude"
};

ApiSchema.product = {
  id: "product_id",
  name: "product_name",
  price: "product_price",
  category: "product_category",
  description: "product_description",
  latitude: "latitude",
  longitude: "longitude"
};

ApiSchema.shop_edit = {
  description: "shop_description"
};

ApiSchema.product_edit = {
  name: "product_name",
  price: "product_price",
  category: "product_category",
  description: "product_description"
};

module.exports = {ApiSchema};
