"use strict";

const {RepoError} = require("../errors.js");
const {MongoSchema} = require("../models/mongoSchema.js");
const {SchemaAdapter} = require("./schemaAdapter.js");

const createIndexes = (db) => {
  db.createIndex("shop", {shop_id: 1}, {unique: true});
  db.createIndex("product", {product_id: 1}, {unique: true});
};

const repository = (container) => {
  const db_client = container.cradle.db_client;
  const db = db_client.db(container.cradle.dbSettings.db);
  const logger = container.cradle.logger;
  createIndexes(db);

  const shopCol = db.collection("shop");
  const prodCol = db.collection("product");

  const repo = {};
  
  repo.getShopInfo = (shop_id) => {
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    let projection = {_id: 0};
    projection[MongoSchema.shop.products] = 0;
    return shopCol.findOne(query, {projection})
      .then(result => {
	    if (result) {
	      return result;
	    }
	    throw new RepoError("shop not found!", 404);
      });
  };

  /**
   * Can be used locally by the server to verify inputs.
   * @param shop_id: id of the shop to query.
   * @param fields: array of fields to include in the result.
   * @return the selected fields from the shop.
   */
  repo.localGetShopInfo = (shop_id, fields) => {
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    let projection = {_id: 0};
    for (let field of fields) {
      projection[field] = 1;
    }
    return shopCol.findOne(query, {projection})
      .then(result => {
        if (result) {
          return result;
        }
        throw new RepoError("shop not found!", 404);
      });
  };

  repo.getShopProductInfo = (shop_id, product_id) => {
    let query = {};
    query[MongoSchema.product.id] = product_id;
    let projection = {_id: 0};
    projection[MongoSchema.product.location] = 0;
    projection[MongoSchema.product.type] = 0;
    return prodCol.findOne(query, {projection})
      .then(result => {
	    if (result) {
	      return result;
	    }
	    throw new RepoError("shop product not found!", 404);
      });
  };

  /**
   * Can be used locally by the server to verify inputs.
   * @param shop_id: id of the shop to query.
   * @param product_id: id of the product to query.
   * @param fields: array of fields to include in the result.
   * @return the selected fields from the product.
   */
  repo.localGetShopProductInfo = (shop_id, product_id, fields) => {
    let query = {};
    query[MongoSchema.product.id] = product_id;
    let projection = {_id: 0};
    for (let field of fields) {
      projection[field] = 1;
    }
    return prodCol.findOne(query, {projection})
      .then(result => {
        if (result) {
          return result;
        }
        throw new RepoError("product not found!", 404);
      });
  };

  repo.getShopProductsBasicInfo = (shop_id, limit, offset) => {
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    let projection = {};
    projection[MongoSchema.shop.products] = {$slice: [offset, limit]};
    return shopCol.findOne(query, {projection})
      .then(result => {
	    if (result) {
	      return result.products ? result.products : [];
	    }
	    throw new RepoError("Shop not found!", 404);
      })
      .then(products => {
	    let query = {};
	    query[MongoSchema.product.id] = {$in: products};
	    let projection = {_id: 0};
	    projection[MongoSchema.product.id] = 1;
	    projection[MongoSchema.product.name] = 1;
	    projection[MongoSchema.product.price] = 1;
	    projection[MongoSchema.product.main_image_file] = 1;	
	    return prodCol.find(query, {projection});
      })
      .then(cursor => cursor.toArray());
  };

  /**
   * shop: object that follows the ApiSchema.
   */
  repo.createShop = (shop) => {
    return shopCol.insertOne(SchemaAdapter.adaptShop(shop));
  };

  repo.removeShop = (shop_id) => {
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    return shopCol.deleteOne(query)
      .then(() => {
	    let query = {};
	    query[MongoSchema.shop.id] = {$eq: shop_id};
	    return prodCol.deleteMany(query);
      });
  };
  
  repo.removeShopProduct = (shop_id, product_id) => {
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    let update = {};
    update.$pull = {};
    update.$pull[MongoSchema.shop.products] = product_id;
    return shopCol.updateOne(query, update)
      .then(result => {
	    if (result && result.matchedCount) {
	      return;
	    }
	    throw new RepoError("Deleting product from non-existent shop", 404);
      })
      .then(() => {
	    let query = {};
	    query[MongoSchema.product.id] = product_id;	
	    return prodCol.deleteOne(query);
      });
  };

  const maxProductLimit = 100;
  repo.createShopProduct = (shop_id, product) => {
    product = SchemaAdapter.adaptProduct(product);
    product[MongoSchema.product.shop_id] = shop_id;
    let update = {$push: {}};
    update.$push[MongoSchema.shop.products] = product[MongoSchema.product.id];
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    query[MongoSchema.shop.products] = {$exists: true};
    query.$where = `this.${MongoSchema.shop.products}.length < ${maxProductLimit}`;
    return shopCol.findOneAndUpdate(query, update)
      .then(result => {
	    if (!result) {
	      throw new RepoError("Unable to find shop to add product in", 404);          
        }
        if (!result.lastErrorObject ||
            !result.lastErrorObject.updatedExisting) {
          let err = new Error();
          err.message = "Product limit reached";
          err.status = 400;
          err.type = "ERR_PRODUCT_LIMIT_REACHED";
          err.sendAsJson = true;
          throw err;
	    }
        return;
      })
      .then(() => prodCol.insertOne(product));
  };

  repo.addShopImageFile = (shop_id, image_file) => {
    let update = {};
    update.$push = {};
    update.$push[MongoSchema.shop.image_files] = image_file;
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    return shopCol.updateOne(query, update);
  };

  repo.setShopMainImageFile = (shop_id, image_file) => {
    let update = {};
    update.$push = {};
    update.$push[MongoSchema.shop.image_files] = {$each: [image_file], $position: 0};
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    return shopCol.updateOne(query, update);
  };
  
  repo.addProductImageFile = (product_id, image_file) => {
    let update = {};
    update.$push = {};
    update.$push[MongoSchema.product.image_files] = image_file;
    let query = {};
    query[MongoSchema.product.id] = product_id;    
    return prodCol.updateOne(query, update);
  };

  repo.setProductMainImageFile = (product_id, main_image_file, old_main_image_file) => {
    let update = {};
    update.$set = {};
    update.$set[MongoSchema.product.main_image_file] = main_image_file;
    if (old_main_image_file) {
      update.$push = {};
      update.$push[MongoSchema.product.image_files] = old_main_image_file;
    }
    let query = {};
    query[MongoSchema.product.id] = product_id;
    return prodCol.updateOne(query, update);
  };

  repo.deleteShopImages = (shop_id, image_files) => {
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    let update = {};
    update.$pull = {};
    update.$pull[MongoSchema.shop.image_files] = {$in: image_files};
    return shopCol.updateOne(query, update);
  };

  repo.swapProductMainImage = (shop_id, product_id, old_main_image_file, new_main_image_file) => {
    let update_pull_and_set = {$set: {}, $pull: {}};
    update_pull_and_set.$set[MongoSchema.product.main_image_file] = new_main_image_file;
    update_pull_and_set.$pull[MongoSchema.product.image_files] = new_main_image_file;
    let query = {};
    query[MongoSchema.product.id] = product_id;
    query[MongoSchema.product.image_files] = {$elemMatch: {$eq: new_main_image_file}};
    query[MongoSchema.product.main_image_file] = old_main_image_file;
    return prodCol.updateOne(query, update_pull_and_set)
      .then(result => {
	    if (result && result.modifiedCount > 0) {
	      return;
	    }
	    throw new RepoError("Unable to find product to modify", 404);
      })
      .then(() => {
	    let query = {};
	    query[MongoSchema.product.id] = product_id;
	    let update_push = {$push: {}};
	    update_push.$push[MongoSchema.product.image_files] = old_main_image_file;
        return prodCol.updateOne(query,update_push);
      });
  };

  repo.updateShopInfo = (shop_id, shop_edit) => {
    let update = {$set: {}};
    update.$set[MongoSchema.shop.description] = shop_edit.shop_description;
    let query = {};
    query[MongoSchema.shop.id] = shop_id;
    return shopCol.updateOne(query, update);
  };

  repo.deleteProductMainImage = (shop_id, product_id) => {
    let update = {$set: {}};
    update.$set[MongoSchema.product.main_image_file] = null;
    let query = {};
    query[MongoSchema.product.id] = product_id;
    let proj = {_id: 0};
    proj[MongoSchema.product.main_image_file] = 1;
    let main_image_file = null;
    return prodCol.findOne(query, {proj})
      .then(product => {
        if (product) {
          main_image_file = product[MongoSchema.product.main_image_file];
        }
        return;
      })
      .then(() => prodCol.updateOne(query, update))
      .then(() => main_image_file);
  };

  repo.deleteProductImages = (shop_id, product_id, image_files) => {
    let update = {$pull: {}};
    update.$pull[MongoSchema.product.image_files] = {$in: image_files};
    let query = {};
    query[MongoSchema.product.id] = product_id;
    return prodCol.updateOne(query, update);
  };

  repo.updateShopProductInfo = (shop_id, product_id, product_edit) => {
    let update = {
      $set: product_edit
    };
    let query = {};
    query[MongoSchema.product.id] = product_id;
    return prodCol.updateOne(query, update);
  };

  repo.getManyShopMainImage = (shop_ids) => {
    var shopImages = {}
    let getShopMainImage = (shop) =>{
      let retVal = {}
      if(shop[MongoSchema.shop.image_files] && shop[MongoSchema.shop.image_files].length > 0) {
        retVal.image = shop[MongoSchema.shop.image_files][0]
        retVal.name = shop[MongoSchema.shop.name]
      } else{ 
        retVal.image = ""
        retVal.name = shop[MongoSchema.shop.name]
      }
      shopImages[shop[MongoSchema.shop.id]] = retVal
    }

    let find = {
      $in:shop_ids
    }
    let query = {};
    query[MongoSchema.shop.id] = find;

    return new Promise((resolve, reject) => {
      let finish = () => resolve(shopImages);
      shopCol
        .find(query)
        .forEach(getShopMainImage, finish);
    });
  }
  
  repo.disconnect = () => {
    db_client.close();
  };

  return repo;
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.cradle.db_client) {
      reject(new Error("Connection db client not supplied!"));
      return;
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});  

