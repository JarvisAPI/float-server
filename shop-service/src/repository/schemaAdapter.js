"use strict";

const {ApiSchema} = require("../models/apiSchema.js");
const {MongoSchema} = require("../models/mongoSchema.js");

/**
 * Object that adapts the ApiSchema to the MongoSchema.
 */
const SchemaAdapter = {};
/**
 * Returns a shop object that abides by the mongo schema.
 */
SchemaAdapter.adaptShop = function (shop) {
  let payload = {};
  payload[MongoSchema.shop.id] = shop[ApiSchema.shop.id];
  payload[MongoSchema.shop.name] = shop[ApiSchema.shop.name];
  payload[MongoSchema.shop.description] = shop[ApiSchema.shop.description];
  payload[MongoSchema.shop.location] = {type: "point",
                                   coordinates: [shop[ApiSchema.shop.longitude],
                                                 shop[ApiSchema.shop.latitude]]
                                       };
  payload[MongoSchema.shop.products] = [];
  return payload;
};

/**
 * Returns a product object that abides by the mongo schema.
 */
SchemaAdapter.adaptProduct = function (product) {
  let payload = {};    
  if (!product[ApiSchema.product.category]) {
    product[ApiSchema.product.category] = "other";
  }
  if (!product[ApiSchema.product.description]) {
    product[ApiSchema.product.description] = "";
  }
  payload[MongoSchema.product.type] = "local";
  payload[MongoSchema.product.id] = product[ApiSchema.product.id];
  payload[MongoSchema.product.name] = product[ApiSchema.product.name];
  payload[MongoSchema.product.price] = product[ApiSchema.product.price];
  payload[MongoSchema.product.category] = product[ApiSchema.product.category];
  payload[MongoSchema.product.description] = product[ApiSchema.product.description];
  payload[MongoSchema.product.location] = {type: "point",
					   coordinates: [product[ApiSchema.product.longitude],
							 product[ApiSchema.product.latitude]]
					  };
  return payload;
};

module.exports = {SchemaAdapter};
