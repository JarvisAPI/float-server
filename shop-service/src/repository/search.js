'use strict';

/**
 *
 * Expected fields in the search package:
 *   searchSettings: object containing search settings.
 *   logger: winston logger.
 *   request: request callback library object.
 */
function SearchAdapter(searchPackage) {
  this.logger = searchPackage.logger;
  this.request = searchPackage.request;
  this.searchSettings = searchPackage.searchSettings;
  this.searchHost = constructSearchHost(this.searchSettings);
}

function constructSearchHost(searchSettings) {
  let proto = searchSettings.proto;
  let host = searchSettings.host;
  let port = searchSettings.port;
  return `${proto}://${host}:${port}`;
}

/**
 * Search a local product by its name.
 * text: (String) text used for searching.
 * from: (Integer) return results starting from.
 * size: (Integer) total number of results to return.
 * location: (Array) GeoJson array index 0 is longitude and 1 is latitude.
 * distance: (Number or String) distance from location in kilometers.
 * 
 * return: Promise that resolves with the array of products.
 */
SearchAdapter.prototype.searchLocalProductByName = function(text, from, size, location, distance) {
  let body = {
    from,
    size,
    query: {
      bool: {
	must: {
	  match: {
            product_name: {
              query: text,
              operator: 'and'
            }
	  }
	},
	filter: {
	  geo_distance: {
	    distance: `${distance}km`,
	    'location.coordinates': location
	  }
	}
      }
    }
  };
  return query.call(this, body);
};

/**
 * Search a shop product by its name.
 * text: (String) text used for searching.
 * from: (Integer) return results starting from.
 * size: (Integer) total number of results to return.
 * shop_id: (String) id of the shop to search products in.
 *
 * return: Promise that resolves with array of products.
 */
SearchAdapter.prototype.searchShopProductByName = function(text, from, size, shop_id) {
  let body = {
    from,
    size,
    query: {
      bool: {
	must: {
	  match: {
	    product_name: {
	      query: text,
	      operator: 'and'
	    }
	  }
	},
	filter: {
	  term: {
	    shop_id
	  }
	}
      }
    }
  };
  return query.call(this, body);
};

function query(body) {
  let path = `/${this.searchSettings.index}/_search`;
  return new Promise((resolve, reject) => {
    this.request.post({
      url: `${this.searchHost}${path}`,
      body,
      json: true
    }, (err, response, body) => {
      if (err) {
        reject(err);
        return;
      }
      try {
        this.logger.debug('Body: %j', body);
        let items = body.hits.hits;
        let products = [];
        for (let i = 0; i < items.length; i++) {
          products.push(items[i]._source);
        }
        resolve(products);
      } catch(err) {
        reject(err);
      }
    })
      .auth('elastic', 'changeme');
  });  
}

module.exports = SearchAdapter;
