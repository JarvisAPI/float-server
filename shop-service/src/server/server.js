'use strict';

const spdy = require('spdy');
const express = require('express');
const helmet = require('helmet');
const {api} = require('../api/shop');
const bodyParser = require('body-parser');
const {asValue} = require('awilix');

const start = (container) => {
  let logger = container.cradle.logger;
  // options contains:
  //  port: port to listen on.
  //  repo: the repository used to access the database.
  return new Promise((resolve, reject) => {
    if (!container.cradle.repo) {
      reject(new Error('The server must be started with a connected repository'));
      return;
    }
    if (!container.cradle.serverSettings.port) {
      reject(new Error('The server must be started with an available port'));
      return;
    }

    const app = express();
    if (!process.env.PROD) {
      let morgan = require('morgan');
      app.use(morgan('dev'));
    }
    app.use(helmet());
    app.use(bodyParser.json());

    logger.info('Setting up api path...');
    api(app, container);
    
    app.use((err, req, res, next) => {
      if (!err) {
	    return;
      }
      logger.error(err);
      if (err.sendAsJson) {
        res.status(err.status).json(err);
        return;
      }
      let msg = err.message ? err.message : 'Something went wrong!';
      let statusCode = err.status ? err.status : 500;
      res.status(statusCode).send(msg);
    });

    if (process.env.TEST === '1') {
      const server = app.listen(container.cradle.serverSettings.port, () => resolve(server));
    } else {
      const server = spdy.createServer(container.cradle.serverSettings.ssl, app)
    	    .listen(container.cradle.serverSettings.port, () => resolve(server));
    }
  });
};


module.exports = Object.assign({}, {start});
