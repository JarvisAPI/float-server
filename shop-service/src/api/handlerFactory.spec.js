
"use strict";

const should = require("should");
const sinon = require("sinon");
const logger = require("../../unit-test/logger.js");
const {createContainer, asValue} = require("awilix");
const handlerFactory = require("./handlerFactory.js");
const {MongoSchema} = require("../models/mongoSchema.js");

const container = createContainer();

const repo = {};

container.register({
  logger: asValue(logger),
  searchAdapter: asValue({}),
  repo: asValue(repo),
  uploadSettings: asValue({}),
  request: asValue({})
});

describe("handlerFactory.js", function() {
  let handler = handlerFactory.make(container);

  describe("swapShopMainImage", function() {
    it("valid inputs", (done) => {
      let req = {params: {}, body: {}};
      req.params.shop_id = "id";
      req.body.image_file = "file";

      repo.localGetShopInfo = (shop_id, fields) => {
        return new Promise((resolve, reject) => {
          let shop = {};
          shop[MongoSchema.shop.image_files] = [req.body.image_file];
          resolve(shop);
        });
      };
      repo.localGetShopInfo = sinon.spy(repo.localGetShopInfo);

      repo.deleteShopImages = (shop_id, image_files) => {
        return Promise.resolve();
      };
      repo.deleteShopImages = sinon.spy(repo.deleteShopImages);

      repo.setShopMainImageFile = (shop_id, image_file) => {
        return Promise.resolve();
      };
      repo.setShopMainImageFile = sinon.spy(repo.setShopMainImageFile);

      let res = {};
      res.sendStatus = sinon.spy();

      let next = (err) => console.log("Error");
      handler.swapShopMainImage(req, res, next);

      should(repo.localGetShopInfo.calledWith(req.params.shop_id, [MongoSchema.shop.image_files])).be.true();
      should(repo.localGetShopInfo.calledOnce).be.true();

      repo.localGetShopInfo.returnValues[0]
        .then(() => {
          should(repo.deleteShopImages.calledWith(req.params.shop_id, [req.body.image_file])).be.true();
          should(repo.deleteShopImages.calledOnce).be.true();
          return Promise.resolve();
        })
        .then(() => repo.deleteShopImages.returnValues[0])
        .then(() => {
          should(repo.setShopMainImageFile.calledWith(req.params.shop_id, req.body.image_file)).be.true();
          should(repo.setShopMainImageFile.calledOnce).be.true();          
          return Promise.resolve();
        })
        .then(() => repo.setShopMainImageFile.returnValues[0])
        .then(() => {
          should(res.sendStatus.calledWith(200)).be.true();
          should(res.sendStatus.calledOnce).be.true();
          done();
        })
        .catch(done);
    });
  });
});
