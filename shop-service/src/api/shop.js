"use strict";

const handlerFactory = require("./handlerFactory.js");

const setCacheHeader = (res, maxAge) => {
  res.set("Cache-Control", "public, max-age=" + maxAge);
};

const api = (app, container) => {
  let handler = handlerFactory.make(container);

  app.get("/shop/:shop_id", handler.getShopInfo);

  app.delete("/shop/:shop_id", handler.removeShop);

  app.get("/shop/:shop_id/product/:product_id", handler.getShopProductInfo);

  /**
   *
   * Query parameters:
   *   query: the text query used for searching.
   *   offset: the number of products to skip.
   *   limit: the maximum number of products to retrieve.
   *
   *   location: the location to search products from, GeoJSON array type. If
   *     this is set then shop_id does not need to be passed in.
   *   distance: the distance from location to limit search results.
   *
   *   shop_id: the shop to search from, if this is set then location does not
   *     need to be passed in.
   */
  app.get("/product/search", handler.search);

  app.delete("/shop/:shop_id/product/:product_id", handler.removeShopProduct);

  // Get a list of products by there basic info.
  app.get("/shop/:shop_id/product/:offset/:limit", handler.getShopProductsBasicInfo);

  app.post("/shop", handler.createShop);

  app.put("/shop/:shop_id", handler.updateShopInfo);

  app.post("/shop/:shop_id/product", handler.createShopProduct);

  app.put("/shop/:shop_id/product/:product_id", handler.updateShopProductInfo);

  app.post("/shop/:shop_id/image", handler.addShopImage);

  app.post("/shop/:shop_id/main-image", handler.setShopMainImage);

  app.put("/shop/:shop_id/main-image", handler.swapShopMainImage);

  app.delete("/shop/:shop_id/image/:image_files", handler.deleteShopImages);

  // Only adds an image to an existing product.
  app.post("/shop/:shop_id/product/:product_id/main-image", handler.setProductMainImage);

  app.post("/shop/:shop_id/product/:product_id/image", handler.addProductImage);

  app.put("/shop/:shop_id/product/:product_id/main-image", handler.swapProductMainImage);

  app.delete("/shop/:shop_id/product/:product_id/main-image", handler.deleteProductMainImage);

  /**
   * Modify product's image files by removing some.
   */
  app.put("/shop/:shop_id/product/:product_id/images", handler.deleteProductImages);

  app.get("/shop/:shop_ids/main-image", handler.getManyShopMainImage);
};

module.exports = {api};
