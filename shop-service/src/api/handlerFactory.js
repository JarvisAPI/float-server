"use strict";

const {ApiSchema} = require("../models/apiSchema.js");
const {MongoSchema} = require("../models/mongoSchema.js");

const setCacheHeader = (res, maxAge) => {
  res.set("Cache-Control", "public, max-age=" + maxAge);
};

let getMaxShopImageLimit = () => 10;
let getMaxProductImageLimit = () => 10;

if (process.env.NODE === "test") {
  // For testing both limits are assumed to be 10.
  getMaxShopImageLimit = () => 10;
  getMaxProductImageLimit = () => 10;
}

// Contains the handler functions for the endpoints.
function make(container) {
  let logger = container.cradle.logger;
  let searchAdapter = container.cradle.searchAdapter;
  let repo = container.cradle.repo;
  let uploadSettings = container.cradle.uploadSettings;
  let imageUrlBase = `${uploadSettings.proto}://${uploadSettings.image_server_host}:${uploadSettings.port}`;
  let request = container.cradle.request;
  
  const handler = {};
  
  handler.getShopInfo = function (req, res, next) {
    repo.getShopInfo(req.params.shop_id)
      .then(shop => {
	    setCacheHeader(res, 60);
	    res.status(200).json({shop});
      })
      .catch(next);
  };

  handler.removeShop = function (req, res, next) {
    repo.removeShop(req.params.shop_id)
      .then(() => res.sendStatus(200))
      .catch(next);
  };

  handler.getShopProductInfo = function (req, res, next) {
    repo.getShopProductInfo(req.params.shop_id, req.params.product_id)
      .then(product => {
	setCacheHeader(res, 60);
        res.status(200).json({product});
      })
      .catch(next);    
  };
  
  handler.search = function (req, res, next) {
    let query = req.query.query;
    let from = parseInt(req.query.offset);
    if (!from) {
      from = 0;
    }
    let size = parseInt(req.query.limit);
    if (!size) {
      size = 5;
    }
    if (!query) {
      res.status(400).send("query not passed in!");
      return;
    }
    let maxSize = 20;
    if (size > maxSize) {
      size = maxSize;
    }    
    let shop_id = req.query.shop_id;
    let location = null;
    if (!shop_id) {
      location = JSON.parse(req.query.location);
      if (!location) {
	    res.status(400).send("Location or Shop Id must be supplied!");
	    return;
      }
      let distance = req.query.distance;
      if (!distance) {
	    distance = "12";
      }
      searchAdapter.searchLocalProductByName(query, from, size, location, distance)
        .then((product_list) => {
          logger.debug("Products: %j", product_list);
          res.status(200).json({product_list});
        })
        .catch(next);
    } else {
      searchAdapter.searchShopProductByName(query, from, size, shop_id)
	    .then((product_list) => {
          logger.debug('Products: %j', product_list);
          res.status(200).json({product_list});
        })
	    .catch(next);
    }
  };

  handler.removeShopProduct = function (req, res, next) {
    repo.removeShopProduct(req.params.shop_id, req.params.product_id)
      .then(() => res.sendStatus(200))
      .catch(next);    
  };

  handler.getShopProductsBasicInfo = function (req, res, next) {
    repo.getShopProductsBasicInfo(req.params.shop_id, parseInt(req.params.limit), parseInt(req.params.offset))
      .then(product_list => {
	    setCacheHeader(res, 60);
        res.status(200).json({product_list});
      })
      .catch(next);
  };

  handler.createShop = function (req, res, next) {
    let shop = req.body.shop;
    ApiSchema.validate(shop, "shop")
      .then(() => repo.createShop(shop))
      .then(() => res.sendStatus(200))
      .catch(next);
  };

  handler.updateShopInfo = function (req, res, next) {
    let shop_edit = req.body.shop_edit;
    ApiSchema.validate(shop_edit, "shop_edit")
      .then(() => repo.updateShopInfo(req.params.shop_id, shop_edit))
      .then(() => res.status(200).send())
      .catch(next);
  };

  handler.createShopProduct = function (req, res, next) {
    let product = req.body.product;
    let shop_id = req.params.shop_id;
    if (!product) {
      res.sendStatus(400);
      return;
    }
    repo.getShopInfo(shop_id)
      .then(result => {
        let coord = result[MongoSchema.shop.location].coordinates;
        product[ApiSchema.product.longitude] = coord[0];
        product[ApiSchema.product.latitude] = coord[1];
        return;
      })
      .then(() => ApiSchema.validate(product, "product"))
      .then(() => repo.createShopProduct(req.params.shop_id, product))
      .then(() => res.status(200).send())
      .catch(next);
  };

  handler.updateShopProductInfo = function (req, res, next) {
    let product_edit = req.body.product_edit;
    if (Object.keys(product_edit).length === 0) {
      res.sendStatus(200);
      return;
    }
    ApiSchema.validate(product_edit, "product_edit")
      .then(() => repo.updateShopProductInfo(req.params.shop_id, req.params.product_id, product_edit))
      .then(() => res.status(200).send())
      .catch(next);
  };

  const checkShopImageLimit = (shop_id) => {
    return repo.localGetShopInfo(shop_id, [MongoSchema.shop.image_files])
      .then(shop => {
        if (!shop[MongoSchema.shop.image_files] ||
            shop[MongoSchema.shop.image_files].length < getMaxShopImageLimit()) {
          return Promise.resolve();
        }
        let err = new Error();
        err.message = "Shop Image Limit Reached";
        err.status = 400;
        throw err;
      });
  };

  handler.addShopImage = function (req, res, next) {
    let shop_id = req.params.shop_id;

    checkShopImageLimit(shop_id)
      .then(() => {
        let image_url = `${imageUrlBase}/image`;
        return new Promise((resolve, reject) => {
          req.pipe(request({url: image_url}, (err, response, body) => {
            if (err) {
              reject(err);
              return;
            }
            try {
              resolve(JSON.parse(body));
            } catch(jerr) {
              reject(jerr);
            }
          }));
        });
      })
      .then(body => repo.addShopImageFile(shop_id, body.filename))
      .then(() => res.sendStatus(200))
      .catch(next);
  };

  handler.setShopMainImage = function (req, res, next) {
    let shop_id = req.params.shop_id;

    checkShopImageLimit(shop_id)
      .then(() => {
        let image_url = `${imageUrlBase}/image`;
        return new Promise((resolve, reject) => {
          req.pipe(request({url: image_url}, (err, response, body) => {
            if (err) {
              reject(err);
              return;
            }
            try {
              resolve(JSON.parse(body));
            } catch(jerr) {
              reject(jerr);
            }
          }));
        });
      })
      .then(body => repo.setShopMainImageFile(shop_id, body.filename))
      .then(() => res.sendStatus(200))
      .catch(next);    
  };

  handler.swapShopMainImage = function (req, res, next) {
    let shop_id = req.params.shop_id;
    let image_file = req.body.image_file;
    
    repo.localGetShopInfo(shop_id, [MongoSchema.shop.image_files])
      .then(shop => {
        let image_files = shop[MongoSchema.shop.image_files];
        if (image_files.indexOf(image_file) > -1) {
          return repo.deleteShopImages(shop_id, [image_file]);
        }
        let err = new Error("Non existent shop image file");
        err.status = 400;
        throw err;
      })
      .then(() => repo.setShopMainImageFile(shop_id, image_file))
      .then(() => res.sendStatus(200))
      .catch((err) => {
        if (err.status) {
          logger.error(err);
          res.sendStatus(err.status);
        } else {
          next(err);
        }
      });
  };

  handler.deleteShopImages = function (req, res, next) {
    let image_files = req.params.image_files;
    let jarr = image_files;
    try {
      image_files = JSON.parse(image_files);
    } catch(jerr) {
      res.sendStatus(400);
      return;
    }
    if (image_files && Array.isArray(image_files)) {
      let url = `${imageUrlBase}/image/${jarr}`;
      repo.deleteShopImages(req.params.shop_id, image_files)
        .then(() => new Promise((resolve, reject) => {
          request({method: "DELETE", url}, (err, response, body) => {
            if (err) {
              reject(err);
              return;
            }
            try {
              resolve(JSON.parse(body));
            } catch(jerr) {
              // Ignore parsing error.
              resolve();
            }
          });
        }))
        .then(() => res.sendStatus(200))
        .catch(next);
    } else {
      res.sendStatus(400);
    }
  };

  /**
   * Check that a product doesn't exceed its image limits.
   * @return promise that resolves a product with keys:
   *   MongoSchema.product.main_image_file
   *   MongoSchema.product.image_files
   */
  const checkProductImageLimit = (shop_id, product_id) => {
    return repo.localGetShopProductInfo(shop_id, product_id, [MongoSchema.product.main_image_file,
                                                              MongoSchema.product.image_files])
      .then(product => {
        let numProductImages = 0;
        if (product[MongoSchema.product.main_image_file]) {
          numProductImages++;
        }
        if (product[MongoSchema.product.image_files]) {
          numProductImages += product[MongoSchema.product.image_files].length;
        }
        if (numProductImages < getMaxProductImageLimit()) {
          return Promise.resolve(product);
        }
        let err = new Error();
        err.message = "Product Image Limit Reached";
        err.status = 400;
        throw err;
      });        
  };

  handler.setProductMainImage = function (req, res, next) {
    let product_id = req.params.product_id;
    let shop_id = req.params.shop_id;

    let old_main_image_file = null;
    
    checkProductImageLimit(shop_id, product_id)
      .then(product => {
	    old_main_image_file = product[MongoSchema.product.main_image_file];
	    let image_url = `${imageUrlBase}/image`;
	    return new Promise((resolve, reject) => {
	      req.pipe(request({url: image_url}, (err, response, body) => {
	        if (err) {
	          reject(err);
              return;
	        }
            try {
	          resolve(JSON.parse(body));
            } catch(jerr) {
              reject(jerr);
            }
	      }));
	    });
      })
      .then(body => repo.setProductMainImageFile(product_id, body.filename, old_main_image_file))
      .then(() => res.sendStatus(200))
      .catch(next);
  };

  handler.addProductImage = function (req, res, next) {
    let product_id = req.params.product_id;
    let shop_id = req.params.shop_id;

    checkProductImageLimit(shop_id, product_id)
      .then(() => {
        let image_url = `${imageUrlBase}/image`;
	    return new Promise((resolve, reject) => {
          req.pipe(request({url: image_url}, (err, response, body) => {
            if (err) {
	          reject(err);
            }
            try {
	          resolve(JSON.parse(body));
            } catch(jerr) {
              reject(jerr);
            }
	      }));
	    });
      })
      .then(body => repo.addProductImageFile(product_id, body.filename))
      .then(() => res.sendStatus(200))
      .catch(next);
  };

  handler.swapProductMainImage = function (req, res, next) {
    let old_main_image_file = req.body.old_main_image_file;
    let new_main_image_file = req.body.new_main_image_file;
    if (old_main_image_file && new_main_image_file) {
      repo.swapProductMainImage(req.params.shop_id, req.params.product_id,
                                old_main_image_file, new_main_image_file)
        .then(() => res.sendStatus(200))
        .catch(next);
    } else {
      res.sendStatus(400);
    }
  };

  handler.deleteProductMainImage = function (req, res, next) {
    repo.deleteProductMainImage(req.params.shop_id, req.params.product_id)
      .then(main_image_file => new Promise((resolve, reject) => {
        let image_files = [main_image_file];
        let jarr = JSON.stringify(image_files);
        let url = `${imageUrlBase}/image/${jarr}`;
        request({method: "DELETE", url}, (err, response, body) => {
          if (err) {
            reject(err);
            return;
          }
          resolve();
        });
      }))
      .then(() => res.sendStatus(200))
      .catch(next);
  };

  handler.deleteProductImages = function (req, res, next) {
    let image_files = req.body.image_files;
    if (image_files && Array.isArray(image_files)) {
      let jarr = JSON.stringify(image_files);      
      let url = `${imageUrlBase}/image/${jarr}`;
      repo.deleteProductImages(req.params.shop_id, req.params.product_id, image_files)
        .then(() => new Promise((resolve, reject) => {
          request({method: "DELETE", url}, (err, response, body) => {
            if (err) {
              reject(err);
              return;
            }
            try {
              resolve(JSON.parse(body));
            } catch(jerr) {
              // Ignore parsing error.
              resolve();
            }
          });
        }))
        .then(() => res.sendStatus(200))
        .catch(next);
    } else {
      res.sendStatus(400);
    }
  };

  handler.getManyShopMainImage = function(req, res, next){
    logger.debug("getting shop main image")	  
    let shop_ids = JSON.parse(req.params.shop_ids);
    if(!shop_ids){
      res.sendStatus(400);
      return;
    }
    repo.getManyShopMainImage(shop_ids)
      .then((shopImages) => {
        res.status(200).send(shopImages)
      })
      .catch((err)=>{
        res.sendStatus(400)
      })
  }

  return handler;
};

module.exports = {
  make
};
