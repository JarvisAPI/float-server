'use strict';

const fs = require('fs');

const serverSettings = {
    port: process.env.PORT || 3000,
    ssl: require('./ssl')
};

const uploadSettings = {
  proto: process.env.TEST === '1' ? 'http' : 'https',
  port: process.env.IMAGE_SERVER_PORT || 3000,
  image_server_host: process.env.IMAGE_SERVER_HOST
};

const requestOptions = {
  CA: fs.readFileSync(__dirname + '/ssl/ca.pem')
};

const dbSettings = {
  db: process.env.DB || 'shop_db',
  user: process.env.DB_USER || 'shop_service',
  pwd: process.env.DB_PASS || 'TCPmyClientMapAppSocketv1.0',  
  host: process.env.DATABASE_HOST || '127.0.0.1',
  port: 27017
};

const searchSettings = {
  host: process.env.SEARCH_SERVER_HOST,
  port: process.env.SEARCH_SERVER_PORT || 9200,
  proto: 'http',
  index: dbSettings.db
};

module.exports = Object.assign({serverSettings, dbSettings, uploadSettings, requestOptions,
                               searchSettings});
