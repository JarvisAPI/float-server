const configSettings = require('./config');
const database = require('./db');
const {initDI} = require('./di');

const init = initDI.bind(null, Object.assign(configSettings, {database}));

module.exports = Object.assign({}, {init});
