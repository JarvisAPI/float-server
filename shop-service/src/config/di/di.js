'use strict';

const {createContainer, asValue} = require('awilix');
var request = require('request');
const SearchAdapter = require('../../repository/search.js');

const winston = require('winston');
const winstonError = require('winston-error');
const config = winston.config;
const logger = new (winston.Logger) ({
  transports: [
    new (winston.transports.Console) ({
      formatter: function(options) {
	    return `[${new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')}]`+
          '[' +
	      config.colorize(options.level, options.level.toUpperCase()) +
	      ']: ' +
	      (options.message && !options.meta.error ? options.message : '') +
          (options.meta.error ? options.meta.error.stack : '');
      }
    })
  ]
});

winstonError(logger, {
  decoratedLevels: ['error']
});

function initDI({serverSettings, dbSettings, uploadSettings, requestOptions,
                 searchSettings, database}, mediator) {
  mediator.once('init', () => {
    if (process.env.DEBUG === '1') {
      logger.level = 'debug';	
    }
    if (process.env.TEST !== '1') {
      request = request.defaults({
        ca: requestOptions.CA
      });
    }
    mediator.on('db_client.ready', (client) => {
      const searchPackage = {
        searchSettings,
        logger,
        request
      };
      let searchAdapter = new SearchAdapter(searchPackage);
 
      const container = createContainer();
      container.register({
        db_client: asValue(client),
        serverSettings: asValue(serverSettings),
        dbSettings: asValue(dbSettings),
        uploadSettings: asValue(uploadSettings),
        logger: asValue(logger),
        request: asValue(request),
        searchAdapter: asValue(searchAdapter)
      });
      
      mediator.emit('di.ready', container);
    });
    
    mediator.on('db_client.error', (err) => {
      mediator.emit('di.error', err);
    });

    database.connect(dbSettings, mediator);
    mediator.emit('boot.ready');
  });
};

module.exports.initDI = initDI;
