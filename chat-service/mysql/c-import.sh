#!/bin/bash
DB_USER="ejabberd"
DB_PASS="password"
DB="ejabberd"

echo "delimiter |
CREATE TRIGGER increment BEFORE INSERT ON archive
FOR EACH ROW
 BEGIN
    INSERT INTO dialog (username, timestamp, bare_peer, xml, unread_count, created_at, txt)
    values (NEW.username, NEW.timestamp, SUBSTRING_INDEX(NEW.bare_peer, '@', 1) , NEW.xml,1,NEW.created_at, NEW.txt )
    ON DUPLICATE KEY UPDATE  timestamp = NEW.timestamp, xml = NEW.xml ,unread_count = unread_count + 1, created_at = NEW.created_at, txt = NEW.txt;
 END|
delimiter ;" | mysql -u "$DB_USER" --password="$DB_PASS" --database="$DB" ;
