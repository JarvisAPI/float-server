// make sure to set environmental variable NODE='test' when calling npm start

'use strict';

const request = require('supertest');
const should = require('should');
const isPortReachable = require('is-port-reachable');

describe('user-service', function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // Need a long timeout for exponential backoff to work.
  this.timeout(20000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      console.log("before test");
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(7, () => isPortReachable(port, {host: host}));
    };
    return tryConnect(url);
  });

  it('trying to create a user from nothing should return bad request', (done) => {
    console.log("starting test2");
    api.post('/chat')
      .expect(404, done);
  });

  it('set a dialog to 0 barepeer:james1 should be set to 0', (done) => {
    console.log("starting test3");
    let checkIfSuccess = (err,res) => {
      if(err){
	       throw err;
      }
      api.get('/chat/user/jcho123/dialog').expect(200, function(err,res) {
        if(err){
          done(Error("something went wrong while retriving dialog list"));
        }
      	let result = res.body.dialog_list;
        let count = 0;
        for(let i = 0 ; i < result.length; i ++ ){
	        if(result[i].unread_count === 0 && result[i].bare_peer === "james1"){
            count ++;
          }
        }
        count.should.equals(1);
        done();
      });
    };
    api.put('/chat/user/jcho123/dialog/james1/0').expect(200, checkIfSuccess);
  });

  it('add user', (done) => {
    console.log("starting test4");
    let user = {
      username: 'jid1'
    };
    api.post('/chat/user').send(user).expect(200, done);
  });

  it('delete user', (done) => {
    let user = {
      username: 'jid1'
    };
    api.delete('/chat/user').send(user).expect(200, done);
  });

  it('get empty dialog list', (done) => {
    let user = {
      username: 'jid1'
    };
    api.get('/chat/user/test/dialog').expect(200, function(err,res){
      if(err){
	       throw err;
      }
      (res.body.dialog_list.length).should.equals(0);
      done();
    });
  });

  it('user gen and token gen test', (done) => {
    let user = {
      username: 'jid1'
    };
    var initalAuthToken;
    var updatedAuthToken;
    var currentAuthToken;

    let checkCurrentAuthToken = (err,res) => {
      if(err){
        done(err);
        return;
      }
      currentAuthToken = res.body.auth_token;
      currentAuthToken.should.equal(updatedAuthToken);
      done();
    };

    let checkUpdatedAuthToken = (err,result) => {
      if(err){
        done(err);
        return;
      }
      updatedAuthToken = result.body.auth_token;
      updatedAuthToken.should.not.equal(initalAuthToken);
      api.get('/chat/user/'+ user.username +'/password').expect(200, checkCurrentAuthToken);
    };

    let getToken = (err, res) => {
      if(err){
        done(err);
        return;
      }
      initalAuthToken = res.body.auth_token;
      api.post('/chat/user/'+ user.username +'/password').expect(200, checkUpdatedAuthToken);
    };
    api.post('/chat/user').send(user).expect(200, getToken);
  });

  it('update notification token and get it', (done) => {
    let user = {
      username: 'jid1'
    };
    var token = "12345";

    let checkIfSuccess = (err,result) => {
      if(err){
        done(err);
        return;
      }
    api.get('/chat/user/'+ user.username +'/notificationToken').expect(200, function(err,res) {
        if(err){
          done(err);
          return;
        }
        let noteToken = res.body.notification_token;
        console.log("noteToken:" + noteToken);
        if((typeof noteToken) !== "string" || token != noteToken){
          done(Error("Error token not generated"));
          return;
        }
        done();
      });
    };
    api.post('/chat/user/' + user.username +'/notificationToken/' + token).expect(200, checkIfSuccess);
  });

  // further tests are done in the api gateway intergration tests
  it('add image to image as a private image', (done) => {
    api.post('/chat/user/user1/dialog/user2/image')
    .attach('image', 'test-image1.jpg')
    .expect(200,done);
  });

});
