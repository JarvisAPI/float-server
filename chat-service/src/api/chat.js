'use strict';

const hal = require('hal');

module.exports = (app, container) => {
  const logger = container.cradle.logger;
  const repo = container.cradle.repo;
  const fcm = container.cradle.notificationServerContact;
  let request = container.cradle.request;

  let uploadSettings = container.cradle.uploadSettings;
  let imageUrlBase = `${uploadSettings.proto}://${uploadSettings.image_server_host}:${uploadSettings.port}`;

  app.put('/chat/user/:username/dialog/:barePeer/:count', (req, res, next) => {
    let username = req.params.username;
    let barePeer = req.params.barePeer;
    let count = req.params.count;
    repo.markDialogAsRead(username, barePeer, count)
      .then(() => {
	res.status(200).send();
      }, (err) => {
	logger.error(err);
	res.status(400).send();
      });
  });

  app.get('/chat/user/:username/dialog', (req, res, next) => {
    let username = req.params.username;
    repo.getDialogList(username)
      .then((dialogList) => {
        var response = {dialog_list: dialogList};
        logger.debug(response);
	res.status(200).send(response);
      })
      .catch((err) => {
	logger.error(err);
	res.status(400).send();
      });
  });

  /*
   *  Adds a user to the database.
   */
  app.post('/chat/user',(req, res, next) => {
    let username = req.body.username;
    if(!username){
      res.status(400).send();
    }
    repo.addNewUser(username)
      .then((auth_token) => {
        var response = {auth_token};
        res.status(200).send(response);
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send(err);
      });
  });

  /*
   *  Deletes a user from the chat database
   */
  app.delete('/chat/user',(req, res, next) => {
    let username = req.body.username;
    repo.deleteUser(username)
      .then(() => {
        res.status(200).send();
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  /*
   *  Updates the password of a user and returns an authentication token
   */
  app.post('/chat/user/:username/password',(req, res, next) => {
    let username = req.params.username;
    repo.updatePassword(username).then((auth_token) => {
      var response = {auth_token};
      res.status(200).send(response);
    })
      .catch((err) => {
        logger.error(err);
        res.status(400).send(err);
      });
  });

  app.get('/chat/user/:username/password', (req, res, next) => {
    let username = req.params.username;
    repo.getAuthToken(username).then((auth_token) =>{
      var response = {auth_token};
      res.status(200).json(response);
    })
      .catch((err) => {
        logger.error(err);
        res.status(400).send(err);
      });
  });

  app.post('/chat/user/:username/notificationToken/:notificationToken', (req, res, next) => {
    let username = req.params.username;
    let notificationToken = req.params.notificationToken;
    repo.updateNotificationToken(username,notificationToken).then((result) =>{
      res.status(200).send();
    })
      .catch((err) => {
        logger.error(err);
        res.status(400).send(err);
      });
  });

  // Currently only used for testing
  app.get('/chat/user/:username/notificationToken', (req, res, next) => {
    let username = req.params.username;
    repo.getNotificationToken(username).then((notification_token) =>{
      let notificationToken = {notification_token};
      res.status(200).send(notificationToken);
    })
      .catch((err) => {
        logger.error(err);
        res.status(400).send(err);
      });
  });

  app.post('/chat/user/:username/dialog/:barePeer/image', (req, res, next) => {
    let permission_users = [req.params.username, req.params.barePeer];
    let image_url = `${imageUrlBase}/private_image?permission_users=${JSON.stringify(permission_users)}`;
    req.pipe(request({url: image_url}, (err, response, body) => {
      if (err) {
        logger.debug('error when requesting to image service');
        logger.error(err);
        res.status(500).send();
      } else {
        try {
          body = JSON.parse(body);
        } catch(err) {
          next(err);
        }
        let imageUrl = body.filename;
        res.status(200).send({image_url: imageUrl});
      }
    }));
  });

  app.post('/notification', (req, res, next) => {
    var receivedBody = req.body;
    var bodyToSend = {data: { to:receivedBody.to, body: receivedBody.body, bare_peer:receivedBody.from, message_id: receivedBody.messageId}};

    function flushNotiToken(notificationToken){
      repo.flushNotificationServerKey(receivedBody.to, notificationToken).then((result) =>{
        if(result.affectedRows !== 1){
          res.status(403).send();
        }
        else{

          res.status(200).send();
        }
      })
        .catch((err) => {
          logger.error(err);
          res.status(400).send(err);
        });
    }

    function sendfcmRequest(notificationToken){
      bodyToSend.to = notificationToken;
      let reqfields = {url: fcm.url, headers: fcm.header, json:bodyToSend , method:'POST', ca: null};
      request(reqfields, function (error, response, body) {
        if(error){
          logger.error(error);
          res.status(400).send(error);
        }
        if(body && body.failure >= 1 && body.results && body.results[0].error === 'NotRegistered'){
          flushNotiToken(notificationToken);
        }
        else{
          logger.info('error: ' + error);
          logger.info('statusCode:', response && response.statusCode);
          logger.info('body:', body);
          res.status(200).send();
        }
      });
    }

    repo.getNotificationToken(receivedBody.to).then((notification_token) =>{
      logger.info(notification_token);
      if(notification_token !== ''){
        sendfcmRequest(notification_token);
      }
      else{
        res.status(400).send();
      }
    })
      .catch((err) => {
        logger.error(err);
        res.status(400).send(err);
      });
  });
};
