'use strict';

const express = require('express');
const helmet = require('helmet');
const bodyparser = require('body-parser');
const api = require('../api/chat');
const spdy = require('spdy');

const start = (container) => {
  return new Promise((resolve, reject) => {
    const logger = container.cradle.logger;
    const {port, ssl} = container.cradle.serverSettings;
    const repo = container.cradle.repo;

    if (!repo) {
      reject(new Error('The server must be started with a connected repository'));
      return;
    }
    if (!port) {
      reject(new Error('The server must be started with an available port'));
      return;
    }

    const app = express();
    if (!process.env.PROD) {
      let morgan = require('morgan');      
      app.use(morgan('dev'));
    }
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({ extended: true }));
    app.use(helmet());

    api(app, container);

    app.use((err, req, res, next) => {
      reject(new Error('Something went wrong!, err:' + err));
      res.status(500).send('Something went wrong!');
      next();
    });    

    if (process.env.TEST === '1') {
      logger.info('Starting test server'); 
      const server = app.listen(port, () => resolve(server));
    } else {
      const server = spdy.createServer(ssl, app)
	    .listen(port, () => resolve(server));
    }
  });
};

module.exports = Object.assign({}, {start});
