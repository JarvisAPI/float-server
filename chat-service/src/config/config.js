'use strict';

const fs = require('fs');

const dbSettings = {
  db: process.env.MYSQL_DATABASE || 'ejabberd',
  user: process.env.MYSQL_USER || 'ejabberd',
  pwd: process.env.MYSQL_PASSWORD || 'password',
  host: process.env.DATABASE_HOST || '127.0.0.1',
  port: 3306
};

const serverSettings = {
  port: process.env.PORT || 3000,
  ssl: require('./ssl')
};

const requestOptions = {
  CA: fs.readFileSync(__dirname + '/ssl/ca.pem')
};

const uploadSettings = {
  proto: process.env.TEST === '1' ? 'http' : 'https',
  port: process.env.IMAGE_SERVER_PORT || 3000,
  image_server_host: process.env.IMAGE_SERVER_HOST
};

const notificationServerContact = {
  url: "https://fcm.googleapis.com/fcm/send",
  header:  {Authorization: 'key=AAAAmmYZRwg:APA91bFj3sz_h-Ycx2To1Ust1CeQ-EY6cHLzwYHnyV1jvv1lRNMn2egAlQ8wZJei_WSHV-kPPC2-tkyylEYCeLElhvlzDs3ra4WpYhCMmo0I92JuzypRSfuSc8GCg4dIuK4FDOL6dv8l', "Content-Type": 'application/json'}
};

module.exports = Object.assign({}, {requestOptions, uploadSettings, dbSettings, serverSettings, notificationServerContact});
