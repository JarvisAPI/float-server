const {requestOptions,uploadSettings, dbSettings, serverSettings, authSettings, notificationServerContact} = require('./config');
const database = require('./db');
const {initDI} = require('./di');
 
const init = initDI.bind(null, {requestOptions, uploadSettings, serverSettings, dbSettings, database, notificationServerContact});

module.exports = Object.assign({}, {init});
