'use strict';
const mysql = require('mysql');
const crypto = require('crypto');

const repository = (container) => {
  const db_client = container.resolve('db_client');
  const logger = container.resolve('logger');


  // Gets all dialogs for a user
  // Postcondition: returns a JSON array of dialogs
  const getDialogList = (username) => {
    return new Promise((resolve, reject) => {
       db_client.getConnection(function(err, connection){
          if(err){
              reject(err);
              return;
           }
           var query = "select * from dialog where username = ? order by timestamp asc ";
           var inserts = [username];
           connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
             connection.release();
             if (err) {
               reject(err);
               return;
             }
             if (result) {
               resolve(result);
             } else {
             reject(new Error('Dialog unable to get dialog list for user:' + username));
           }
         });
       });
     });
   };

  const markDialogAsRead = (username, barePeer, count) => {
    return new Promise((resolve, reject) => {
      db_client.getConnection(function(err, connection){
         if(err){
             reject(err);
             return;
          }
          let query = "UPDATE dialog SET unread_count = ? WHERE username = ?  AND bare_peer = ?";
          var inserts = [count,username,barePeer];
          connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
            connection.release();
            if (err) {
              reject(err);
              return;
            }
            resolve(result);
          });
        });
      });
    };

  const addNewUser = (username) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if(err){
          reject(err);
          return;
        }
        var authToken = buf.toString('hex');
        createUser(username, authToken).then((err, result) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(authToken);
        });
      });
    });
  };

  const deleteUser = (username) => {
    return new Promise((resolve, reject) => {
      db_client.getConnection(function(err, connection){
        if(err){
           reject(err);
           return;
        }
        let query = "DELETE FROM users WHERE username = ? ";
        var inserts = [username];
        connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
          connection.release();
          if (err) {
            reject(err);
            return;
          }
          resolve();
        });
      });
    });
  };

  // updates password of the database for a user. The password is just a temperary token
 const updatePassword = (username) => {
   return new Promise((resolve, reject) => {
     crypto.randomBytes(16, (err, buf) => {
       if(err){
         reject(err);
         return;
       }
       var authToken = buf.toString('hex');
       updatePasswordOnDataBase(username, authToken).then((err, result) => {
         if (err) {
           reject(err);
           return;
         }
         resolve(authToken);
       });
     });
   });
 };

 const getAuthToken = (username) => {
   return new Promise((resolve, reject) => {
     db_client.getConnection(function(err, connection){
       if(err){
          reject(err);
          return;
       }
       let query = "Select password FROM users WHERE username = ?";
       var inserts = [username];
       connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
         connection.release();
         if (err || (result && result.length != 1)){
           reject(err);
           return;
         }
         resolve(result[0].password);
       });
     });
   });
 };

 function updatePasswordOnDataBase(username,authToken){
   return new Promise((resolve, reject) => {
     db_client.getConnection(function(err, connection){
       if(err){
          reject(err);
          return;
       }
       let query = "UPDATE users SET password = ? WHERE username = ?";
       var inserts = [authToken, username];
       connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
         connection.release();
         if (err) {
           reject(err);
           return;
         }
         resolve();
       });
     });
   });
 };

 function createUser(username,authToken){
   return new Promise((resolve, reject) => {
     db_client.getConnection(function(err, connection){
       if(err){
          reject(err);
          return;
       }
       let query = "INSERT INTO users (username,password) VALUES(?, ?)";
       var inserts = [username, authToken];
       connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
         connection.release();
         if (err) {
           reject(err);
           return;
         }
         resolve();
       });
     });
   });
 };

 function updateNotificationToken(username, noteToken){
   return new Promise((resolve, reject) => {
     db_client.getConnection(function(err, connection){
       if(err){
          reject(err);
          return;
       }
       let query = "INSERT INTO notification(username, serverkey) VALUES(?,?) ON DUPLICATE KEY UPDATE serverkey = ?";
       var inserts = [username, noteToken, noteToken];
       connection.query({sql: query, timeout: 4000}, inserts, (err, result, fields) => {
         connection.release();
         if (err) {
           reject(err);
           return;
         }
         resolve();
       });
     });
   });
 }

 const getNotificationToken = (username) => {
   return new Promise((resolve, reject) => {
     db_client.getConnection(function(err, connection){
       if(err){
          reject(err);
          return;
       }
       let query = "Select serverkey FROM notification WHERE username = ?";
       var inserts = [username];
       connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
         connection.release();
         if (err || (result && result.length != 1 )) {
           reject(err);
           return;
         }
         resolve(result[0].serverkey);
       });
     });
   });
 };

 function flushNotificationServerKey(username,noteToken){
   return new Promise((resolve, reject) => {
     db_client.getConnection(function(err, connection){
       if(err){
          reject(err);
          return;
       }
       let query = "UPDATE notification SET serverkey = '' WHERE username = ? AND serverkey = ?";
       var inserts = [username, noteToken];
       connection.query({ sql: query, timeout: 4000}, inserts, (err, result, fields) => {
         connection.release();
         if (err) {
           reject(err);
           return;
         }
         resolve(result);
       });
     });
   });
 };

  const disconnect = () => {
    db_client.end();
  };

  return {
    getDialogList,
    markDialogAsRead,
    addNewUser,
    deleteUser,
    updatePassword,
    getAuthToken,
    updateNotificationToken,
    getNotificationToken,
    flushNotificationServerKey,
    disconnect
  };
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve('db_client')) {
      reject(new Error('Connection db client not supplied!'));
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});
