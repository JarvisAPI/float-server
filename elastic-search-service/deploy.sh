#!/bin/bash

docker rm sleepy_chancellor
docker run --name sleepy_chancellor \
       -d \
       -p 9200:9200 \
       -p 9300:9300 \
       -e "discovery.type=single-node" elastic-search
