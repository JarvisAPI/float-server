#!/bin/bash

mongo="${MONGO:-mongo}"
mongoport="${MONGOPORT:-27017}"
elasticsearch="${ELASTICSEARCH:-elasticsearch}"
elasticport="${ELASTICPORT:-9200}"


function _mongo() {
        mongo --quiet --host ${mongo} --port ${mongoport} <<EOF
        $@
EOF
}

is_master_result="false"
expected_result="true"

timeout=20
slen=3
success="false"
while [ "$timeout" -gt "0" ];
do
    if [ "${is_master_result}" == "${expected_result}" ] ; then
        is_master_result=$(_mongo "rs.isMaster().ismaster")
        echo "Waiting for Mongod node to assume primary status..."
        timout=$((timeout-slen))
        sleep $slen
    else
        echo "Mongod node is now primary"
        success="true"
        break;
    fi
done

if [ "${success}" == "false" ]; then
    exit 1
fi

sleep 1

mongo-connector -m ${mongo}:${mongoport} \
                -t ${elasticsearch}:${elasticport} \
                -d elastic2_doc_manager \
                --continue-on-error \
                --admin-username root \
                --password-file /tmp/mongo-root-password

echo "Script Ended!"

while :
do
    sleep 1
done
