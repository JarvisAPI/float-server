#!/bin/bash

index="shop_db"

curl -XPUT -u elastic:changeme "http://localhost:9200/${index}" \
     -d @config.json \
     --header "Content-Type: application/json"

#types=("shop" "product")
#
#for ttype in ${types[@]}
#do
#    curl -XPUT -u elastic:changeme "http://localhost:9200/${index}/_mapping/${ttype}" \
#         -d @${ttype}.json \
#         --header "Content-Type: application/json"
#done
