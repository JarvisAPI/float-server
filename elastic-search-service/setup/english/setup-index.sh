#!/bin/bash
scriptdir="$(dirname "$0")"
cd $scriptdir

# CMD index port
proto="http"
host="localhost"
port=9200
index=
while getopts "c:h:p:i:" opt; do
    case ${opt} in
        c )
            proto=${OPTARG}
            ;;
        h )
            host=${OPTARG}
            ;;
        p )
            port=${OPTARG}
            ;;
        i )
            index=${OPTARG}
            ;;
        \? )
            echo "Usage cmd [-c protocol:=http|https] [-h host] [-p port] -i index"
    esac
done

url="${proto}://${host}:${port}/${index}?pretty=1"
curl -XPUT -u elastic:changeme $url \
     -d @config.json \
     --header "Content-Type: application/json"
