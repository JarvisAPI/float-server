#!/bin/bash

proto="http"
host="localhost"
port=9200
index="$1"

url="${proto}://${host}:${port}/${index}?pretty=1"
curl -XDELETE -u elastic:changeme $url
