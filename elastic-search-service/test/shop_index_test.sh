#!/bin/bash

text=$1
body='{"text":'\"${text}\"'}'
curl -XGET -u elastic:changeme localhost:9200/shop_db/_analyze?pretty=1 -d $body --header "Content-Type: application/json; charset=utf-8"
