#!/bin/bash

text="$1"
#body='{"size":5,"query":{"match":{"product_name":{"query":'\"${text}\"',"operator":"and"}}}}'

body()
{
    cat <<EOF
{
  "from": 0,
  "size": 5,
  "query": {
    "match": {
      "product_name": {
        "query": "$text",
        "operator": "and"
      }
    }
  }
}
EOF
}
curl -XPOST -v -u elastic:changeme \
     --data "$(body)" \
     localhost:9200/shop_db/_search?pretty=1 \
     --header "Content-Type: application/json; charset=utf-8"
