#!/bin/bash

text="$1"
#body='{"size":5,"query":{"match":{"product_name":{"query":'\"${text}\"',"operator":"and"}}}}'

body()
{
    cat <<EOF
{
  "from": 0,
  "size": 5,
  "query": {
    "bool": {
      "must": {
        "match": {
          "product_name": {
            "query": "$text",
            "operator": "and"
          }
        }
      },
      "filter": {
        "term": {
          "shop_id": "test-shop-id"
        }
      }
    }
  }
}
EOF
}
curl -XPOST -v -u elastic:changeme \
     --data "$(body)" \
     localhost:9300/shop_db/_search?pretty=1 \
     --header "Content-Type: application/json; charset=utf-8"
