#!/bin/bash

text=$1
body='{"query":{"simple_query_string":{"query":'\"${text}\"'}}}'
curl -XGET -u elastic:changeme localhost:9200/shop_db/_search?pretty=1 \
     -d $body \
     --header "Content-Type: application/json; charset=utf-8"
