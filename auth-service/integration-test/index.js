'use strict';

const should = require('should');
const isPortReachable = require('is-port-reachable');
const request = require('supertest');

/*
 * The full test. Start the server, and then make
 * the most important requests to it
 * 
 * The 5 tests are:
 * Make a new user
 * Make a new user, but fails because user exists
 * Logs in an user
 * Logs in an user, but fails because of invalid credentials
 * Refresh an user with a token
 */
describe('Auth Integration Test', function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // need a long timeout for exponential backoff to work correctly
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * It probes the port for the service; if it receives a response,
   * then start the server; else retry until you run out, then fail.
   */
  before(() => {
    const tryConnect = (url) => {
      // function to backoff from retrying
      const pause = (duration) => new Promise(res => setTimeout(res, duration));
      const backoff = (retries, fn, delay = 1000) => {
        return fn().then(reachable => {
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries -1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(7, () => isPortReachable(port, {host: host}));
    };
    return tryConnect(url);
  });

  it("creating a user returns a 200 success", (done) => {
    let user = {username: "Willem", password: "Peanuts", email:"rand1@example.com"};
    api.post('/auth/user')
      .send({user})
      .expect(200, done);
  });

  it("creating a user returns that has a password less than 6 characters should return 400", (done) => {
    let user = {username: "jamescho", password: "12345", email:"rand1@example.com"};
    api.post('/auth/user')
      .send({user})
      .expect(400, done);
  });

  it("creating a user meant to be oauth returns a 200 success", (done) => {
    let user = {username: "jcho123", email:"jcho@ex.com", oauth:true};
    api.post('/auth/user')
	  .send({user})
	  .expect(200, done);
  });

  it("obtain access_token and refresh_token pair when logging in with OAuth", (done) => {
    api.post('/auth/token?oauth=true')
          .send({username: "jcho123", oauth:true})
          .expect(200, (err, res) => {
             res.body.should.have.property('access_token');
             res.body.should.have.property('refresh_token');
             done();
          });
  });

  it('try to make user that already exists should fail', (done) => {
    let user = {
	    username: 'RandomUser', password: 'Password', email:'rand2@example.com'
    };
    let addUserAgain = (err, res) => {
      api.post('/auth/user')
        .send({user})
        .expect(409, done);
    };
    api.post('/auth/user')
      .send({user})
      .expect(200, addUserAgain);
  });

  it("creating a user with a email that exists in the database should fail", (done) => {
    let user = {username: "Willem", password: "Peanuts", email:"rand1@example.com"};
    api.post('/auth/user')
      .send({user})
      .expect(409, done);
  });

  it('obtain access_token and refresh_token pair when logging in with username and password', (done) => {
    // Assuming user is already created from previous tests.
    let basicAuthValue = Buffer.from('Willem:Peanuts').toString('base64');
    api.post('/auth/token')
      .set('Authorization', 'Basic ' + basicAuthValue)
      .expect(200, (err, res) => {
        res.body.should.have.property('access_token');
        res.body.should.have.property('refresh_token');
        done();
      });
  });

  it('refresh tokens using refresh token', (done) => {
    // Assuming user is already created from previous tests.    
    let testRefresh = (access_token, refresh_token) => {
      api.post('/auth/token')
        .set('Authorization', 'Bearer ' + access_token)
        .send({refresh_token})
        .expect(200, (err, res) => {
          res.body.should.have.property('access_token');
          res.body.should.not.have.property('refresh_token');
          done();
        });
    };
    let basicAuthValue = Buffer.from('Willem:Peanuts').toString('base64');
    api.post('/auth/token')
      .set('Authorization', 'Basic ' + basicAuthValue)
      .expect(200, (err, res) => {
        testRefresh(res.body.access_token, res.body.refresh_token);
      });
  });

  it('delete a user should allow same user to be created', (done) => {
    // Assuming user is already created from previous tests.
    let user = {
      username: 'Williem',
      password: 'Peanuts'
    };
    let testUserIsGone = () => {
      api.post('/auth/user')
        .send({user})
        .expect(200, done);
    };
    api.delete('/auth/user')
      .send({user})
      .expect(200, testUserIsGone);
  });

  it('login two times on same user using basic authentication then refresh tokens with the two refresh tokens obtained', (done) => {
    let user = {
      username: 'MultiUser', password: 'Password' , email:"rand3@example.com"
    };
    let refresh_token_once = null, access_token_once = null;
    let refresh_token_twice = null, access_token_twice = null;
    let basicAuthValue = Buffer.from(`${user.username}:${user.password}`).toString('base64');
    let refreshTwice = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      api.post('/auth/token')
        .set('Authorization', 'Bearer ' + access_token_twice)
        .send({refresh_token: refresh_token_twice})
        .expect(200, done);
    };
    let refreshOnce = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      refresh_token_twice = res.body.refresh_token;
      access_token_twice = res.body.access_token;
      api.post('/auth/token')
        .set('Authorization', 'Bearer ' + access_token_once)
        .send({refresh_token: refresh_token_once})
        .expect(200, refreshTwice);
    };
    let loginTwice = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      refresh_token_once = res.body.refresh_token;
      access_token_once = res.body.access_token;
      api.post('/auth/token')
        .set('Authorization', 'Basic ' + basicAuthValue)
        .expect(200, refreshOnce);
    };
    let loginOnce = (err, res) => {
      if (err) {
        done(err);
        return;
      }
      api.post('/auth/token')
        .set('Authorization', 'Basic ' + basicAuthValue)
        .expect(200, loginTwice);
    };
    api.post('/auth/user')
      .send({user})
      .expect(200, loginOnce);
  });
});
