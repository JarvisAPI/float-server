'use strict';
/*
 * Auth Error: signifies that the user is
 * trying to authenticate with invalid 
 * credentials
 */
module.exports = function AuthError(message) {
    this.name = this.constructor.name;
    this.message = message;
};

require('util').inherits(module.exports, Error);
