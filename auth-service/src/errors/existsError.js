'use strict';
/* 
 * Exists error: Signifies the user has we are attempting to create
 * has beeen already been created.
 */

module.exports = function ExistsError(field,message) {
    this.field = field	
    this.name = this.constructor.name;
    this.message = message;
};

require('util').inherits(module.exports, Error);
