'use strict';

const bcrypt = require('../libfunctions/bcrypt.js');
const jwt = require('../libfunctions/jwt.js');
const crypto = require('crypto');
const AuthError = require('../errors/autherror.js');
const ExistsError = require('../errors/existsError.js');

module.exports = (container) => {
  let repo = container.cradle.repo;
  let logger = container.cradle.logger;
  
  /*
   * Stores a username and password into the database
   *
   * Returns: An empty resolved promise on success or 
   *          a promise containing the error if it failed
   */
  const createUser = (user) => {
    return bcrypt.makeHashedPassword(user.password)
      .then((hash) => {
	user.hash = hash
        return repo.addUser(user);
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };

  /*
   * Generate access_token and refresh_token based on basic authentication.
   */
  const genTokensBasic = (username, password) => {
    return new Promise((resolve, reject) => {
      let makeAccessToken = (refresh_token) => {
        jwt.makeNewToken(username, container)
          .then((access_token) => {
            resolve({access_token, refresh_token});
          })
          .catch(reject);
      };
      let makeRefreshToken = () => {
        crypto.randomBytes(16, (err, buf) => {
          if (err) {
            reject(err);
            return;
          }
          let refresh_token = buf.toString('hex');
          repo.setUserRefreshToken(username, refresh_token)
            .then(() => {
              makeAccessToken(refresh_token);
            })
            .catch(reject);
        });        
      };
      /*
       * Check to see if refresh token already exists for user. If it exists
       * then use that, otherwise generate it.
       */
      let checkRefreshToken = () => {
        repo.getUserRefreshToken(username)
          .then((refresh_token) => {
            if (!refresh_token) {
              makeRefreshToken();
            } else {
              makeAccessToken(refresh_token);
            }
          })
          .catch(reject);
      };
      let checkPassword = (password, hash) => {
        bcrypt.compareHashedPassword(password, hash)
          .then((hashMatch) => {
            if(hashMatch) {
              checkRefreshToken();
            } else {
              reject(new AuthError('Invalid password'));
            }
          })
          .catch(reject);
      };
      repo.getUserPasswordHash(username)
        .then((hash) => {
          if (hash) {
	    checkPassword(password, hash);
          } else{
	    reject(new AuthError('genTokenBasic: User doesn\'t exists'));
	  }
        })
        .catch(reject);
    });
  };


  /*
   * Generate access_token and refresh_token based on basic authentication.
   */
  const genTokensOAuth = (username) => {
    return new Promise((resolve, reject) => {
      let makeAccessToken = (refresh_token) => {
        jwt.makeNewToken(username, container)
          .then((access_token) => {
            resolve({access_token, refresh_token});
          })
          .catch(reject);
      };
      let makeRefreshToken = () => {
        crypto.randomBytes(16, (err, buf) => {
          if (err) {
            reject(err);
            return;
          }
          let refresh_token = buf.toString('hex');
          repo.setUserRefreshToken(username, refresh_token)
            .then(() => {
              makeAccessToken(refresh_token);
            })
            .catch(reject);
        });        
      };
      /*
       * Check to see if refresh token already exists for user. If it exists
       * then use that, otherwise generate it.
       */
      let checkRefreshToken = () => {
        repo.getUserRefreshToken(username)
          .then((refresh_token) => {
            if (!refresh_token) {
              makeRefreshToken();
            } else {
              makeAccessToken(refresh_token);
            }
          }).catch(reject);
      };
      repo.checkIfOAuthUserExists(username)
        .then((exists) => {
          if (exists) {
	    checkRefreshToken()
          } else{
	    reject(new AuthError('genTokenBasic: User doesn\'t exists'));
	  }
        })
        .catch(reject);
    });
  };




  /*
   * Generate access token based on refresh token.
   */
  const refreshToken = (access_token, refresh_token) => {
    return new Promise((resolve, reject) => {
      let issueNewToken = (username) => {
        logger.debug('refreshToken: username: ' + username);
        logger.debug('refreshToken: refresh_token: ' + refresh_token);
        repo.checkRefreshToken(username, refresh_token)
          .then((match) => {
            if (match) {
              jwt.makeNewToken(username, container)
                .then(resolve)
                .catch(reject);
            } else {
              logger.debug('Refresh token: ' + refresh_token);
              reject(new Error('Refresh token doesn\'t match'));
            }
          })
          .catch(reject);
      };
      jwt.checkTokenIgnoreExp(access_token, container)
        .then((payload) => {
          logger.debug('Refreshing token payload: %j', payload);
          issueNewToken(payload.sub);
        })
        .catch((err) => {
          logger.error(err);
          reject(new AuthError('Invalid token'));
        });
    });
  };

  // exports the functions here
  return {
    createUser,
    genTokensBasic,
    refreshToken,
    genTokensOAuth
  };
};
