
const authFunctionSetter = require('./authfunc.js');
const AuthError = require('../errors/autherror.js');
const ExistsError = require('../errors/existsError.js');

module.exports = (app, container) => {
  const authFunctions = authFunctionSetter(container);
  const logger = container.cradle.logger;
  const repo = container.cradle.repo;
  const validate = container.cradle.validate;
  /*
   * HTTP Gateway for creation of users
   *
   * Request: A JSON object with fields: username, name, passwords and email (both strings)
   *
   * Responses:
   * 201: New user created, body contains a success message in JSON
   *  Headers: Location - the location of where the user should log in
   * 409: User already exists, body contains a failure message in JSON
   * 500: Other error(Internal server error)
   * 
   */
  app.post('/auth/user', (req, res, next) => {
    logger.info("Auth Service: Request to create user received.");
    let user = req.body.user;
    var {error, value} = validate(user, 'user');
    if(!user.oauth && !user.password){
      res.status(400).send();
      return;
    } else if (user.password){
      user.oauth = false;
    }
    if(!error) {
      user.username = user.username.toLowerCase().trim();
      if(user.email){
        user.email = user.email.toLowerCase().trim();
      }
      authFunctions.createUser(user)
        .then(() => {
          logger.info("Auth Service: Request to create user has succeeded");
          let response = {
            message: 'The user has been created'
          };
          res.status(200).json(response);
        })
        .catch((err) => {
          logger.error('%j', err);
          if(err instanceof ExistsError) {
            res.status(409).json(err);
          } else {
            res.status(500).send();
          }
        });
    } else {
      logger.info("Auth Service: Request to create user failed.");
      logger.error(error);      
      let jsonErrMsg = {};
      for (detail of error.details) {
        if (detail.type === "string.email") {
          jsonErrMsg.message = "Email is invalid";
          jsonErrMsg.field = "email";
          res.status(409).send(jsonErrMsg);
          return;
	    }
      }
      res.status(400).send();
    }
  });

  /**
   *
   * authValue: base64 encoded string consisting of username:password
   */
  const basicAuth = (res, authValue) => {
    logger.info('Basic authentication for token');
    let credentialPair = new Buffer(authValue, 'base64').toString('ascii');
    let credentialArr = credentialPair.split(':');
    let username = credentialArr[0], password = credentialArr[1];
    username = username.toLowerCase().trim();
    let user = {username, password};
    let {error, value} = validate(user, 'user');    
    if(!error) {
      authFunctions.genTokensBasic(username, password)
        .then((tokens) => {
          res.status(200).json(tokens);
        })
        .catch((err) => {
          logger.error(err);
          if(err instanceof AuthError) {
            let response = {
              message: 'Wrong username or password'
            };
            res.status(401).json(response);
          } else {
            res.status(500).send();
          }
        });
    } else {
      logger.info("Auth Service: Request to login user has failed.");
      logger.error(error);
      res.status(400).send();
    }    
  };

  const refreshToken = (res, access_token, refresh_token) => {
    logger.info('Refreshing token');
    logger.debug('refresh_token: ' + refresh_token);
    authFunctions.refreshToken(access_token, refresh_token)
      .then((newAccessToken) => {
        res.status(200).json({access_token: newAccessToken});
      })
      .catch((err) => {
        logger.error('Refreshing token failed');
        if (err instanceof AuthError) {
          logger.error(err.message);
          res.status(401).send();
        } else {
          logger.error(err);
          res.status(500).send();
        }
      });
  };
  
  const OAuthAutenticate = (res, username) => {
    logger.info("OAuth authentication for user:" + username);
    authFunctions.genTokensOAuth(username)
      .then((tokens) => {
          res.status(200).json(tokens);
        })
        .catch((err) => {
          logger.error(err);
          if(err instanceof AuthError) {
            let response = {
              message: 'Invalid username'
            };
            res.status(401).json(response);
          } else {
            res.status(500).send();
          }
        });
  }

  /*
   * HTTP Gateway for getting a new token by logging in
   *
   * Headers:
   *   Authorization: Basic for username password authentication to get
   *     access_token/refresh_token pair.
   *   Authorization: Bearer for refreshing access_token.
   *
   * Responses:
   * 200: Token generated for user, body contains token in JSON
   * 400: Not supported authentication method
   * 401: Unauthorized
   * 500: Other error(Internal server error)
   * 
   */
  app.post('/auth/token', (req, res, next) => {
    logger.debug("auth token request:"+ req.body + " is OAuth:" + req.query.oauth)
    let isOAuth = req.query.oauth;
    if(isOAuth && req.body && req.body.username){
      OAuthAutenticate(res, req.body.username);
    } else if(req.get('Authorization')){
      let authHeaderArr = req.get('Authorization').split(' ');
      let authType = authHeaderArr[0];
      let authValue = authHeaderArr[1];
      if(authType === 'Basic') {
        basicAuth(res, authValue);
      } else if (authType === 'Bearer') {
        refreshToken(res, authValue, req.body.refresh_token);
      } else {
        res.status(400).send();
      }
    } else{
      res.status(400).send();
    }
  });

  app.delete('/auth/user', (req, res, next) => {
    let user = req.body.user;
    repo.removeUser(user.username.toLowerCase().trim())
      .then(() => {
        res.status(200).send();
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });
};
