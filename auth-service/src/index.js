'use strict';

const {EventEmitter} = require('events');
const server = require('./server/server.js');
const repository = require('./repository/repository.js');
const di = require('./config');
const mediator = new EventEmitter();
const {asValue} = require('awilix');

const bootServer = (container) => {
  const logger = container.cradle.logger;
  logger.info('db connected!');
  repository.connect(container)
    .then((repo) => {
      logger.info('Connected... Starting Server');
      container.register('repo', asValue(repo));
      return server.start(container);
    })
    .then((app) => {
      logger.info(`Server started successfully, running on port: ${container.cradle.serverSettings.port}.`);
      app.on('close', () => {
        container.resolve('repo').disconnect();
      });
    });
};

mediator.on('di.ready', (container) => {
  bootServer(container);
});

mediator.on('di.error', (err) => {
  throw err;
});

di.init(mediator);

mediator.emit('init');
