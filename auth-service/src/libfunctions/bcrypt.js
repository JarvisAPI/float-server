'use strict';
const bcrypt = require('bcrypt-nodejs');

/*
 * Generates and returns a hash using a salt
 */
exports.makeHashedPassword = (password) => {
  return new Promise((resolve, reject) => {
    let genHash = (salt) => {
      bcrypt.hash(password, salt, null, (err, hash) => {
        if (err) {
          reject(err);
        } else {
          resolve(hash);
        }
      });
    };
    let rounds = 10;
    bcrypt.genSalt(rounds, (err, salt) => {
      if (err) {
        reject(err);
      } else {
        genHash(salt);
      }
    });
  });
};

/*
 * Checks to see if a hash is the same as the unencrypted data
 * Returns true if they match, false if they don't
 */
exports.compareHashedPassword = (password, hashedPassword) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, hashedPassword, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
};
