'use strict';
const jwt = require('jsonwebtoken');

/*
 * Makes and returns a new token representing the user
 */
const makeNewToken = (username, container) => {
  let options = {
    algorithm: 'RS256',
    expiresIn: '30m',
    subject: username,
    issuer: 'MapApp'
  };
  let payload = {
  };
  let cert = container.cradle.authSettings.privateKey;
  return new Promise((resolve, reject) => {
    jwt.sign(payload, cert, options, (err, token) => {
      if(err) {
	reject(err);
      } else {
        resolve(token);
      }
    });
  });  
};

/**
 * Checks if the token is valid or not
 *
 * Returns an error if not valid, otherwise returns the decoded payload
 */
const checkToken = (token, container) => {
  let options = {
    algorithms: ['RS256'],
    issuer: 'MapApp'
  };
  let cert = container.cradle.authSettings.publicKey;
  return checkTokenHelper(token, cert, options);
};

/**
 * Check token validity but ignores token expiration.
 */
const checkTokenIgnoreExp = (token, container) => {
  let options = {
    algorithms: ['RS256'],
    issuer: 'MapApp',
    ignoreExpiration: true
  };
  let cert = container.cradle.authSettings.publicKey;
  return checkTokenHelper(token, cert, options);
};

const checkTokenHelper = (token, cert, options) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, cert, options, (err, decoded) => {
      if(err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });  
};

module.exports = Object.assign({}, {makeNewToken, checkToken, checkTokenIgnoreExp});
