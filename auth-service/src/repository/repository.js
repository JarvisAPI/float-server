'use strict';

const ExistsError = require('../errors/existsError.js');

const createUserIndexes = (db) => {
  db.createIndex('user', {username: 1}, {unique: true}, (err, name) => {/*Ignore*/});
  db.createIndex('user', {email: 1}, {unique: true}, (err, name) => {/*Ignore*/});
};

const repository = (container) => {
  // collections: user, token
  const db_client = container.resolve('db_client');
  const db = db_client.db(container.cradle.dbSettings.db);
  const logger = container.cradle.logger;
  createUserIndexes(db);
  
  /*
   * Stores the username and hashed password into the database
   */
  const addUser = (user) => {
    return new Promise((resolve, reject) => {
      logger.debug("creating user for:"+ user.username + " with email:"+ user.email)
      let username = user.username
      let name = user.name
      let password = user.hash
      let email = user.email
      let oauth = user.oauth
      let addUserToDB = () => {
        db.collection('user').insertOne({username, name, password, email, oauth}, (err, result) => {
          if (err) {
            if(err.code == 11000) {
              reject(new ExistsError("username",'User already exists'));
            } else {
              reject(err);
            }
          } else {
            resolve();
          }
        });
      }

      let checkEmail = () => {
        db.collection('user').findOne({email}, (err, result) => {
          if(err){
            reject(err)
            return
          } else {
            if(result && result.username){
              reject(new ExistsError("email",'Email already taken'));
            } else {
              addUserToDB();
            }
          }
        })
      }

      if(email){
        checkEmail()
      } else{
	addUserToDB()
      }
    });
  };

  const checkIfOAuthUserExists = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username}, (err, result) => {
        if(err){
	  reject(err)
	} else {
	  logger.debug("checkIfOAuthUserExists user:"+ username + " exists:" + result)
	  if(result && result.oauth){
            resolve(true)
	  } else{
	    resolve(false)
	  }
	}
      })
    })
  }

  /*
   * Gets a user's password hash, or nothing if user does not exist.
   */
  const getUserPasswordHash = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username}, (err, result) => {
        if (err) {
          reject(err);
        } else {
          logger.debug('getUserPasswordHash: result: ' + result);
          if (result && !result.oauth) {
            resolve(result.password);
          } else {
            resolve(null);
          }
        }
      });
    });
  };

  /*
   * Removes the user entry from the database using the username
   */
  const removeUser = (username) => {
    return db.collection('user').deleteOne({username});
  };

  /**
   * Sets the refresh_token of a user.
   */
  const setUserRefreshToken = (username, refresh_token) => {
    return new Promise((resolve, reject) => {
      let update = {
        $set: {refresh_token}
      };      
      db.collection('user').updateOne({username}, update, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  /**
   * Get the refresh token of the user from the database.
   * If there is no error in the query then promise reolves to
   * either null or the refresh_token. Otherwise reject with query error.
   */
  const getUserRefreshToken = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username}, (err, result) => {
        if (err) {
          reject(err);
        } else {
          if (result) {
            resolve(result.refresh_token);
          } else {
            resolve(null);
          }
        }
      });
    });
  };
  
  /**
   * Checks to see if the username and refresh_token pair is valid in the
   * database.
   * Resolves to true if pair matches, false otherwise.
   */
  const checkRefreshToken = (username, refresh_token) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username}, (err, result) => {
        if (err) {
          reject(err);
        } else {
          if (result !== null) {
            resolve(refresh_token === result.refresh_token);
          } else {
            resolve(false);
          }
        }
      });
    });
  };

  return {
    addUser,
    getUserPasswordHash,
    removeUser,
    setUserRefreshToken,
    checkRefreshToken,
    getUserRefreshToken,
    checkIfOAuthUserExists
  };
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve('db_client')) {
      reject(new Error('Connection db client not supplied!'));
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});
