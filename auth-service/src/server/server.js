'use strict';

const express = require('express');
const helmet = require('helmet');
const bodyparser = require('body-parser');
const api = require('../api/auth');
const spdy = require('spdy');

const start = (container) => {
  return new Promise((resolve, reject) => {
    const {port, ssl} = container.resolve('serverSettings');
    if(!port) {
      reject(new Error("Server must be started with an available port"));
      return;
    };

    // create a new express app and establish dependencies
    const app = express();
    if (!process.env.PROD) {
      // Only add this if in development.
      let morgan = require('morgan');
      app.use(morgan('dev'));
    }
    app.use(bodyparser.json());
    app.use(helmet());
    app.use((err, req, res, next) => {
      reject(new Error("Something went wrong!, err: " + err));
      res.status(500).send('Someting went wrong...');
      next();
    });

    // create all authentication functions
    api(app, container);
    // starts the server
    if (process.env.TEST === '1') {
      const server = app.listen(port, () => resolve(server));
    } else {
      const server = spdy.createServer(ssl, app)
	      .listen(port, () => resolve(server));
    }
  });
};

module.exports = Object.assign({}, {start});
