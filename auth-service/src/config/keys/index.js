'use strict';

const fs = require('fs');

module.exports = {
  privateKey: fs.readFileSync(`${__dirname}/authkeys-private.key`),
  publicKey: fs.readFileSync(`${__dirname}/authkeys-public.pem`)
};
