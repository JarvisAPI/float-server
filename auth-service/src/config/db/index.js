'use strict';

const {connect} = require('./mongo');

module.exports = Object.assign({}, {connect});
