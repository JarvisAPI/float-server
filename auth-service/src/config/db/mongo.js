'use strict';
const {MongoClient, Server} = require('mongodb');

const MAX_TIMEOUT = 20000;
let timeOut = 100;

const makeClient = (options, callback, errorCallback) => {
  new MongoClient(new Server(options.host, options.port), {
    user: options.user,
    password: options.pwd,
    authSource: options.db})
    .connect((err, client) => {
      if (err) {
        errorCallback(err);
        return;
      }
      callback(client);
    });
};

const expBackoff = (options, mediator) => {
  makeClient(options, (client) => {
    mediator.emit('db_client.ready', client);
  }, (err) => {
    if (timeOut < MAX_TIMEOUT) {
      setTimeout(expBackoff, timeOut, options, mediator);
      timeOut *= 2;
    } else {
      mediator.emit('db_client.error', err);
    }
  });
};

const connect = (options, mediator) => {
  mediator.once('boot.ready', () => {
    expBackoff(options, mediator);
  });
};

module.exports = Object.assign({}, {connect});
