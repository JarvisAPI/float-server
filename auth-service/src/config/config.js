'use strict';

const dbSettings = {
  db: process.env.DB || 'auth_db',
  user: process.env.DB_USER || 'auth_service',
  pwd: process.env.DB_PASS || 'TCPmyClientMapAppSocketv1.0',  
  host: process.env.DATABASE_HOST || '127.0.0.1',
  port: '27017'
};

const serverSettings = {
  port: process.env.PORT || 3000, // port this microservice starts on
  ssl: require('./ssl')
};

const authSettings = require('./keys');

module.exports = {dbSettings, serverSettings, authSettings};
