const {dbSettings, serverSettings, authSettings} = require('./config');
const database = require('./db');
const {initDI} = require('./di');
const models = require('../models');

const init = initDI.bind(null, {serverSettings, dbSettings, authSettings, database, models});

module.exports = Object.assign({}, {init});
