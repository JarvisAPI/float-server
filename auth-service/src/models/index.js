const joi = require('joi');
const user = require('./user.model')(joi);

const schemas = {user};

/**
 * Return true if object follows schema, false otherwise.
 */
const schemaValidator = (object, type) => {
  if (!object) {
    return {error: new Error('Invalid object passed in!'), value: null};
  }
  return joi.validate(object, schemas[type]);
};

module.exports = {validate: schemaValidator};
