'use strict';

const userSchema = (joi) => ({
  username: joi.string().min(6, "utf8").max(30, "utf8").required(),
  password: joi.string().max(30).min(6),
  email: joi.string().max(100, "utf8").email({ minDomainAtoms: 2 }),
  oauth: joi.boolean()
});

module.exports = userSchema;
