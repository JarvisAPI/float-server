// Make sure to set environmental variable NODE='test' when calling npm start

'use strict';

const request = require('supertest');
const should = require('should');
const isPortReachable = require('is-port-reachable');

describe('user-service', function() {
  const host = process.env.HOST;
  const port = process.env.PORT;
  const url = `http://${host}:${port}`;
  const api = request(url);

  // Need a long timeout for exponential backoff to work.
  this.timeout(200000);

  /*
   * Does exponential backoff to wait for service to get ready
   * it probes the port for the service; if it receives a response,
   * then start the tests; else retry until time runs out.
   */
  before(() => {
    function tryConnect(url) {
      const pause = (duration) => new Promise((res) => setTimeout(res, duration));
      const backoff = (retries, fn, delay=1000) => {
        return fn().then((reachable) => {
          return reachable ? Promise.resolve(reachable)
            : (retries > 1
               ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
               : Promise.reject());
        });
      };
      return backoff(5, () => isPortReachable(port, {host: host}));
    };
    return tryConnect(url);
  });

  it('create a user, add a shop to user then get the shop for the user back', (done) => {
    let user = {
      username: 'test-user',
      name: 'james'
    };
    let shop = {
      shop_name: 'test-shop',
      shop_id: 'test-shop-id'
    };
    let verifyShops = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.should.have.property('shops').containEql(shop);
      done();
    };
    let getUserShops = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/user/${user.username}/shops`)
        .expect(200, verifyShops);
    };
    let addShopToUser = (err, res) => {
      if (err) {
        throw err;
      }
      api.post('/user/shops')
        .send({user, shop})
        .expect(200, getUserShops);
    };
    api.post('/user')
      .send({user})
      .expect(200, addShopToUser);
  });

  it('get user base on shop id', (done) => {
    api.get('/user/shop/test-shop-id')
      .expect(200, function(err , res){
        if (err) {
          throw err;
        }
        console.log(res.body);
        if(res.body.usernames[0] === "test-user"){
          done();
        }
        else{
          done(Error("something went wrong"));
        }
      })
  });

  it('trying to create a user from nothing should return bad request', (done) => {
    api.post('/user')
      .expect(400, done);
  });

  it('trying to add a null shop to a user should return bad request', (done) => {
    let user = {
      username: 'testUser2',
      name: 'james'
    };
    let addBadShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.post('/user/shops')
        .send({user})
        .expect(400, done);
    };
    api.post('/user')
      .send({user})
      .expect(200, addBadShop);
  });

  it('returns  a 400 bad request if trying to add a shop to a null user', (done) => {
    let shop = {
      shop_name: 'test-shop',
      shop_id: 'test-shop2-id'
    };
    let user = {};
    api.post('/user/shops')
      .send({user, shop})
      .expect(400, done);
  });

  it('add shop to a user, remove that shop from user and then check if user stil has shop', (done) => {
    // Assuming user created in previous tests.
    let user = {
      username: 'testUser2',
      name: 'james'
    };
    let shop = {
      shop_name: 'test-remove-shop',
      shop_id: 'test-remove-shop-id'
    };
    let verifyShopGone = (err, res) => {
      if (err) {
        throw err;
      }
      res.body.shops.should.not.containEql(shop);
      done();
    };
    let getShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.get(`/user/${user.username}/shops`)
        .expect(200, verifyShopGone);
    };
    let removeShop = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete('/user/shops')
        .send({user, shop})
        .expect(200, getShop);
    };
    api.post('/user/shops')
      .send({user, shop})
      .expect(200, removeShop);
  });

  it('add profile and background pictures and retrieve them', (done) => {
    var profileURL1;
    var profileURL2;
    var profileURL3;
    var backURL1;
    var backURL2;
    var backURL3;

    let getProfileMainPictures = () => {
      let usernames = JSON.stringify(["testUser2", "test-user"])
      api.get(`/user/${usernames}/profile_images`)
	 .expect(200, function(err , res){
		 console.log(res.body)
	   if (err) {
             throw err;
	   }
	   if(res.body.testUser2.image != profileURL3){
	     done(new Error("something went wrong"))
	   }
	   done()
	 }) 
    }

    let getImages = (err, res) => {
      if (err) {
        throw err;
      }
      backURL3 = res.body.image_url;
      api.get('/user/testUser2/profile')
        .expect(200, function(err , res){
          if (err) {
            throw err;
          }
	  console.log(res.body)
          var profileImages = res.body.profile_images;
          var backgroundImages = res.body.background_images;
          if(!(profileImages[0].url === profileURL3)){
            done(new Error("something went wrong"))
	  }
          else if(!(backgroundImages[0].url === backURL3)){
            done(new Error("something went wrong"))
          }
          else{
            getProfileMainPictures();
          }
        })
    }

    let addBackgroundImage3 = (err, res) => {
      if (err) {
        throw err;
      }
      backURL2 = res.body.image_url;
      api.post('/user/testUser2/profile/background')
      .field('name', 'test-image')
      .attach('image', 'test-image3.jpg')
      .expect(200, getImages);
    }

    let addBackgroundImage2 = (err, res) => {
      if (err) {
        throw err;
      }
      backURL1 = res.body.image_url;
      api.post('/user/testUser2/profile/background')
      .field('name', 'test-image')
      .attach('image', 'test-image2.jpg')
      .expect(200, addBackgroundImage3);
    }

    let addBackgroundImage1 = (err, res) => {
      if (err) {
        throw err;
      }
      profileURL3 = res.body.image_url;
      api.post('/user/testUser2/profile/background')
      .field('name', 'test-image')
      .attach('image', 'test-image1.jpg')
      .expect(200, addBackgroundImage2);
    }

    let addProfileImage3 = (err, res) => {
      if (err) {
        throw err;
      }
      profileURL2 = res.body.image_url;
      api.post('/user/testUser2/profile/profile_image')
      .field('name', 'test-image')
      .attach('image', 'test-image3.jpg')
      .expect(200, addBackgroundImage1);
    }

    let addProfileImage2 = (err, res) => {
      if (err) {
        throw err;
      }
      profileURL1 =  res.body.image_url;
      api.post('/user/testUser2/profile/profile_image')
      .field('name', 'test-image')
      .attach('image', 'test-image2.jpg')
      .expect(200, addProfileImage3);
    }

    api.post('/user/testUser2/profile/profile_image')
    .field('name', 'test-image')
    .attach('image', 'test-image1.jpg')
    .expect(200,addProfileImage2);
  });

  //Test case deprecated since no way to query all profile images and background images
  /*
  it('delete profile picture', (done) => {
    var profileURL1;
    var profileURL2;
    var profileURL3;
    var backURL1;
    var backURL2;
    var backURL3;

    let getImages = (err, res) => {
      if (err) {
        throw err;
      }
      api.get('/user/testUser2/profile')
        .expect(200, function(err , res){
          if (err) {
            throw err;
          }
          var profileImages = res.body.profile_images;
          var backgroundImages = res.body.background_images;
          if(profileImages.length !== 0 && backgroundImages.length !== 0){
            done(new Error("something went wrong"));
          }
          else{
            done();
          }
        })
    }

    let deleteBackgroundImage3 = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete('/user/testUser2/profile/background/'+ backURL3)
        .expect(200, getImages);
    }

    let deleteBackgroundImage2 = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete('/user/testUser2/profile/background/'+ backURL2)
        .expect(200, deleteBackgroundImage3);
    }

    let deleteBackgroundImage1 = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete('/user/testUser2/profile/background/'+ backURL1)
        .expect(200, deleteBackgroundImage2);
    }

    let deleteProfileImage3 = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete('/user/testUser2/profile/profile_image/' + profileURL3)
        .expect(200, deleteBackgroundImage1);
    }

    let deleteProfileImage2 = (err, res) => {
      if (err) {
        throw err;
      }
      api.delete('/user/testUser2/profile/profile_image/' + profileURL2)
        .expect(200, deleteProfileImage3);
    }

    let deleteProfileImage1 = (err, res) => {
      if (err) {
        throw err;
      }
      var imageURLs = res.body.profile_images;
      profileURL3 = imageURLs[0].url;
      profileURL2 = imageURLs[1].url;
      profileURL1 = imageURLs[2].url;
      imageURLs = res.body.background_images;
      backURL3 = imageURLs[0].url;
      backURL2 = imageURLs[1].url;
      backURL1 = imageURLs[2].url;
      api.delete('/user/testUser2/profile/profile_image/' + profileURL1).expect(200, deleteProfileImage2);
    }
    api.get('/user/testUser2/profile').expect(200, deleteProfileImage1)
  })
  */
});
