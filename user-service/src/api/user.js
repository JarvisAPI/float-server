'use strict';

const hal = require('hal');

module.exports = (app, container) => {
  const logger = container.cradle.logger;
  const repo = container.cradle.repo;
  const validate = container.cradle.validate;
  let request = container.cradle.request;
  let uploadSettings = container.cradle.uploadSettings;
  let imageUrlBase = `${uploadSettings.proto}://${uploadSettings.image_server_host}:${uploadSettings.port}`;

  app.post('/user', (req, res, next) => {
    let user = req.body.user;
    if (validate(user, 'user')) {
      repo.addUser(user.username, user.name)
        .then(() => {
	      res.status(200).send();
        }, (err) => {
	      logger.error(err);
	      res.status(400).send();
        });
    } else {
      res.status(400).send();
    }
  });

  app.delete('/user', (req, res, next) => {
    let user = req.body.user;
    repo.removeUser(user.username)
      .then(() => {
        res.status(200).send();
      })
      .catch((err) => {
        logger.error(err);
        res.status(500).send();
      });
  });

  app.get('/user/:username/shops', (req, res, next) => {
    repo.getShops(req.params.username)
      .then((shops) => {
	    res.status(200).json({shops});
      })
      .catch((err) => {
	    logger.error(err);
	    res.status(400).send();
      });
  });

  app.get('/user/shop/:shop_id', (req, res, next) => {
    repo.getUsersBasedOnShopId(req.params.shop_id)
      .then((usernames) => {
	    res.status(200).send({usernames});
      })
      .catch((err) => {
	    logger.error(err);
	    res.status(400).send();
      });
  });

  app.post('/user/shops', (req, res, next) => {
    logger.debug('adding shop request body: %j', req.body);
    let user = req.body.user;
    let shop = req.body.shop;
    if (req.body && user.username && validate(shop, 'shop')) {
      repo.addShop(user.username, shop)
        .then(() => {
	      res.status(200).send();
        })
        .catch(next);
    } else {
      res.status(400).send();
    }
  });

  app.delete('/user/shops', (req, res, next) => {
    logger.debug('removing shop from user');
    let username = req.body.user.username;
    let shop = req.body.shop;
    repo.removeShopFromUser(username, shop)
      .then(() => {
        res.status(200).send();
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  app.get('/user/:username/profile', (req, res, next) => {
    logger.debug('getting profile');
    let username = req.params.username;
    repo.getUser(username).then((result) => {
      res.status(200).send(result);
    })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  app.post('/user/:username/profile/profile_image', (req, res, next) => {
    logger.debug('adding profile image to image service');
    let image_url = `${imageUrlBase}/image`;
    req.pipe(request({url: image_url}, (err, response, body) => {
      if (err) {
        logger.debug('error when requesting to image service');
        logger.error(err);
        res.status(500).send();
      } else {
        try {
          body = JSON.parse(body);
        } catch(err) {
          next(err);
          return;
        }
        logger.debug('adding profile image url to database');
        let username = req.params.username;
        let imageUrl = body.filename;
        repo.addProfileImage(username, imageUrl)
          .then(() => {
            res.status(200).send({image_url: imageUrl});
          })
          .catch(next);
      }
    }));
  });

  app.get('/user/:username/profile/profile_image', (req, res, next) => {
    logger.debug('getting profile image(s)');
    let username = req.params.username;
    repo.getProfileImages(username)
      .then((profileImages) => {
        res.status(200).send({profile_images: profileImages});
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  app.get('/user/:usernames/profile_images', (req, res, next) => {
    logger.debug('getting profile images for multiple users');
    let usernames = JSON.parse(req.params.usernames);
    if(!usernames && usernames.length == 0){
	  res.status(400).send();
	  return;
    }
    repo.getUsersProfileImage(usernames)
      .then((profileImages) => {
        res.status(200).send(profileImages);
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  app.post('/user/:username/profile/background', (req, res, next) => {
    let image_url = `${imageUrlBase}/image`;
    req.pipe(request({url: image_url}, (err, response, body) => {
      if (err) {
        logger.error(err);
        res.status(500).send();
      } else {
        try {
          body = JSON.parse(body);
        } catch(err) {
          next(err);
          return;
        }
        logger.debug('adding profile image');
        let username = req.params.username;
        let imageUrl = body.filename;
        repo.addBackgroundImage(username, imageUrl)
          .then(() => {
            res.status(200).send({image_url: imageUrl});
          })
          .catch(next);
      }
    }));
  });

  // Using for testing.
  app.get('/user/:username/profile/background', (req, res, next) => {
    logger.debug('getting profile background image(s)');
    let username = req.params.username;
    repo.getBackgroundImages(username)
      .then((backgroundImages) => {
        res.status(200).send({background_images: backgroundImages});
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  app.delete('/user/:username/profile/profile_image/:url', (req, res, next) => {
    logger.debug('deleting profile background image');
    let username = req.params.username;
    let imageUrl = req.params.url;
    repo.removeProfileImageFromUser(username, imageUrl)
      .then(() => {
        res.status(200).send();
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });

  app.delete('/user/:username/profile/background/:url', (req, res, next) => {
    logger.debug('deleting profile background image');
    let username = req.params.username;
    let imageUrl = req.params.url;
    repo.removeBackgroundImageFromUser(username, imageUrl)
      .then(() => {
        res.status(200).send();
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send();
      });
  });
};
