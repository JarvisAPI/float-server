'use strict';

const shopSchema = (joi) => ({
  shop_name: joi.string().required(),
  shop_id: joi.string().required()
});

module.exports = shopSchema;
