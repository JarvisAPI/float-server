const joi = require('joi');
const user = require('./user.model')(joi);
const shop = require('./shop.model')(joi);

const schemas = Object.create({user, shop});

const schemaValidator = (object, type) => {
  if (!object) {
    return false;
  }
  const {error, value} = joi.validate(object, schemas[type]);
  return !error;
};

module.exports = {validate: schemaValidator};
