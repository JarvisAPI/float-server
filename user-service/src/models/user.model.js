'use strict';

const userSchema = (joi) => ({
  username: joi.string().min(6, "utf8").max(30, "utf8").required(),
  name: joi.string().min(1, "utf8").max(30, "utf8").required()
});

module.exports = userSchema;
