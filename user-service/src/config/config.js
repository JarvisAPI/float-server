'use strict';
const fs = require('fs');

const dbSettings = {
  db: process.env.DB || 'user_db',
  user: process.env.DB_USER || 'user_service',
  pwd: process.env.DB_PASS || 'TCPmyClientMapAppSocketv1.0',
  host: process.env.DATABASE_HOST || '127.0.0.1',
  port: 27017
};

const serverSettings = {
  port: process.env.PORT || 3000,
  ssl: require('./ssl')
};

const requestOptions = {
  CA: process.env.TEST === '1' ? null : fs.readFileSync(__dirname + '/ssl/ca.pem')
};

const uploadSettings = {
  proto: process.env.TEST === '1' ? 'http' : 'https',
  port: process.env.IMAGE_SERVER_PORT || 3000,
  image_server_host: process.env.IMAGE_SERVER_HOST
};

module.exports = Object.assign({}, { dbSettings, serverSettings, uploadSettings, requestOptions });
