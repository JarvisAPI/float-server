'use strict';

const {createContainer, asValue} = require('awilix');
var request = require('request');
const winston = require('winston');
const winstonError = require('winston-error');
const config = winston.config;
const logger = new (winston.Logger) ({
  transports: [
    new (winston.transports.Console) ({
      formatter: function(options) {
	    return `[${new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')}]`+
          '[' +
	      config.colorize(options.level, options.level.toUpperCase()) +
	      ']: ' +
	      (options.message && !options.meta.error ? options.message : '') +
          (options.meta.error ? options.meta.error.stack : '');
      }
    })
  ]
});

winstonError(logger, {
  decoratedLevels: ['error']
});

function initDI({serverSettings, dbSettings, uploadSettings, database, models, requestOptions}, mediator) {
  mediator.once('init', () => {
    if (process.env.DEBUG === '1') {
      logger.level = 'debug';
    }
    if (process.env.TEST !== '1') {
      request = request.defaults({
        ca: requestOptions.CA
      });
    }
    
    mediator.on('db_client.ready', (client) => {
      const container = createContainer();
      
      container.register({
	db_client: asValue(client),
	serverSettings: asValue(serverSettings),
	logger: asValue(logger),
        validate: asValue(models.validate),
	dbSettings: asValue(dbSettings),
        uploadSettings: asValue(uploadSettings),
        request: asValue(request)
      });

      mediator.emit('di.ready', container);
    });

    mediator.on('db_client.error', (err) => {
      mediator.emit('di.error', err);
    });

    database.connect(dbSettings, mediator);
    // Signals the database to startup.
    mediator.emit('boot.ready');
  });	
};

module.exports.initDI = initDI;
