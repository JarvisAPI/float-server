'use strict';

const express = require('express');
const helmet = require('helmet');
const bodyparser = require('body-parser');
const api = require('../api/user');
const spdy = require('spdy');

const start = (container) => {
  const logger = container.cradle.logger;
  return new Promise((resolve, reject) => {
    const {port, ssl} = container.cradle.serverSettings;
    const repo = container.cradle.repo;

    if (!repo) {
      reject(new Error('The server must be started with a connected repository'));
      return;
    }
    if (!port) {
      reject(new Error('The server must be started with an available port'));
      return;
    }

    const app = express();
    if (!process.env.PROD) {
      let morgan = require('morgan');      
      app.use(morgan('dev'));
    }
    app.use(bodyparser.json());
    app.use(helmet());

    api(app, container);

    app.use((err, req, res, next) => {
      logger.error(err);
      if (err.status) {
        res.status(err.status).json(err);
        return;
      }
      reject(new Error('Something went wrong!, err:' + err));
      res.status(500).send('Something went wrong!');
    });    

    if (process.env.TEST === '1') {
      const server = app.listen(port, () => resolve(server));
    } else {
      const server = spdy.createServer(ssl, app)
	    .listen(port, () => resolve(server));
    }
  });
};

module.exports = Object.assign({}, {start});
