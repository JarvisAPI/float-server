'use strict';

const createIndex = (db) => {
  db.createIndex('user', {username: 1}, {unique: true}, (err, name) => {/*Ignore*/});
};

const repository = (container) => {
  const db_client = container.resolve('db_client');
  const db = db_client.db(container.cradle.dbSettings.db);
  const logger = container.resolve('logger');
  createIndex(db);

  // Adds a username, hence new user, entry into the user database.
  const addUser = (username,name) => {
    return new Promise((resolve, reject) => {
      const payload = {
        username,
	    name,
	    profile_images: [],
	    background_images: [],
        shops: []
      };
      db.collection('user').insertOne(payload, (err, result) => {
	    if (err) {
	      reject(new Error('An error occurred when adding a new user, err: ' + err));
	      return;
	    }
	    resolve();
      });
    });
  };

  const getUser = (username) => {
    return new Promise((resolve, reject) => {
      let projection = {projection: {background_images: {$slice:-1}, profile_images: {$slice:-1}, username:1, name:1, _id:0}};
      db.collection('user').findOne({username}, projection, (err, result) => {
        if (err) {
          reject(new Error('An error occurred when retriving user, err: ' + err));
          return;
        }
        if (result){
          resolve(result);
        } else {
          reject(new Error("Unable to find user in db"));
        }
      });
    });
  };


  // the profile image with the most recent timestamp is the current profile picture
  const addProfileImage = (username, imageURL) => {
    let update = {
      $push: {profile_images: { url:imageURL}}
    };
    return new Promise((resolve, reject) => {
      db.collection('user').updateOne({username}, update, (err, result) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	    resolve();
      });
    });
  };

  // the background image with the most recent timestamp is the current profile picture
  const addBackgroundImage = (username, imageURL) => {
    let update = {
      $push: { background_images: {url:imageURL}}
    };
    return new Promise((resolve, reject) => {
      db.collection('user').updateOne({username}, update, (err, result) => {
 	    if (err) {
 	      reject(err);
 	      return;
 	    }
 	    resolve();
      });
    });
  };

  const getProfileImages = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username},(err, result) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	    if (result) {
          if(result.profile_images){
            resolve(result.profile_images);
          }
          else{
            resolve([]); //return an empty array instead of undefinded
          }
        } else {
	      reject(new Error('Unable to find user when getting profile image'));
	    }
      });
    });
  };

  const getBackgroundImages = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username},(err, result) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	    if (result) {
          if(result.background_images){
            resolve(result.background_images);
          }
          else{
            resolve([]); //return an empty array instead of undefinded
          }
        } else {
	      reject(new Error('Unable to find user when getting background image'));
	    }
      });
    });
  };
  
  const getUserProfileBackgroundImage = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username},(err, result) => {
        if (err) {
          reject(err);
          return;
        }
        if (result) {
          let returnVal = {};
          if(result.background_images){
            returnVal.background_images = result.background_images;
          }
          else{
            returnVal.background_images = []; //return an empty array instead of undefinded
          }

          if(result.profile_images){
            returnVal.profile_images = result.profile_images;
          }
          else{
            returnVal.profile_images = [];
          }
          resolve(returnVal);
        } else {
          reject(new Error('Unable to find user when getting background image'));
        }
      });
    });
  };


  const maxShopLimit = 4;
  // Associate a given shop to a particular user.
  const addShop = (username, shop) => {
    let update = {
      $addToSet: {shops: shop}
    };
    let filter = {username};
    filter.shops = {$exists: true};
    filter.$where = `this.shops.length < ${maxShopLimit}`;
    return new Promise((resolve, reject) => {
      db.collection('user')
        .findOneAndUpdate(
          filter,
          update, (err, result) => {
	        if (err) {
	          reject(err);
	          return;
	        }
            if (!result || !result.lastErrorObject ||
                !result.lastErrorObject.updatedExisting) {
              let err = new Error();
              err.message = "Shop limit reached";
              err.status = 400;
              err.type = "ERR_SHOP_LIMIT_REACHED";
              reject(err);
              return;
            }
	        resolve();
          });
    });
  };

  // Gets all the shops that a particular user is admin of.
  // Postcondition: returns an array of shops.
  const getShops = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').findOne({username}, (err, result) => {
	    if (err) {
	      reject(err);
	      return;
	    }
	    if (result) {
	      resolve(result.shops);
        } else {
	      reject(new Error('Unable to find user when getting shops'));
	    }
      });
    });
  };

  const removeUser = (username) => {
    return new Promise((resolve, reject) => {
      db.collection('user').removeOne({username}, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  const removeShopFromUser = (username, shop) => {
    let update = {
      $pull: {shops: {shop_id: shop.shop_id}}
    };
    return new Promise((resolve, reject) => {
      db.collection('user').updateOne({username}, update, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  const getUsersBasedOnShopId = (shop_id) => {
    return new Promise((resolve, reject) => {
      db.collection('user').find({"shops.shop_id": shop_id}).toArray(function(err, result) {
        if (err) {
          reject(err);
        } else {
          var usernames = [];
          for(var i = 0 ; i < result.length; i++){
            usernames.push(result[i].username);
          }
          resolve(usernames);
        }
      });
    });
  };

  const removeProfileImageFromUser = (username, imageURL) =>{
    let update = {
      $pull: { profile_images: {url: imageURL}}
    };
    return new Promise((resolve, reject) => {
      db.collection('user').updateOne({username}, update, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  const removeBackgroundImageFromUser = (username, imageURL) =>{
    let update = {
      $pull: { background_images: {url: imageURL}}
    };
    return new Promise((resolve, reject) => {
      db.collection('user').updateOne({username}, update, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  const getUsersProfileImage = (usernames) =>{
    let profileImages = {};
    let getRecentProfileImg = (profile) =>{
      let user = {};
      user.name = profile.name;
	  
      if(profile.profile_images){
        var images = profile.profile_images;
        if(images.length > 0 && images[0].url){
          user.image = images[0].url;
	      profileImages[profile.username] = user;
        } else{
	      user.image = "";
          profileImages[profile.username] = user;
        }
      } else{
	    user.image = "";
	    profileImages[profile.username] = user;
      }
    };
    return new Promise((resolve, reject) => {
      let finish = () => resolve(profileImages);
      db.collection('user')
        .find({username: {$in:usernames}}, {projection: {background_images: {$slice:-1}, profile_images: {$slice:-1}}})
        .forEach(getRecentProfileImg, finish);
    });
  };


  const disconnect = () => {
    db_client.close();
  };

  return {
    addUser,
    addShop,
    getShops,
    removeShopFromUser,
    removeUser,
    disconnect,
    getUserProfileBackgroundImage,
    addProfileImage,
    addBackgroundImage,
    getProfileImages,
    getBackgroundImages,
    removeProfileImageFromUser,
    removeBackgroundImageFromUser,
    getUsersBasedOnShopId,
    getUser,
    getUsersProfileImage
  };
};

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve('db_client')) {
      reject(new Error('Connection db client not supplied!'));
    }
    resolve(repository(container));
  });
};

module.exports = Object.assign({}, {connect});
