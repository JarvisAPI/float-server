docker exec deploy_ejabberd-service_1 rm /opt/ejabberd/conf/ejabberd.yml
docker exec deploy_ejabberd-service_1 rm /opt/ejabberd/conf/ejabberd.yml.tpl
docker exec deploy_ejabberd-service_1 cp /opt/ejabberd/conf/ejabberd_mod_http_offline.yml /opt/ejabberd/conf/ejabberd.yml
docker exec deploy_ejabberd-service_1 mv /opt/ejabberd/conf/ejabberd_mod_http_offline.yml /opt/ejabberd/conf/ejabberd.yml.tpl
docker exec deploy_ejabberd-service_1 ejabberdctl restart
